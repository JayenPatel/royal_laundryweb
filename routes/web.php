<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/**
 * Admin routes
 */
Route::namespace('Admin')->group(function () {
    Route::get('admin/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'LoginController@login')->name('admin.login');
    Route::get('admin/logout', 'LoginController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin', 'middleware' => ['employee'], 'as' => 'admin.' ], function () {
    Route::namespace('Admin')->group(function () {
        // Route::group(['middleware' => ['role:admin|superadmin|clerk, guard:employee']], function () {
            Route::get('/', 'DashboardController@index')->name('dashboard');
            Route::namespace('Products')->group(function () {
                // Route::resource('products', 'ProductController');
                Route::get('products/index', 'ProductController@index')->name('products.index');
                Route::get('products/create', 'ProductController@create')->name('products.create');
                Route::post('products/store', 'ProductController@store')->name('products.store');
                Route::get('products/show', 'ProductController@show')->name('products.show');
                Route::get('products/edit/{id}', 'ProductController@edit')->name('products.edit');
                Route::get('products/update', 'ProductController@update')->name('products.update');
                Route::get('products/destroy/{id}', 'ProductController@destroy')->name('products.destroy');
                Route::get('products/productshow/{id}', 'ProductController@productshow')->name('products.productshow');
                
                Route::get('products/category', 'ProductController@category')->name('product.category');
                Route::get('products/Create-category', 'ProductController@create_category')->name('product.create-category');
                Route::post('products/store-category', 'ProductController@store_category')->name('product.store_category');
                Route::get('products/edit_category/{id}', 'ProductController@edit_category')->name('products.edit_category');
                Route::get('products/category_destroy/{id}', 'ProductController@category_destroy')->name('products.category_destroy');
                Route::get('products/category_show/{id}', 'ProductController@category_show')->name('products.category_show');

                Route::get('products/subcategory', 'ProductController@subcategory')->name('products.subcategory');
                Route::get('products/create-subcategory', 'ProductController@create_subcategory')->name('products.create_subcategory');
                Route::post('products/store-subcategory', 'ProductController@store_subcategory')->name('products.store_subcategory');
                Route::get('products/edit_subcategory/{id}', 'ProductController@edit_subcategory')->name('products.edit_subcategory');
                Route::get('products/subcategory_destroy/{id}', 'ProductController@subcategory_destroy')->name('products.subcategory_destroy');
                Route::get('products/subcategory_show/{id}', 'ProductController@subcategory_show')->name('products.subcategory_show');
            });
            Route::namespace('Customers')->group(function () {
                Route::resource('customers', 'CustomerController');
                Route::resource('customers.addresses', 'CustomerAddressController');

            });
            // Route::namespace('Crop')->group(function () {
            //     Route::resource('crops', 'CropController');
            // });
            Route::namespace('Crops')->group(function () {
                Route::resource('crops', 'CropController');
                Route::get('crops/create/{id?}', 'CropController@create')->name('crops.create');
            });
            
            Route::namespace('CustomerNotifications')->group(function () {
                Route::resource('notifications', 'NotificationController');
                Route::post('getValue', 'NotificationController@getValue')->name('notifications.getValue');
            });

            Route::namespace('TechnicalCategoties')->group(function () {
                Route::resource('technical', 'TechnicalController');
            });

            Route::namespace('UpdateStocks')->group(function () {
                Route::resource('updateStock', 'UpdateStockController');
            });


            Route::namespace('Driver')->group(function () {
                // Route::resource('driver', 'DriverController');
                Route::get('driver/index', 'DriverController@show')->name('driver.show');
                Route::get('driver/usershow/{id}', 'DriverController@usershow')->name('driver.usershow');
                Route::get('driver/create/', 'DriverController@create')->name('driver.create');
                Route::post('driver/store', 'DriverController@store')->name('driver.store');
                Route::get('driver/edit/{id}', 'DriverController@edit')->name('driver.edit');
                Route::get('driver/destroy/{id}', 'DriverController@destroy')->name('driver.destroy');
            });
            
            Route::namespace('CountriesData')->group(function () {
                Route::resource('country_data', 'CountryDataController');
                Route::get('country_data/show-tehsil/{id?}', 'CountryDataController@showTehsil')->name('country_data.showTehsil');
                Route::get('country_data/show-village/{id?}', 'CountryDataController@showVillage')->name('country_data.showVillage');
                Route::get('country_data/destroy-village/{id?}', 'CountryDataController@destroyVillage')->name('country_data.destroyVillage');
                Route::post('getCity', 'CountryDataController@getCity')->name('country_data.getCity');
                Route::post('getTehsil', 'CountryDataController@getTehsil')->name('country_data.getTehsil');
                Route::get('createVillage','CountryDataController@createVillage')->name('country_data.createVillage');
                Route::get('createCity','CountryDataController@createCity')->name('country_data.createCity');
            });

            Route::namespace('Priorities')->group(function () {
                Route::resource('pro_priority', 'PriorityController');
                // Route::post('update', 'PriorityController@update');
                Route::post('pro_priority/sort', 'PriorityController@cat_sort')->name('priority.sort');
                Route::post('pro_priority/subcat_sort', 'PriorityController@subcat_sort')->name('priority.subcat_sort');
                Route::post('pro_priority/getSubCategories', 'PriorityController@getSubCategories')->name('priority.getSubCategories');
                
                Route::post('pro_priority/getProduct', 'PriorityController@getProduct')->name('priority.getProduct');
            });

            Route::namespace('Offers')->group(function () {
                Route::resource('offers', 'OffersController');
                Route::post('offers/{id}', 'OffersController@update')->name('offers.update');
                Route::get('remove-image-offer', 'OffersController@removeImage')->name('offers.remove.image');
            });
            Route::namespace('Categories')->group(function () {
                Route::resource('categories', 'CategoryController');
                Route::get('remove-image-category', 'CategoryController@removeImage')->name('category.remove.image');
            });
            Route::namespace('Orders')->group(function () {
                Route::resource('orders', 'OrderController');
                Route::resource('order-statuses', 'OrderStatusController');
                Route::get('orders/{id}/invoice', 'OrderController@generateInvoice')->name('orders.invoice.generate');
            });
            Route::resource('addresses', 'Addresses\AddressController');
            Route::resource('countries', 'Countries\CountryController');
            Route::resource('countries.provinces', 'Provinces\ProvinceController');
            Route::resource('countries.provinces.cities', 'Cities\CityController');
            Route::resource('couriers', 'Couriers\CourierController');
            Route::resource('attributes', 'Attributes\AttributeController');
            Route::resource('attributes.values', 'Attributes\AttributeValueController');
            Route::resource('brands', 'Brands\BrandController');

        // });
        //Changed the employees to companies
        // Route::group(['middleware' => ['role:admin|superadmin, guard:employee']], function () {
            Route::resource('companies', 'CompaniesController');
            Route::get('companies/{id}/profile', 'CompaniesController@getProfile')->name('companies.profile');
            Route::get('remove-logo', 'CompaniesController@removeLogo')->name('company.remove.logo');
            Route::get('changeStatus', 'CompaniesController@changeStatus')->name('companies.status');
            Route::put('companies/{id}/profile', 'CompaniesController@updateProfile')->name('companies.profile.update');
            Route::resource('roles', 'Roles\RoleController');
            Route::resource('permissions', 'Permissions\PermissionController');
        // });
    });
});

/**
 * Frontend routes
 */
Auth::routes();
Route::namespace('Auth')->group(function () {
    Route::get('cart/login', 'CartLoginController@showLoginForm')->name('cart.login');
    Route::post('cart/login', 'CartLoginController@login')->name('cart.login');
    Route::get('logout', 'LoginController@logout');
});

Route::namespace('Front')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::group(['middleware' => ['auth', 'web']], function () {

        Route::namespace('Payments')->group(function () {
            Route::get('bank-transfer', 'BankTransferController@index')->name('bank-transfer.index');
            Route::post('bank-transfer', 'BankTransferController@store')->name('bank-transfer.store');
        });

        Route::namespace('Addresses')->group(function () {
            Route::resource('country.state', 'CountryStateController');
            Route::resource('state.city', 'StateCityController');
        });

        Route::get('accounts', 'AccountsController@index')->name('accounts');
        Route::get('checkout', 'CheckoutController@index')->name('checkout.index');
        Route::post('checkout', 'CheckoutController@store')->name('checkout.store');
        Route::get('checkout/execute', 'CheckoutController@executePayPalPayment')->name('checkout.execute');
        Route::post('checkout/execute', 'CheckoutController@charge')->name('checkout.execute');
        Route::get('checkout/cancel', 'CheckoutController@cancel')->name('checkout.cancel');
        Route::get('checkout/success', 'CheckoutController@success')->name('checkout.success');
        Route::resource('customer.address', 'CustomerAddressController');
    });
    Route::resource('cart', 'CartController');
    Route::get("category/{slug}", 'CategoryController@getCategory')->name('front.category.slug');
    Route::get("search", 'ProductController@search')->name('search.product');
    Route::get("{product}", 'ProductController@show')->name('front.get.product');
});