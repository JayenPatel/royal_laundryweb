<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {

    Route::post('register_customer', 'RESTAPIs\v1\customer\LoginRegisterServiceController@register');
    Route::post('update/profile', 'RESTAPIs\v1\customer\LoginRegisterServiceController@updateProfile');
    Route::post('log_in_out', 'RESTAPIs\v1\customer\LoginRegisterServiceController@log_in_out');
    
    Route::post('sendOTP', 'RESTAPIs\v1\customer\LoginRegisterServiceController@sendOTP');
    Route::post('verify', 'RESTAPIs\v1\customer\LoginRegisterServiceController@verify');

    Route::post('offer', 'RESTAPIs\v1\customer\OfferController@offer');

    Route::post('crop', 'RESTAPIs\v1\customer\CropController@selectCrop');
    Route::post('cropSolution', 'RESTAPIs\v1\customer\CropController@selectCropSolution');
	Route::post('solution', 'RESTAPIs\v1\customer\CropController@selectSolution');

	Route::get('product_categories', 'RESTAPIs\v1\customer\CategoryController@productCategories');
	Route::post('selectedCategory', 'RESTAPIs\v1\customer\CategoryController@selectedCategory');
	Route::get('get_subCategory', 'RESTAPIs\v1\customer\CategoryController@get_subCategory');
	Route::get('get_subCategory_product', 'RESTAPIs\v1\customer\CategoryController@get_subCategory_product');

	Route::post('similarProducts', 'RESTAPIs\v1\customer\ProductController@similarProducts');
	Route::post('productDetail', 'RESTAPIs\v1\customer\ProductController@productDetail');
	Route::post('companyProducts', 'RESTAPIs\v1\customer\ProductController@companyProducts');
	Route::post('traddingProducts', 'RESTAPIs\v1\customer\ProductController@traddingProducts');
	Route::post('productAttribute', 'RESTAPIs\v1\customer\ProductController@productAttribute');

	Route::post('currentOrder', 'RESTAPIs\v1\customer\ShoppingCartController@currentOrder');
	Route::post('cancelOrder', 'RESTAPIs\v1\customer\ShoppingCartController@cancelOrder');
	Route::post('showCancelOrder', 'RESTAPIs\v1\customer\ShoppingCartController@showCancelOrder');
	Route::post('completeOrder', 'RESTAPIs\v1\customer\ShoppingCartController@completeOrder');
	Route::post('addCart', 'RESTAPIs\v1\customer\ShoppingCartController@addCart');
	Route::post('payment', 'RESTAPIs\v1\customer\ShoppingCartController@payment');
	Route::get('showCart', 'RESTAPIs\v1\customer\ShoppingCartController@showCart');
	Route::post('order', 'RESTAPIs\v1\customer\ShoppingCartController@order');
	Route::post('updateQuntity', 'RESTAPIs\v1\customer\ShoppingCartController@updateQuntity');
	Route::post('deleteProductCart', 'RESTAPIs\v1\customer\ShoppingCartController@deleteProductCart');
	Route::get('count-product', 'RESTAPIs\v1\customer\ShoppingCartController@cartProductCount');

	Route::post('notification', 'RESTAPIs\v1\customer\NotificationController@notify');
	Route::post('sendNotification', 'RESTAPIs\v1\customer\NotificationController@sendNotification');
	Route::get('productNotify', 'RESTAPIs\v1\customer\NotificationController@productNotify');
	Route::post('customerNotify', 'RESTAPIs\v1\customer\NotificationController@customerNotify');
	Route::get('sendInvoice', 'RESTAPIs\v1\customer\NotificationController@sendInvoice');

	Route::post('filter', 'RESTAPIs\v1\customer\FilterController@filter');

	Route::get('state', 'RESTAPIs\v1\customer\AddressController@state');
	Route::post('cities', 'RESTAPIs\v1\customer\AddressController@city');
	Route::get('tehsil', 'RESTAPIs\v1\customer\AddressController@tehsil');
	Route::get('villages', 'RESTAPIs\v1\customer\AddressController@village');

	Route::get('ctfilter', 'RESTAPIs\v1\customer\CompanyTechnicalFilterController@companyTechnicalFilter');

});

Route::resource('details', 'RESTAPIs\v1\customer\ProductController');



