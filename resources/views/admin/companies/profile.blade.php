@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border border-header">
                    <h2>Profile</h2>
                </div>
            </div>
            <form action="{{ route('admin.companies.profile.update', $employee->id) }}" id="edit_profile" method="post" class="form" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
                <!-- Default box -->
                <!--<div class="box">-->
                    <div class="box-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="col-md-3">Name</td>
                                    <td class="col-md-3">Email</td>
                                    <td class="col-md-3">Password</td>
                                    <td class="col-md-3">Profile Image</td>
                                </tr>
                            </tbody>
                            <tbody>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <input name="name" type="text" class="form-control" value="{{ $employee->name }}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="email" type="email" class="form-control" value="{{ $employee->email }}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="password" type="password" class="form-control" value="" placeholder="xxxxxx">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="file" name="logo" id="logo" class="form-control" onchange="GetFileSize()">
                                        <label id="image_error" class="image_error">Image size is large.</label>
                                        <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.dashboard') }}" class="btn btn-default btn-sm">Back</a>
                            <button class="btn btn-success btn-sm" id="add_submit" type="submit"> <i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                <!--</div>-->
                <!-- /.box -->
            </form>
        </div>
    </section>
    <!-- /.content -->
@endsection

@section('js')

<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>

<script type="text/javascript">
    $("#edit_profile").validate({
        ignore: [],
        errorClass: 'error text-change',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                maxlength: 25,
            },
            email : {
                required: true,
                email: true,
            },
        },
        messages: {
            "name":{
                required: "Please enter name.",
                maxlength:"Please enter no more than 25 character.",
            },
            "email":{
                required: "Please enter email.",
                email: "Please enter a valid email address.",
            },
        },
            
        submitHandler: function (form) {
            $('#add_submit').attr('disabled', true);
            form.submit();
        },
    });
    function GetFileSize2() {
        var fi = document.getElementById('logo');
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {

                var fsize = fi.files.item(i).size;
                var type = fi.files.item(i).type;
                var size = fsize/1024;    
                if(size>1024){
                    document.getElementById('image_error').style.display="block";
                }
                if(type != 'png' || type != 'jpeg' || type != 'jpg'){
                    document.getElementById('image_type_error').style.display="block";
                }
            }
        }
    }
</script>
@endsection
