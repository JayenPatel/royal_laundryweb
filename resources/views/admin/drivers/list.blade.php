@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        
    @include('layouts.errors-and-messages')
    <!-- Default box -->
            <div class="box">

                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header"  >
                            <h4 class="page-title header-color">Manage Drivers</h4>
                        	<a href="{{ route('admin.driver.create') }}" class="header-add-btn">Add Driver</a>
                        </div>                        	
                    </div>
                    <div class="box-body box-body2">
	                	<!-- <select class="form-control form-control-line">
	                        <option selected>Select Vehicle Type</option>
	                        <option>Truck</option>
	                    </select> -->
	                    <select class="form-control form-control-line">
	                        <option selected>Status</option>
	                        <option>Active</option>
	                        <option>InActive</option>
	                        <option>InProgress</option>
	                        <option>Pending</option>
	                    </select>
	                    <!-- <input type="search" class="form-control form-control-line" placeholder="Search" name=""> -->
                    </div>
                    
                  		<div class="box-body">
							<div>
								 <table class="table border-b1px" id="table-id">
									<thead>
										<th class="col-md-1">Id</th>
										<th class="col-md-1">Name</th>
										<th class="col-md-1">Vehicle type</th>
										<th class="col-lg-2">email</th>
										<th class="col-md-2">Contact</th>
										<th class="col-md-2">Profile</th>
										<th class="col-md-1">Status</th>
										<!-- <th class="col-md-3">Rating</th> -->
										<th class="col-md-3">Action</th>
								</thead>
								<tbody>
									@foreach($drivers as $driver)
									<tr>
										<td>{{$driver->id}}</td>
										<td>{{$driver->name}}</td>
										<td>{{$driver->vehicle_type}}</td>
										<td>{{$driver->email}}</td>
										<td>{{$driver->contact}}</td>
										<td>@if($driver->profile!="") 
											<img src="{{ url('drivers/'.$driver->profile) }}" height="30px" width="30px">@endif
										</td>
										<td>@include('layouts.status', ['status' => $driver['status']])</td>
										<!-- <td>{{$driver->rating}}</td> -->
										<td>
	                                        <form action="{{ url('admin/driver/destroy/'.$driver->id) }}" method="get" class="form-horizontal">
	                                            {{ csrf_field() }}
	                                            
	                                            <input type="hidden" name="_method" value="delete">
	                                            <div class="btn-group">
	                                                <a href="{{route('admin.driver.usershow',$driver->id)}}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a>
                                                    <a href="{{route('admin.driver.edit',$driver->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
	                                            </div>
	                                        </form>
	                                    </td>

									</tr>
									@endforeach
								</tbody>
							</table>
							</div>
						</div>
				                <!-- /.box-body -->
				</div>
			</div>
				            <!-- /.box -->
				           

				    </section>
				    <!-- /.content -->

				    <script>
				    	$(document).ready(function() {
							$('#table-id').DataTable( {
							    
							} );
						});
				    </script>
				@endsection