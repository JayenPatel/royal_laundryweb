@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Add Driver</h4>
                </div>
            </div>

            <div class="box-body">
                <form enctype="multipart/form-data" action="{{ route('admin.driver.store') }}" id="create_driver" method="post" class="form">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="@if(isset($drivers->id)){{$drivers->id}}@endif">
                         <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="vehicle">Vehicle type <span class="text-danger">*</span></label>
                                    <select name="vehicletype" id="vehicle" class="form-control character">
                                        <option selected>Select Vehicle type</option>
                                        <option>Bike</option>
                                    </select>
                                </div>
                            </div>
                        
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Name <span class="text-danger">*</span></label>
                                    <input type="text" name="name" id="name" placeholder="Name" class="form-control character" value="@if(old('name')!=''){{old('name')}}@elseif(isset($drivers->name)){{$drivers->name}}@else{{old('name')}}@endif" maxlength="25">
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <!--<div class="input-group">-->
                                        <!--<span class="input-group-addon">@</span>-->
                                        <input type="text" name="email" id="email" placeholder="Email" class="form-control" value="@if(old('email')!=''){{old('email')}}@elseif(isset($drivers->email)){{$drivers->email}}@else{{old('email')}}@endif" maxlength="255">
                                    <!--</div>-->
                                    @if ($errors->has('email'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                       
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="mobile">Mobile <span class="text-danger">*</span></label>
                                    <input type="text" name="contact" id="mobile" placeholder="Mobile" class="form-control numeric" value="@if(old('contact')!=''){{old('contact')}}@elseif(isset($drivers->contact)){{$drivers->contact}}@else{{old('contact')}}@endif" maxlength="10">
                                </div>
                            </div>
                        </div>
                      
                         <div class="row">                       
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="status">Status </label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">Select</option>
                                        <option value="0">Disable</option>
                                        <option value="1">Enable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Vehicle Registration number <span class="text-danger">*</span></label>
                                    <input type="text" name="vehicle_reg_no" id="vehicle_reg_no" placeholder="vehicle_reg_no" class="form-control numeric" value="@if(old('vehicle_reg_no')!=''){{old('vehicle_reg_no')}}@elseif(isset($drivers->vehicle_reg_no)){{$drivers->vehicle_reg_no}}@else{{old('vehicle_reg_no')}}@endif" maxlength="25">
                                </div>
                            </div>
                        </div>
                    <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Licence number <span class="text-danger">*</span></label>
                                    <input type="text" name="licence_no" id="licence_no" placeholder="licence_no" class="form-control character" value="@if(old('licence_no')!=''){{old('licence_no')}}@elseif(isset($drivers->licence_no)){{$drivers->licence_no}}@else{{old('licence_no')}}@endif" maxlength="25">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="profile">Profile <span class="text-danger">*</span></label>
                                        <td><input type="file" id="Profile" name="profile" value="@if(old('profile')!=''){{old('profile')}}@elseif(isset($drivers->profile)){{$drivers->profile}}@else{{old('profile')}}@endif"></td>
                                        @if(isset($drivers->profile))
                                            <img id="imgupdt" src="@if(isset($drivers->profile)) {{ url('drivers/'.$drivers->profile) }}@endif" width="30px" height="30px">
                                        @else
                                            <img id="imgupdt" width="30px">
                                        @endif
                                </div>
                            </div>
                        
                            <!-- <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="mobile">Bank Account number <span class="text-danger">*</span></label>
                                    <input type="text" name="bank_acc_no" id="bank_acc_no" placeholder="bank_acc_no" class="form-control numeric" value="{{ old('bank_acc_no') }}" maxlength="10">
                                </div>
                            </div> -->
                        </div>
                        <div class="row">
                            <!-- <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Bank name <span class="text-danger">*</span></label>
                                    <input type="text" name="bank_name" id="bank_name" placeholder="bank_name" class="form-control character" value="{{ old('bank_name') }}" maxlength="25">
                                </div>
                            </div>
                        
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Account holder name <span class="text-danger">*</span></label>
                                    <input type="text" name="acc_holder_name" id="acc_holder_name" placeholder="acc_holder_name" class="form-control character" value="{{ old('acc_holder_name') }}" maxlength="25">
                                </div>
                            </div> -->
                            
                        </div>

                        </div>
                         
                        
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ url('admin/driver/index') }}" class="btn btn-default">Cancel</a>
                            <button type="submit" id="submit" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#create_driver").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
             vehicle: {
                required: true,
                // lettersonly: true,
                maxlength: 25,
            },
            name: {
                required: true,
                // lettersonly: true,
                maxlength: 25,
            },
            mobile: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
            },
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 15,
            },
            pc_certificate: {
                required: true,
            },
            status : {
                required: true,
            },
            vehicle_reg_no:{
                required: true,
                digits: true,
                maxlength: 10,
            },
            licence_no : {
                required: true,
                maxlength: 20,
            },
            bank_name : {
                required: true,
            },
            acc_holder_name : {
                required: true,
            },
            bank_acc_no:{
                required:true
            }
        },
        messages: {
            "pc_certificate":{
                required: "please select certificate.",
            },
            "name":{
                required: "Please enter name.",
                // lettersonly: "Please enter only character.",
                maxlength: "Please enter max 25 character.",
            },
            "mobile":{
                required: "Please enter mobile.",
                digits: "Please enter number only.",
                minlength: "Please enter atleast 10 digit.",
                maxlength: "Please enter no more than 10 digit.",
            },
            "email":{
                required: "Please enter email.",
                email: "Please enter a valid email address.",
            },
            "password":{
                required: "Please enter password.",
                minlength: "Please enter min 8 character.",
                maxlength: "Please enter max 15 character.",
            },
            "status":{
                required: "Please select status.",
            }
        },
        submitHandler: function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });
    
    $("#mobile").on('input', function (event){    
        mobile = document.getElementById('mobile').value;
    });
    
    $('.numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    $('.character').on('input', function (event) {
            this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    }); 

</script>
@endsection
