@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <!-- <div class="box-body">
                <h2>Customer</h2>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="col-md-3">ID</th>
                        <th class="col-md-3">Name</th>
                        <th class="col-md-3">Email</th>
                        <th class="col-md-3">Mobile Number</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>name</td>
                        <td>email</td>
                        <td>nomber</td>
                    </tr>
                    </tbody>
                </table>
            </div> -->
            <div class="box-body">
                <h2>Addresses</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th class="col-md-1">Profile</th>
                        <th class="col-md-1">Name</th>
                        <th class="col-md-1">Email</th>
                        <th class="col-md-1">Mobile</th>
                        <th class="col-md-1">Vehicle Type</th>
                        <th class="col-md-1">Vehicle Reg. no.</th>
                        <th class="col-md-1">Licence number</th>    
                    </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            <td><img src="{{ url('drivers/'.$drivers->profile) }}" height="30px" width="30px"></td>
                            <td>{{ $drivers->name }}</td>
                            <td>{{ $drivers->email }}</td>
                            <td>{{ $drivers->contact }}</td>
                            <td>{{ $drivers->vehicle_type }}</td>
                            <td>{{ $drivers->vehicle_reg_no }}</td>
                            <td>{{ $drivers->licence_no }}</td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="btn-group">
                    <a href="{{ url('admin/driver/index') }}" class="btn btn-default btn-sm">Back</a>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection
