@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Manage Technical Category</h4>
                    </div>
                </div>
                <form action="{{ route('admin.technical.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_technical">
                    <div class="box-body">
                        <div class="row">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <input type="hidden" name="id" value="@if(isset($technical->id)) {{$technical->id}} @endif">
                                <div class="row">
                                    <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="name">Technical name <span class="text-danger">*</span></label>
                                            <input type="text" name="name" id="name" placeholder="Technical name" class="form-control" value="{{ isset($technical->name) ? $technical->name : null }}" maxlength="25">
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.technical.index') }}" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-primary">@if(isset($technical)) Update @else Add @endif</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection


@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#add_technical").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                maxlength: 25,
            }
        },
        messages: {
            "name":{
                required: "Please enter technical name",
                maxlength: "Please enter max 25 character",
            }
        },
        submitHandler:  function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });
</script>
@endsection
