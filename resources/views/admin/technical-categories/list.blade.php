@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Manage Technical Categories</h4>
                    </div>
                </div>
                @if(!$technicals->isEmpty())
                    <div class="box-body">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                                <tr>
                                    <th class="col-md-3">ID</th>
                                    <th class="col-md-3">Technical Name</th>
                                    <th class="col-md-3">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($technicals as $technical)
                                <tr>
                                    <td>
                                        {{ $technical->id }}
                                    </td>
                                    <td>
                                        {{ $technical->name }}
                                    </td>
                                    <td>
                                        <form action="{{ route('admin.technical.destroy', $technical->id) }}" method="post" class="form-horizontal">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="delete">
                                            <div class="btn-group">
                                                @if($permission->edit==1)
                                                    <a href="{{ route('admin.technical.edit', $technical->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                @endif
                                                @if($permission->delete==1)
                                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                                                @endif
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- <div class="box-footer">
                            <div class="btn-group">
                                <a class="btn btn-sm btn-primary" href="{{ route('admin.technical.create') }}"><i class="fa fa-plus"></i> Create Technical Category</a>
                            </div>
                        </div> -->
                    </div>
                @else
                    <div class="box-body">
                        <p class="alert alert-warning">No technical category created yet. <a href="{{ route('admin.technical.create') }}">Create one!</a></p>
                    </div>
                @endif
            </div>
        </div>
        <!-- /.box -->
            
    </section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection
