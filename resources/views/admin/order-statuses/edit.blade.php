@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

        @include('layouts.errors-and-messages')
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Edit Status</h4>
                </div>
            </div>
            <div class="box-body">
                <form action="{{ route('admin.order-statuses.update', $orderStatus->id) }}" method="post" id="edit_order_status">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Name <span class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="name" id="name" value="{{ $orderStatus->name ?: old('name') }}" placeholder="Name" maxlength="25">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="display_name">Display Name <span class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="display_name" id="display_name" value="{{ $orderStatus->display_name ?: old('display_name') }}" placeholder="Display Name" maxlength="25">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="color">Color <span class="text-danger">*</span></label>
                                    <input class="form-control jscolor {hash:true}" type="text" name="color" id="color" value="{{ $orderStatus->color ?: old('color') }}" maxlength="15">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer btn-group">
                        <a href="{{ route('admin.order-statuses.index') }}" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection
@section('js')
    <script src="{{ asset('js/jscolor.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript">
        $("#edit_order_status").validate({
            ignore: [],
            errorClass: 'error',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            validClass: "validation-valid-label",
            rules: {
                name: {
                    required: true,
                    maxlength: 25,
                },
                display_name: {
                    required: true,
                    maxlength: 25,
                },
                color: {
                    required: true,
                    maxlength: 15,
                }
            },
            messages: {
                "name":{
                    required: "Please enter name.",
                    maxlength: "Please enter max 25 character",
                },
                "display_name":{
                    required: "Please enter display name.",
                    maxlength: "Please enter max 25 character",
                },
                "color":{
                    required: "Please enter color.",
                    maxlength: "Please enter max 15 character",
                }
            },
            submitHandler: function(form) {
                $('button[type="submit"]').attr('disabled', true);
                form.submit();
            },
        });
    </script>
@endsection
