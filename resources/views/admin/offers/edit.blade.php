@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Edit Offer</h4>
                    </div>
                </div>
                <form action="{{ route('admin.offers.update', $offer->id) }}" method="post" class="form" enctype="multipart/form-data" id="edit_offer">
                    <div class="box-body">
                        {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="company_id">Company Name<span class="text-danger">*</span></label>
                                        <div class="form-group">
                                            <select name="company_id" id="company_id" class="form-control select2">
                                                <option>Select Company Name</option>
                                                @foreach($companies as $company)
                                                    <option @if($company->id == $offer->company_id) selected="selected" @endif value="{{$company->id}}">{{$company->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="parent">Category<span class="text-danger">*</span></label>
                                        <select name="category_id" id="parent" class="form-control select2">
                                            @foreach($categories as $cat)
                                                <option @if($cat->id == $offer->parent_id) selected="selected" @endif value="{{$cat->id}}">{{$cat->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="offer_type">Offer Type <span class="text-danger">*</span></label>
                                        <input type="text" name="offer_type" id="offer_type" placeholder="Offer Type" class="form-control character" value="{!! $offer->offer_type ?: old('offer_type')  !!}" maxlength="25">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="status">Status </label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="0" @if($offer->status == 0) selected="selected" @endif>Disable</option>
                                            <option value="1" @if($offer->status == 1) selected="selected" @endif>Enable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="expired_date">Expired Date<span class="text-danger">*</span></label>
                                        <input type="Date" name="expired_date" id="expired_date" placeholder="Date" class="form-control" value="{!! $offer->expired_date ?: old('expired_date') !!}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="expired_time">Expired Time<span class="text-danger">*</span></label>
                                        <input type="time" name="expired_time" id="expired_time" placeholder="Time" class="form-control" value="{!! $offer->expired_time ?: old('expired_time') !!}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="description">Description <span class="text-danger">*</span></label>
                                        <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description">{{ $offer->description ? $offer->description : old('description') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="cover">Image </label>
                                        <input type="file" name="cover" id="cover" class="form-control" accept="image/*" onchange="GetFileSize()">
                                        <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small>
                                        <label id="image" class="image_error">Please select offer image</label>
                                        <label id="image_error" class="image_error">Image size is large.</label>
                                        <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                    </div>
                                    @if(isset($offer->cover))
                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                            <div class="form-group" style="margin-top: 10px;">
                                                <img style="height:100px !important" src="{{ $offer->cover }}" alt="" class="img-responsive"> <br/>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.offers.index') }}" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
@section('js')
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript">
        $('#company_id').change(function(){
            $("#parent option").remove();
            var id = $(this).val();
            console.log('id',id);
        
            $.ajax({
                url : "{{route('admin.products.getCategories')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    $.each( result, function(k, v) {
                        $('#parent').append($('<option>', {value:k, text:v}));
                    });
                },
                error: function()
                {
                    alert('error...');
                }
            });
        });


        $("#edit_offer").validate({
        ignore: [],
        errorClass: 'error text-change',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            company_id : {
                required: true,
            },
            category_id: {
                required: true,
            },
            offer_type: {
                required: true,
                maxlength: 25,
            },
            expired_date: {
                required: true,
            },
            expired_time: {
                required: true,
            },
        },
        messages: {
            "company_id":{
                required: "Please select company name.",
            },
            "category_id":{
                required: "Please select category.",
            },
            "offer_type":{
                required: "Please enter offer type.",
                maxlength:"Please enter no more than 25 character.",
            },
            "expired_date":{
                required: "Please enter expire date.",
            },
            "expired_time":{
                required: "Please enter expire time.",
            },
        },
        submitHandler: function(form) {
            error=0;
            document.getElementById('image_error').style.display="none";
            document.getElementById('image_type_error').style.display="none";
            document.getElementById('image').style.display="none";
            var logo = $('#cover').val();
            console.log(logo);
            if(logo!=""){
                var fi = document.getElementById('cover');
                if (fi.files.length > 0) {
                    for (var i = 0; i <= fi.files.length - 1; i++) {
                        var fsize = fi.files.item(i).size;
                        var type = fi.files.item(i).type;
                        var size = fsize/1024;   
                        console.log('fsize'+fsize+' type '+type+' size '+size);
                        if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                            document.getElementById('image_type_error').style.display="block";
                            error = 1;
                        } else if(size>1024){
                            document.getElementById('image_error').style.display="block";
                            error = 1;
                        }
                    }
                } 
            }
            if (error == 1)  {
                return false;
            }
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
            
    });
    function GetFileSize() {
        var fi = document.getElementById('cover');
        document.getElementById('image').style.display="none";
        document.getElementById('image_error').style.display="none";
        document.getElementById('image_type_error').style.display="none";
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {

                var fsize = fi.files.item(i).size;
                var type = fi.files.item(i).type;
                var size = fsize/1024; 
                if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                    document.getElementById('image_type_error').style.display="block";
                } else if(size>1024){
                    document.getElementById('image_error').style.display="block";
                }
            }
        }
    }
    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    });

    </script>
@endsection
