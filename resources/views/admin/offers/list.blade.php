@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($offers)
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Offers</h4>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                                <tr>
                                    <th class="col-md-2">Offer Type</th>
                                    <th class="col-md-2">Company Name</th>
                                    <th class="col-md-2">Category Name</th>
                                    <th class="col-md-2">Expired Date</th>
                                    <th class="col-md-1">Status</th>
                                    <th class="col-md-3">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($offers as $offer)
                                <tr>
                                    <!-- <td><a href="{{ route('admin.offers.show', $offer->id) }}">{{ $offer->offer_type }}</a></td> -->
                                    <td>{{ $offer->offer_type }}</td>
                                    <td>{{ $offer->company }}</td>
                                    <td>{{ $offer->category }}</td>
                                    @php 
                                       $newDate = date("d/m/Y", strtotime($offer->expired_date)); 
                                    @endphp
                                    <td>{{ $newDate }}</td>
                                    <td>@include('layouts.status', ['status' => $offer->status])</td>
                                    <td>
                                        <form action="{{ route('admin.offers.destroy', $offer->id) }}" method="post" class="form-horizontal">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="delete">
                                            <div class="btn-group">
                                                @if($permission->edit==1)
                                                    <a href="{{ route('admin.offers.edit', $offer->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                @endif
                                                @if($permission->delete==1)
                                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                                                @endif
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $offers->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body"><p class="alert alert-warning">No offers found.</p></div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection