@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($offer)
            <div class="box">
                <div class="box-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <td class="col-md-2">Offer Type</td>
                            <td class="col-md-3">Cover</td>
                            <td class="col-md-2">Expired Date</td>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $offer->offer_type }}</td>
                                <td>
                                    @if(isset($offer->cover))
                                        <img src="{{ asset("storage/app/public/cover/$offer->cover") }}" class="img-responsive" alt="">
                                    @endif
                                </td>
                                <td>{{ $offer->expired_date }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="box-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="col-md-2">Offer Type</th>
                                <th class="col-md-2">Company Name</th>
                                <th class="col-md-2">Category Name</th>
                                <th class="col-md-2">Expired Date</th>
                                <th class="col-md-1">Status</th>
                                <th class="col-md-3">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($offers as $off)
                            <tr>
                                <td>{{ $off->offer_type }}</td>
                                <td>{{ $off->company }}</td>
                                <td>{{ $off->category }}</td>
                                <td>{{ $off->expired_date }}</td>
                                <td>@include('layouts.status', ['status' => $off->status])</td>
                                <td>
                                    <form action="{{ route('admin.offers.destroy', $offer->id) }}" method="post" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="delete">
                                        <div class="btn-group">
                                            @if($permission->edit==1)
                                                <a href="{{ route('admin.offers.edit', $offer->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                            @endif
                                            @if($permission->delete==1)
                                                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Delete</button>
                                            @endif
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.offers.index') }}" class="btn btn-default btn-sm">Back</a>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        @endif

    </section>
    <!-- /.content -->
@endsection
