@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- @include('layouts.errors-and-messages') -->
        <div class="box"><div class="box-body">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Add Role</h4>
                </div>
            </div>
            <div class="box-body">
                <form action="{{ route('admin.roles.store') }}" id="create_role" method="post" class="form">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Name  <span class="text text-danger">*</span></label>
                                    <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ old('name') }}" maxlength="25">
                                    @if ($errors->has('name'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="display_name">Display Name  <span class="text text-danger">*</span></label>
                                    <input type="text" name="display_name" id="display_name" placeholder="Display name" class="form-control" value="{{ old('display_name') }}" maxlength="25">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-10 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" id="description" class="form-control ckeditor" placeholder="Description">{{ old('description') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-10 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <table  class="table table-striped" id="permission">
                                        <thead>
                                            <th>Sr No</th>
                                            <th>Modules</th>
                                            <th> View <input type="checkbox" class="role-add" id="view" onClick="viewAll(this)" class="role-check-box"></th>
                                            <th> Add <input type="checkbox" class="role-add" onClick="addAll(this)" class="role-check-box" id="add"></th>
                                            <th> Edit <input type="checkbox" class="role-add" onClick="editAll(this)" class="role-check-box" id="edit"></th>
                                            <th> Delete <input type="checkbox" class="role-delete" onClick="deleteAll(this)" class="role-check-box" id="delete"></th>
                                        </thead>
                                        <tbody>
                                            @foreach($modules as $module)
                                                <tr>
                                                    <td>{{ $module->id }} </td>
                                                    <td>{{ $module->display_name }} </td>
                                                    <td align="center"><input type="checkbox" name="view[]" value="1-{{$module->name}}" class="checkbox"></td>
                                                    <td align="center"><input type="checkbox" name="add[]" value="1-{{$module->name}}" class="checkbox"></td>
                                                    <td align="center"><input type="checkbox" name="edit[]" value="1-{{$module->name}}" class="checkbox"></td>
                                                    <td align="center"><input type="checkbox" name="delete[]" value="1-{{$module->name}}" class="checkbox"></td>
                                                </tr>
                                            @endforeach   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <div class="btn-group">
                                <a href="{{ route('admin.roles.index') }}" class="btn btn-default">Cancel</a>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>        
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">

    function viewAll(source) {
        checkboxes = document.getElementsByName('view[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
    function addAll(source) {
        checkboxes = document.getElementsByName('add[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
    function editAll(source) {
        checkboxes = document.getElementsByName('edit[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
    function deleteAll(source) {
        checkboxes = document.getElementsByName('delete[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }

    $('.checkbox').change(function() {
        checkboxes = document.getElementsByName('view[]');
        var a=0;
        for(var i=0, n=checkboxes.length;i<n;i++) {
            var check = checkboxes[i].checked;
            if(check==true){
                a=a+1;
                if(a==13){
                    document.getElementById('view').checked=true;
                }else {
                    document.getElementById('view').checked=false;
                }
            }
        }
        checkboxes = document.getElementsByName('add[]');
        var a=0;
        for(var i=0, n=checkboxes.length;i<n;i++) {
            var check = checkboxes[i].checked;
            if(check==true){
                a=a+1;
                if(a==13){
                    document.getElementById('add').checked=true;
                }else {
                    document.getElementById('add').checked=false;
                }
            }
        }
        checkboxes = document.getElementsByName('edit[]');
        var a=0;
        for(var i=0, n=checkboxes.length;i<n;i++) {
            var check = checkboxes[i].checked;
            if(check==true){
                a=a+1;
                if(a==13){
                    document.getElementById('edit').checked=true;
                }else {
                    document.getElementById('edit').checked=false;
                }
            }
        }
        checkboxes = document.getElementsByName('delete[]');
        var a=0;
        for(var i=0, n=checkboxes.length;i<n;i++) {
            var check = checkboxes[i].checked;
            if(check==true){
                a=a+1;
                if(a==13){
                    document.getElementById('delete').checked=true;
                }else {
                    document.getElementById('delete').checked=false;
                }
            }
        }
    });

    $("#create_role").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
            },
            display_name: {
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter name.",
            },
            "display_name":{
                required: "Please enter display name.",
            },
        },
        errorPlacement: function(error, element) {
            var elem = $(element);
            if (elem.hasClass("select2-hidden-accessible")) {
               element = $("#select2-" + elem.attr("id") + "-container").parent();
               error.insertAfter(element);
            } else {
               error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });
</script>

@endsection
