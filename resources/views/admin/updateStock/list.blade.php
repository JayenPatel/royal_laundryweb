@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if(!$products->isEmpty())
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Update Stock</h4>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-sm-9 col-md-9 col-sm-9 col-xs-9">
                        </div>
                        <div class="col-sm-6 col-md-3 col-sm-3 col-xs-3">
                            <div class="form-group" style="text-align: left;">
                                <input type="text" name="searching" id="searching" class="form-control" maxlength="25" onkeyup="searching(this.id)">
                            </div>
                        </div>
                    </div>
                    @if(!$products->isEmpty())
                    <div class="box-body">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                            <tr>
                                <th class="col-md-2">Name</th>
                                <th class="col-md-2">Technical</th>
                                <!-- <th>Category</th> -->
                                <th class="col-md-2">Company</th>
                                <th class="col-md-2">Stock Status</th>
                                <th class="col-md-2">Status</th>
                                <th class="col-md-2">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $product->name }} </td>
                                    <td>{{ $product->technical_name }}</td>
                                    <!-- <td>Rs {{ $product->category }}</td> -->
                                    <td>Rs {{ $product->company }}</td>
                                    <td>{{ $product->stock_status }}</td>
                                    <td>@include('layouts.status', ['status' => $product->status])</td>
                                    <td>
                                        @if($permission->edit==1)
                                            <div class="btn-group">
                                                <a href="{{ route('admin.updateStock.edit', $product->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> </a>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <nav>
                            {{ $fullscreen_theme->links() }}
                        </nav>
                    </div>
                    @endif
                    <div class="box-footer">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @else
            <div class="box">
                <div class="box-body"><p class="alert alert-warning">No products found.</p></div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection

@section('js')

<script type="text/javascript">


    $(document).ready(function() {
        $('#table-id').DataTable({});
    });

    function searching(id){
        var char = $('#'+id).val();
        console.log(char);
        $.ajax({
            url : "{{route('admin.products.getSorting')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "char": char
                },
            type: 'post',
            // dataType: 'json',
            success: function( result )
            {
                $("#table-id td").remove();
                for(var i=0;i<result.length;i++){
                    var status = result[0]['status'];
                    var id = parseInt(result[0]["id"]);
                    console.log(id);
                    $('#table-id').append('<tr><td>'+result[i]['name']+'</td><td>'+result[i]['technical_name']+'</td><td>'+result[i]['company']+'</td><td>'+result[i]['stock_status']+'</td><td>'+(status == 1 ? '<span style="display: none; visibility: hidden">1</span><button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button>': '<span style="display: none; visibility: hidden;">0</span><button type="button" class="btn btn-danger btn-sm" style=" width: 34px;"><i class="fa fa-times"></i></button>')+'</td><td><form action="{{ route("admin.products.destroy",'+id+')}}" method="post" class="form-horizontal">{{ csrf_field() }}<input type="hidden" name="_method" value="delete"><div class="btn-group"><a href="{{ route("admin.products.edit",'+id+')}}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a></td><td></tr>');
                }
            },
            error: function()
            {
                alert('error...');
            }
        });
    }
</script>

@endsection