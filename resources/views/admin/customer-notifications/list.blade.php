@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
<section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Manage Customer Notification</h4>
                </div>
            </div>
            <div class="box-body">
                @if($notifications)
                    <table class="table border-b1px" id="table-id">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Company</th>
                                <th>Customer Type</th>
                                <th>Message</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($notifications as $notification)
                                <tr>
                                    <td>{{ $notification->id }} </td>
                                    <td>{{ $notification->company_id }} </td>
                                    <td>{{ $notification->customer_type }} </td>
                                    <td>{{ $notification->msg }} </td>
                                    <td>@include('layouts.status', ['status' => $notification->status]) </td>
                                    <td>
                                        <form action="{{ route('admin.notifications.destroy', $notification->id) }}" method="post" class="form-horizontal">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="delete">
                                            <div class="btn-group">
                                                    <a href="{{ route('admin.notifications.edit', $notification->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <p class="alert alert-warning">No attribute yet. <a href="{{ route('admin.attributes.create') }}">Create one</a></p>
                @endif
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
    <!-- /.content -->
@endsection


@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection