@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">@if(!isset($notification)) Add @else Update @endif Notification</h4>
                </div>
            </div>
            <div class="box-body">
                <form action="{{ route('admin.notifications.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_notification">
                <div class="box-body">
                    <div class="row">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <input type="hidden" name="id" value="@if(isset($notification->id)) {{$notification->id}} @endif">
                            <div class="row">
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Company</label>
                                        <select name="company_id" id="company_id" class="form-control">
                                            @if(!isset($notification))
                                                <option value="">Select Company</option>
                                            @endif
                                            @foreach($companies as $company)
                                                <option value="{{ $company->id }}"@if(isset($notification) && ($notification->company_id==$company->id)) selected="selected" @endif>{{ $company->name }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="status">Status <span class="text-danger">*</span></label>
                                        <select name="status" id="status" class="form-control">
                                            @if(!isset($notification))
                                                <option value="">Select Status</option>
                                                <option value="0" @if(old('status')) selected="selected" @endif>Disable</option>
                                                <option value="1" @if(old('status')) selected="selected" @endif>Enable</option>
                                            @else
                                                <option value="0" @if($notification->status==0) selected="selected" @endif>Disable</option>
                                                <option value="1" @if($notification->status==1) selected="selected" @endif>Enable</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="name">Notification Type <span class="text-danger">*</span></label>
                                        <select name="customer_type" id="customer_type" class="form-control">
                                            @if(!isset($notification))
                                                <option value="">Select Notification Type</option>
                                            @endif
                                            <option value="single_customer" @if(isset($notification) && ($notification->customer_type=="single_customer")) selected @endif>Single Customer</option>
                                            <option value="all_customer" @if(isset($notification) && ($notification->customer_type=="all_customer")) selected @endif>All Customer</option>
                                            <option value="state_customer" @if(isset($notification) && ($notification->customer_type=="state_customer")) selected @endif>State Customer</option>
                                            <option value="city_customer" @if(isset($notification) && ($notification->customer_type=="city_customer")) selected @endif>City Customer</option>
                                            <option value="tehsil_customer" @if(isset($notification) && ($notification->customer_type=="tehsil_customer")) selected @endif>Tehsil Customer</option>
                                            <option value="village_customer" @if(isset($notification) && ($notification->customer_type=="village_customer")) selected @endif>Village Customer</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    @if(isset($notification) && (isset($notification->state_id)))
                                        <div class="form-group" id='state'>
                                            <label for="states">State </label>
                                            <select name="states" id="states" class="form-control">
                                                @if($values['state'])   
                                                    @foreach($values['state'] as $key=>$value)
                                                        <option @if($notification->state_id == $key) selected="selected" @endif value="{{ $notification->key }}">{{ $value }}</option>
                                                    @endforeach
                                                @endIf
                                            </select>
                                        </div>
                                    @else
                                        <div class="form-group" style="display: none;" id="state">
                                            <label for="states">State </label>
                                            <select name="states" id="states" class="form-control">
                                            </select>
                                        </div>
                                    @endif
                                    @if(isset($notification) && ($notification->customer_type=="state_cuatomer" || $notification->customer_type=="single_cuatomer"))
                                        <div class="form-group" id='values'>
                                            <label for="value">Value </label>
                                            <select name="value" id="value" class="form-control">
                                                @if($data)   
                                                    @foreach($data as $key=>$value)
                                                        <option @if($notification->value == $key) selected="selected" @endif value="{{ $notification->key }}">{{ $value }}</option>
                                                    @endforeach
                                                @endIf
                                            </select>
                                        </div>
                                    @endif 
                                </div>
                                <div id="state-customer"></div>
                            </div>
                            <div class="row">
                                @if(isset($notification) && ($notification->city_id))
                                    <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                        <div class="form-group" id="city">
                                            <label for="cities">District </label>
                                            <select name="cities" id="cities" class="form-control">
                                                @if($values['city'])   
                                                    @foreach($values['city'] as $key=>$value)
                                                        <option @if($notification->city_id == $key) selected="selected" @endif value="{{ $notification->key }}">{{ $value }}</option>
                                                    @endforeach
                                                @endIf
                                            </select>
                                        </div>
                                    </div>
                                @else 
                                    <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                        <div class="form-group" id="city" style="display: none;">
                                            <label for="cities">District </label>
                                            <select name="cities" id="cities" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                @if(isset($notification) && (isset($notification->tehsil_id)))
                                    <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                        <div class="form-group" id="tehsils">
                                            <label for="tehsil">Tehsil </label>
                                            <select name="tehsil" id="tehsil" class="form-control">
                                                @if($values['tehsil'])   
                                                    @foreach($values['tehsil'] as $key=>$value)
                                                        <option @if($notification->tehsil_id == $key) selected="selected" @endif value="{{ $notification->key }}">{{ $value }}</option>
                                                    @endforeach
                                                @endIf
                                            </select>
                                        </div>
                                    </div>
                                @else
                                    <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                        <div class="form-group" id="tehsils" style="display: none;">
                                            <label for="tehsil">Tehsil </label>
                                            <select name="tehsil" id="tehsil" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                @if(isset($notification) && ($notification->customer_type=="tehsil_customer" || $notification->customer_type=="city_customer") )
                                    <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                        <div class="form-group" id='values'>
                                            <label for="value">Value </label>   
                                            <select name="value" id="value" class="form-control">
                                                @if($data)   
                                                    @foreach($data as $key=>$value)
                                                        <option @if($notification->value == $key) selected="selected" @endif value="{{ $notification->key }}">{{ $value }}</option>
                                                    @endforeach
                                                @endIf
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                <div id="city-tehsil"></div>
                            </div>
                            @if(isset($notification) && ($notification->customer_type=="village_customer"))
                            <div class="row">
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                    <div class="form-group" id='values'>
                                        <label for="value">Value </label>   
                                        <select name="value" id="value" class="form-control">
                                            @if($data)   
                                                @foreach($data as $key=>$value)
                                                    <option @if($notification->value == $key) selected="selected" @endif value="{{ $notification->key }}">{{ $value }}</option>
                                                @endforeach
                                            @endIf
                                        </select>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row" id="village"></div>
                            <!-- <div class="row"  style="display: none;">
                                <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12" id='values'>
                                    <div class="form-group">
                                        <label for="value">Value </label>
                                        <select name="value" id="value" class="form-control">
                                        </select>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col-sm-6 col-md-10 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="msg">Message <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="msg" id="msg" rows="5" placeholder="Message" maxlength="200">@if(isset($notification)) {!! $notification->msg  !!} @else {{ old('msg') }} @endif</textarea>
                                        @if ($errors->has('msg'))
                                            <span class="text-danger" style="color: red">{{ $errors->first('msg') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.notifications.index') }}" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-primary">@if(!isset($notification)) Add @else Update @endif</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection


@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#add_notification").validate({
        validClass: "validation-valid-label",
        rules: {
            company_id: {
                required: true,
            },
            customer_type: {
                required: true,
            },
            status: {
                required: true,
            },
            msg: {
                required: true,
            }
        },
        messages: {
            "company_id":{
                required: "Please select company.",
            },
            "customer_type":{
                required: "Please select notification type.",
            },
            "status":{
                required: "Please select status.",
            },
            "msg":{
                required: "Please enter message.",
            }
        },
        submitHandler: function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });



    $('#customer_type').change(function(){
        $("#value option").remove();
        var company_id = $("#company_id").val();
        var value = $(this).val();
        $('#values').hide();
        $('#state').hide();
        $('#city').hide();
        $('#tehsils').hide();
        $('#city-tehsil').hide();
        $('#state-customer').hide();
        if(value=="state_customer" || value=="single_customer"){
            $('#state').hide();
            $.ajax({
                url:"{{route('admin.notifications.getValue')}}",
                data:{
                        "_token": "{{ csrf_token() }}",
                        "value": value,
                        "company_id": company_id
                    },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    console.log(result);
                    $("#state-customer").html('<div class="col-sm-6 col-md-5 col-sm-6 col-xs-12" id="values"><div class="form-group"><label for="value">Value </label><select name="value" id="value" class="form-control"></select></div></div></div>');
                    $('#value').append($('<option>', {value:'', text:'Select'}));
                    $.each( result, function(k, v) {
                        $('#value').append($('<option>', {value:k, text:v}));
                    });
                    $('#state-customer').show();
                },
                error: function()
                {
                    alert('error...');
                }
            });
        } else if (value=="city_customer" || value=="tehsil_customer" || value=="village_customer"){
            $('#values').hide();
            $.ajax({
                url:"{{route('admin.notifications.getValue')}}",
                data:{
                        "_token": "{{ csrf_token() }}",
                        "value": "state_customer",
                        "company_id": company_id,
                    },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    console.log(result);
                    $("#states option").remove();
                    $('#states').append($('<option>', {value:'', text:'Select'}));
                    $.each( result, function(k, v) {
                        $('#states').append($('<option>', {value:k, text:v}));
                    });
                    $('#state').show();
                },
                error: function()
                {
                    alert('error...');
                }
            });
        } else if (value=='all_customer'){
            $('#values').hide();
            $('#state').hide();
            $('#city').hide();
            $('#tehsils').hide();
        }
    });

    $('#state').change(function(){
        var company_id = $("#company_id").val();
        var values = $('#customer_type').val();
        var value = "city_customer";
        console.log(value);
        var state = $('#states').val();
        $.ajax({
                url:"{{route('admin.notifications.getValue')}}",
                data:{
                        "_token": "{{ csrf_token() }}",
                        "value": value,
                        "company_id": company_id,
                        "state": state,
                    },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                    if(values=="city_customer") {
                        console.log(value);
                        $("#city-tehsil").html('<div class="col-sm-6 col-md-5 col-sm-6 col-xs-12" id="values"><div class="form-group"><label for="value">Value </label><select name="value" id="value" class="form-control"></select></div></div></div>');
                        $('#value').append($('<option>', {value:'', text:'Select'}));
                        $.each( result, function(k, v) {
                            $('#value').append($('<option>', {value:k, text:v}));
                        });
                        $('#city-tehsil').show();
                    } else {
                        console.log(result);
                        $('#cities').append($('<option>', {value:'', text:'Select'}));
                        $.each( result, function(k, v) {
                            $('#cities').append($('<option>', {value:k, text:v}));
                        });
                        $('#city').show();
                    }
                },
                error: function()
                {
                    alert('error...');
                }
            });
    });
    $('#city').change(function(){
        var company_id = $("#company_id").val();
        var values = $('#customer_type').val();
        var value = "tehsil_customer";
        var city = $('#cities').val();

        $.ajax({
            url:"{{route('admin.notifications.getValue')}}",
            data:{
                    "_token": "{{ csrf_token() }}",
                    "value": value,
                    "company_id": company_id,
                    "city": city,
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                if(values=="tehsil_customer") {
                    console.log(value);
                    $("#city-tehsil").html('<div class="col-sm-6 col-md-5 col-sm-6 col-xs-12" id="values"><div class="form-group"><label for="value">Value </label><select name="value" id="value" class="form-control"></select></div></div></div>');
                    $('#value').append($('<option>', {value:'', text:'Select'}));
                    $.each( result, function(k, v) {
                        $('#value').append($('<option>', {value:k, text:v}));
                    });
                    $('#city-tehsil').show();
                } else {
                    console.log(result);
                    $('#tehsil').append($('<option>', {value:'', text:'Select'}));
                    $.each( result, function(k, v) {
                        $('#tehsil').append($('<option>', {value:k, text:v}));
                    });
                    $('#tehsils').show();
                }
            },
            error: function()
            {
                alert('error...');
            }
        });
    });
    $('#tehsils').change(function(){
        var company_id = $("#tehsil").val();
        var value = $('#customer_type').val();
        var tehsil = $('#tehsil').val();
        $.ajax({
            url:"{{route('admin.notifications.getValue')}}",
            data:{
                    "_token": "{{ csrf_token() }}",
                    "value": value,
                    "company_id": company_id,
                    "tehsil": tehsil,
                },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
                console.log(result);
                $("#village").html('<div class="col-sm-6 col-md-5 col-sm-6 col-xs-12" id="values"><div class="form-group"><label for="value">Value </label><select name="value" id="value" class="form-control"></select></div></div></div>');
                $('#value').append($('<option>', {value:'', text:'Select'}));
                $.each( result, function(k, v) {
                    $('#value').append($('<option>', {value:k, text:v}));
                });
                $('#village').show();
            },
            error: function()
            {
                alert('error...');
            }
        });
    });
</script>
@endsection
