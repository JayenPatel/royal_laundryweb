@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if(!$list->isEmpty())
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Village</h4>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table border-b1px" id="table-id">
                            <thead>
                                <tr>
                                    <th class="col-md-4">ID</th>
                                    <th class="col-md-4">Name</th>
                                    <th class="col-md-4">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($list as $village)
                                <tr>
                                    <td>
                                        {{ $village->TVCode }}
                                    </td>
                                    <td>
                                        {{ $village->Name }}
                                    </td>
                                    <td>
                                        <form action="{{ route('admin.country_data.destroy', $village->TVCode) }}" method="post" class="form-horizontal">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="delete">
                                            <div class="btn-group">
                                                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> </button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
            
            <!-- /.box -->
            @else
                <div class="box">
                    <p class="alert alert-warning">No village created yet. <a href="{{ route('admin.roles.create') }}">Create one!</a></p>
                    
                    <div class="box-footer">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
            @endif
    </section>
    <!-- /.content -->
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    } );
</script>
@endsection
