@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color"> Add Tehsile</h4>
                    </div>
                </div>
                <form action="{{ route('admin.country_data.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_technical">
                    <div class="box-body">
                        <div class="row">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="state_id">State <span class="text-danger">*</span></label>
                                            <select name="state_id" id="state_id" class="form-control">
                                                @if($list)   
                                                    <option value="">Select State</option>
                                                    @foreach($list as $state)
                                                        <option value="{{ $state->STCode }}">{{ $state->name }}</option>
                                                    @endforeach
                                                @endIf
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="city_id">City <span class="text-danger">*</span></label>
                                            <select name="city_id" id="city_id" class="form-control">
                                                
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Tehsil <span class="text-danger">*</span></label>
                                            <input type="text" name="name" id="name" placeholder="Tehsil name" class="form-control"  maxlength="25">
                                            @if ($errors->has('name'))
                                                <span class="text-danger" style="color: red">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.country_data.index') }}" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-primary">Add </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection


@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#add_technical").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                maxlength: 25,
            },
            state_id: {
                required: true,
            },
            city_id:{
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter tehsil name",
                maxlength: "Please enter max 25 character",
            },
            "state_id":{
                required: "Please enter state",
            },
            "city_id":{
                required: "Please enter city",
            }
        },
        submitHandler:  function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });
    
    $('#state_id').change(function(){
        $("#city_id option").remove();
        var id = $(this).val();
        $.ajax({
            url : "{{route('admin.country_data.getCity')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
                },
            type: 'post',
            // dataType: 'json',
            success: function( result )
            {
                console.log(result);
                $('#city_id').append($('<option>', {value:'', text:'Select'}));
                $.each( result, function(k, v) {
                    $('#city_id').append($('<option>', {value:k, text:v}));
                });
            },
            error: function()
            {
                alert('error...');
            }
        });
    });
</script>
@endsection
