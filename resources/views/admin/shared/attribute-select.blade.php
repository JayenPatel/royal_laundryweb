<div class="form-group">
    <label for="weight">Weight </label>
    <div class="form-inline">
        <input type="text" class="form-control col-md-8 numeric" id="weight" name="weight" placeholder="0" value="{{ number_format($product->weight, 2) }}" maxlength="7">
        <label for="mass_unit" class="sr-only">Mass unit</label>
        <!-- <select name="mass_unit" id="mass_unit" class="form-control col-md-4 select2"> -->
            @foreach($weight_units as $key => $unit)
            <input type="text" class="form-control col-md-8" name="mass_unit" id="mass_unit" value="{{ $key }} - ({{ $unit }})" disabled>
                <!-- <option @if($default_weight == $unit) selected="selected" @endif value="{{ $unit }}">{{ $key }} - ({{ $unit }})</option> -->
            @endforeach
        <!-- </select> -->
    </div>
    <div class="clearfix"></div>
</div>