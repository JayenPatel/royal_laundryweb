    <table class="table border-b1px" id="product">
        <thead>
        <tr>
            <!-- <th>ID</th> -->
            <th>Name</th>
            <td>Description</td>
            <th>Image</th>
            <!-- <th>Technical</th> -->
            <!-- <th>Category</th> -->
            <!-- <th>Company</th> -->
            <th >Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
            @foreach($products as $product)
            <tr>
                <td>{{ $product->name }}</td>
                <td>{{ $product->description }}</td>
                <td>@if($product->cover!="") 
                    <img src="{{ url('product/'.$product->cover) }}" height="30px" width="30px">
                    @endif
                </td>
                <td>@include('layouts.status', ['status' => $product['status']])</td>
                <td>
                    <form action="{{ url('admin/products/destroy/'.$product->id) }}" method="get" class="form-horizontal">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete">
                        <div class="btn-group">
                            <a href="{{route('admin.products.productshow',$product->id)}}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a>
                            <a href="{{ route('admin.products.edit',$product->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                        </div>
                    </form>
                </td>
            </tr>
            @endforeach
        
        </tbody>
    </table>