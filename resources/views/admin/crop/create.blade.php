@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Add @if($crop) Sub-Solution @else Solution @endif</h4>
                    </div>
                </div>
                <input type="hidden"  id="crop" name="crop-type" value="@if(!$crop)Solution @else Sub-Solution  @endif">
                <form action="{{ route('admin.crops.store') }}" method="post" class="form" enctype="multipart/form-data" id="add_crop">
                    <div class="box-body">
                        <div class="row">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <div class="row">
                                    @if($crop) 
                                    <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="core_category">Solution Category</label>
                                            <select name="core_category" id="core_category" class="form-control core_scroll"  >
                                                <option value="">Select Category</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category['id'] }}" >{{ $category['name'] }}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="name">Name <span class="text-danger">*</span></label>
                                            <input type="text" name="name" id="name" placeholder="@if(!$crop)Solution @else Sub-Solution @endif Name" class="form-control character" value="{!! old('name')  !!}" maxlength="25">
                                        </div>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="image">Image <span class="text-danger">*</span></label>
                                            <input type="file" name="image" id="image" class="form-control" accept="image/*" onchange="GetFileSize()">
                                            <small class="text-warning">Note :- Upload image with .jpg, .jpeg and .png extensions upto 1 MB.</small>
                                            <label id="image1" class="image_error">Please select image</label>
                                            <label id="image_error" class="image_error">Image size is large.</label>
                                            <label id="image_type_error"  class="image_type_error">Select image only .jpg, .png, .jpeg file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.crops.index') }}" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-primary button-color">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    
    var cropType=document.getElementById('crop').value;
    console.log()
    cropType=cropType.replace(/\s+/, "");
    if(cropType=="Solution") {
        $("#add_crop").validate({
            ignore: [],
            errorClass: 'error',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            validClass: "validation-valid-label",
            rules: {
                name: {
                    required: true,
                    // lettersonly: true,
                    maxlength: 25,
                },
            },
            messages: {
                "name":{
                    required: "Please enter solution name.",
                    maxlength: "Please enter max 25 character",
                },
            },
            submitHandler: function(form) {
                error=0;
                document.getElementById('image_error').style.display="none";
                document.getElementById('image_type_error').style.display="none";
                document.getElementById('image1').style.display="none";
                var logo = $('#image').val();
                if(logo==""){
                    console.log('e');
                    document.getElementById('image1').style.display="block";
                    error=1;
                } else {
                    var fi = document.getElementById('image');
                    document.getElementById('image_error').style.display="none";
                    document.getElementById('image_type_error').style.display="none";
                    if (fi.files.length > 0) {
                        for (var i = 0; i <= fi.files.length - 1; i++) {
                            var fsize = fi.files.item(i).size;
                            var type = fi.files.item(i).type;
                            var size = fsize/1024;  
                            if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                                document.getElementById('image_type_error').style.display="block";
                                error=1;
                            } else if(size>1024){
                                document.getElementById('image_error').style.display="block";
                                error=1;
                            }
                        }
                    }
                }
                
                if (error == 1)  {
                    return false;
                }
                $('button[type="submit"]').attr('disabled', true);
                form.submit();
            },
        });
    } else {
        $("#add_crop").validate({
            ignore: [],
            errorClass: 'error',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            validClass: "validation-valid-label",
            rules: {
                name: {
                    required: true,
                    // lettersonly: true,
                    maxlength: 25,
                },
                image: {
                    required: true,
                },
                core_category: {
                    required: true,
                }
            },
            messages: {
                "name":{
                    required: "Please enter sub-solution name.",
                    maxlength: "Please enter max 25 character",
                },
                "core_category":{
                    required: "Please select solution.",
                }
            },
            submitHandler: function(form) {
                error=0;
                document.getElementById('image_error').style.display="none";
                document.getElementById('image_type_error').style.display="none";
                document.getElementById('image1').style.display="none";
                var logo = $('#image').val();
                if(logo==""){
                    console.log('e');
                    document.getElementById('image1').style.display="block";
                    error=1;
                } else {
                    var fi = document.getElementById('image');
                    document.getElementById('image_error').style.display="none";
                    document.getElementById('image_type_error').style.display="none";
                    if (fi.files.length > 0) {
                        for (var i = 0; i <= fi.files.length - 1; i++) {
                            var fsize = fi.files.item(i).size;
                            var type = fi.files.item(i).type;
                            var size = fsize/1024;  
                            if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                                document.getElementById('image_type_error').style.display="block";
                                error=1;
                            } else if(size>1024){
                                document.getElementById('image_error').style.display="block";
                                error=1;
                            }
                        }
                    }
                }
                
                if (error == 1)  {
                    return false;
                }
                $('button[type="submit"]').attr('disabled', true);
                form.submit();
            },
        });
    }
    
    
    function GetFileSize() {
        var fi = document.getElementById('image');
        document.getElementById('image_error').style.display="none";
        document.getElementById('image_type_error').style.display="none";
        if (fi.files.length > 0) {
            for (var i = 0; i <= fi.files.length - 1; i++) {
                var fsize = fi.files.item(i).size;
                var type = fi.files.item(i).type;
                var size = fsize/1024;  
                if(!(type=='image/png'||type=='image/jpeg'||type=='image/jpg')){
                    document.getElementById('image_type_error').style.display="block";
                } else if(size>1024){
                    document.getElementById('image_error').style.display="block";
                }
            }
        }
    }

    $('.character').on('input', function (event) {
        this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
    });
</script>
@endsection
