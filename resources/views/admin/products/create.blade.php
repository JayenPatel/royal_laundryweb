@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Add Product</h4>
                    </div>
                </div>
                <div class="box-body">
                    <form action="{{ route('admin.products.store') }}" id="add_product" method="post" class="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="@if(isset($product->id)){{$product->id}}@endif">
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Name <span class="text-danger">*</span></label>
                                    <input type="text" name="name" id="name" placeholder="Name" class="form-control character" value="@if(old('name')!=''){{old('name')}}@elseif(isset($product->name)){{$product->name}}@else{{old('name')}}@endif" maxlength="25">
                                    @if($errors->has('name'))
                                        <span style="color: red;">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <label for="status">Status <span class="text-danger">*</span></label>
                                <select name="status" id="status" class="form-control">
                                    <option disabled selected>Select</option>
                                    
                                        <option value="0" >Disable</option>
                                        <option value="1" >Enable</option>
                                    
                                </select>
                                @if($errors->has('status'))
                                    <span style="color: red;">{{ $errors->first('status') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="cover">Image </label>
                                    <input type="file" name="cover" id="cover" class="form-control" accept="image/*">
                                    
                                    @if(isset($product->cover))
                                        <img id="imgupdt" src="@if(isset($product->cover)) {{ url('product/'.$product->cover) }}@endif" width="30px" height="30px">
                                    @else
                                        <img id="imgupdt" width="30px">
                                    @endif

                                    @if($errors->has('cover'))
                                        <span style="color: red;">{{ $errors->first('cover') }}</span>
                                    @endif
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6 col-md-10 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="description">Description </label>
                                    <textarea class="form-control" name="description" id="description" rows="5" placeholder="Description"  maxlength="300">@if(old('description')!=''){{old('description')}}@elseif(isset($product->description)){{$product->description}}@else{{old('description')}}@endif</textarea>
                                    @if($errors->has('description'))
                                        <span style="color: red;">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        

                    </div>
                    <div class="col-md-4">
                       <!-- <h2>Categories</h2> -->
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.products.index') }}" class="btn btn-default">Cancel</a>
                        <button type="submit" id="add-submit" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </form>
                </div>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
    $("#add_product").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                lettersonly: true,
                maxlength: 25,
            },
            status : {
                required: true,
            },
            description : {
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter name.",
                lettersonly: "Please enter only character.",
                maxlength: "Please enter max 25 character.",
            },
            "status":{
                required: "Please select status.",
            },
            "description":{
                required: "Please enter description",
            }
        },
        submitHandler: function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });

</script>
@endsection
