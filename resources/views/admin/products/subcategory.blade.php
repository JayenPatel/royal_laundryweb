@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
            <div class="box">

                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header"  >
                            <h4 class="page-title header-color">Manage SubCategory</h4>
                        	<a href="{{ route('admin.products.create_subcategory') }}" class="header-add-btn">Add SubCategory</a>
                        </div>                        	
                    </div>
                    <div class="box-body box-body2">
	                    
                    </div>
                    
                  		<div class="box-body">
							<div>
								 <table class="table border-b1px" id="table-id">
									<thead>
										<th class="col-md-1">Id</th>
										<th class="col-md-1">Name</th>
										<th class="col-md-2">Category</th>
										<th class="col-md-2">Description</th>
										<th class="col-md-1">Price</th>
										<th class="col-md-1">Discount</th>
										<th class="col-md-1">Offers</th>
										<th class="col-md-1">Status</th>
										<th class="col-md-3">Action</th>
								</thead>
								<tbody>
									@foreach($subcategory as $subcat)
									<tr>
										<td>{{ $subcat->id }} </td>
										<td>{{ $subcat->name }} </td>
										<td>{{ $subcat->category_name }} </td>
										<td>{{ $subcat->description }} </td>
										<td>{{ $subcat->price }} </td>
										<td>{{ $subcat->discount }} </td>
										<td>{{ $subcat->offers }} </td>
										<td>@include('layouts.status', ['status' => $subcat['status']]) </td>
										<td>
	                                        <form action="{{ route('admin.products.subcategory_destroy',$subcat->id) }}" method="get" class="form-horizontal">	                                            
	                                            <input type="hidden" name="_method" value="delete">
	                                            <div class="btn-group">
	                                            	<a href="{{route('admin.products.subcategory_show',$subcat->id)}}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a>
                                                    <a href="{{ route('admin.products.edit_subcategory',$subcat->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
	                                            </div>
	                                        </form>
	                                    </td>

									</tr>
									@endforeach
								</tbody>
							</table>
							</div>
						</div>
				                <!-- /.box-body -->
				</div>
			</div>
				            <!-- /.box -->
				    </section>
				    <!-- /.content -->
				@endsection