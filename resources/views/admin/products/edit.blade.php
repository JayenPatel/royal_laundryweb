@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <div class="box-body">
                <div class="box-body">
                    <div class="box-header with-border bg-title border-header">
                        <h4 class="page-title header-color">Edit Product</h4>
                    </div>
                </div>
                <div class="box-body">
                    <form action="{{ route('admin.products.create') }}" method="post" id="edit_product" class="form" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="put">
                                <div class="col-md-12">
                                
                                    <!-- Tab panes -->
                                    <!--<div class="tab-content" id="tabcontent">-->
                                        <div role="tabpanel" class="tab-pane">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="name">Name <span class="text-danger">*</span></label>
                                                                <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="" maxlength="8">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-10 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="description">Description </label>
                                                                <textarea class="form-control" name="description" id="description" rows="5" placeholder="Description"  maxlength="300"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="cover">Image </label>
                                                                <input type="file" name="cover" id="cover" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>                                          
                                                    <!-- /.box-body -->
                                                    </div>
                                            </div>
                                            <div class="row">
                                                <div class="box-footer">
                                                    <div class="btn-group">
                                                        <a href="{{ route('admin.products.index') }}" class="btn btn-default btn-sm">Cancel</a>
                                                        <button type="submit" id="edit-submit" class="btn btn-primary btn-sm">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection
@section('css')
    <style type="text/css">
        table.dataTable tbody th, table.dataTable tbody td {
        padding: 8px 10px;
        /* width: 100%; */
        }
        label.checkbox-inline {
            padding: 10px 5px;
            display: block;
            margin-bottom: 5px;
        }
        label.checkbox-inline > input[type="checkbox"] {
            margin-left: 10px;
        }
        ul.attribute-lists > li > label:hover {
            background: #3c8dbc;
            color: #fff;
        }
        ul.attribute-lists > li {
            background: #eee;
        }
        ul.attribute-lists > li:hover {
            background: #ccc;
        }
        ul.attribute-lists > li {
            margin-bottom: 15px;
            padding: 15px;
        }
    </style>
@endsection
@section('js')
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/additional-methods.min.js') }}"></script>
@endsection