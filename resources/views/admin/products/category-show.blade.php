@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

    @include('layouts.errors-and-messages')
    <!-- Default box -->
            <div class="box">
                <div class="box-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <td class="col-md-2">Category Name</td>
                            <td class="col-md-3">Product Name</td>
                            <td class="col-md-3">Description</td>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$category->name}}</td>
                                <td>{{$category->product_name}}</td>
                                <td>{{$category->description}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.product.category') }}" class="btn btn-default btn-sm">Back</a>
                    </div>
                </div>
            </div>
            <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
