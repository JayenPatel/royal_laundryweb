@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Add Category</h4>
                </div>
            </div>

            <div class="box-body">
                <form enctype="multipart/form-data" action="{{ route('admin.product.store_category') }}" method="post" class="form" id="create_category">
                    <div class="box-body">
                        {{ csrf_field() }}
                        
                        <input type="hidden" name="id" value="@if(isset($category->id)){{$category->id}}@endif">
                         <div class="row">
                        
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Category Name <span class="text-danger">*</span></label>
                                    <input type="text" name="name" id="name" placeholder="Category Name" class="form-control character" value="@if(old('name')!=''){{old('name')}}@elseif(isset($category->name)){{$category->name}}@else{{old('name')}}@endif" maxlength="25">
                                    @if($errors->has('name'))
                                        <span style="color: red;">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Product Name <span class="text-danger">*</span></label>
                                    <select name="product_name" class="form-control character">
                                        <option selected disabled>Select Product Name</option>
                                        @foreach($products as $product)
                                            <option value="{{$product->name}}">{{$product->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('product_name'))
                                        <span style="color: red;">{{ $errors->first('product_name') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="status">Status </label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">Select</option>
                                        <option value="0" >Disable</option>
                                        <option value="1" >Enable</option>
                                    </select>
                                    @if($errors->has('status'))
                                        <span style="color: red;">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-10 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Short Description <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="description" id="description" rows="5" placeholder="Description"  maxlength="300">
                                            @if(old('description')!=''){{old('description')}}@elseif(isset($category->description)){{$category->description}}@else{{old('description')}}@endif
                                        </textarea>
                                        @if($errors->has('description'))
                                            <span style="color: red;">{{ $errors->first('description') }}</span>
                                        @endif
                                </div>
                            </div>
                        </div>

                        </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ url('admin/products/category') }}" class="btn btn-default">Cancel</a>
                            <button type="submit" id="submit" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#create_category").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                lettersonly: true,
                maxlength: 25,
            },
            product_name: {
                required: true,
                lettersonly: true,
                maxlength: 25,
            },
            status : {
                required: true,
            },
            description : {
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter name.",
                lettersonly: "Please enter only character.",
                maxlength: "Please enter max 25 character.",
            },
            "product_name" : {
                required: "Please enter Product name",
                lettersonly: "Please enter only character.",
                maxlength: "Please enter max 25 character."
            }
            "status":{
                required: "Please select status.",
            },
            "description":{
                required: "Please enter description",
            }
        },
        submitHandler: function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });

</script>
@endsection
