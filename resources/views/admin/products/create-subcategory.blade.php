@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Add SubCategory</h4>
                </div>
            </div>

            <div class="box-body">
                <form enctype="multipart/form-data" action="{{ route('admin.products.store_subcategory') }}" method="post" class="form" id="sub_cat">
                    <div class="box-body">
                        {{ csrf_field() }}

                        <input type="hidden" name="id" value="@if(isset($categories->id)){{$categories->id}}@endif">
                         <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Name <span class="text-danger">*</span></label>
                                    <input type="text" name="name" id="name" placeholder="Name" class="form-control character" value="@if(old('name')!=''){{old('name')}}@elseif(isset($categories->name)){{$categories->name}}@else{{old('name')}}@endif" maxlength="25">
                                    @if($errors->has('name'))
                                        <span style="color: red;">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Category Name <span class="text-danger">*</span></label>
                                    <select name="category_name" class="form-control character">
                                        <option selected>Select Category Name</option>
                                        @foreach($category as $cat)
                                            <option value="{{$cat->name}}">{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('category_name'))
                                        <span style="color: red;">{{ $errors->first('category_name') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Short Description <span class="text-danger">*</span></label>
                                        <input type="text" name="description" id="description" placeholder="Short Description" class="form-control" value="@if(old('description')!=''){{old('description')}}@elseif(isset($categories->description)){{$categories->description}}@else{{old('description')}}@endif" maxlength="255">
                                        @if($errors->has('description'))
                                        <span style="color: red;">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="status">Status </label>
                                    <select name="status" id="status" class="form-control">
                                        <option disabled selected>Select</option>
                                        <option value="0" >Disable</option>
                                        <option value="1" >Enable</option>
                                    </select>
                                    @if($errors->has('status'))
                                        <span style="color: red;">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Price <span class="text-danger">*</span></label>
                                        <input type="text" name="price" id="price" placeholder="Price" class="form-control numeric" value="@if(old('price')!=''){{old('price')}}@elseif(isset($categories->price)){{$categories->price}}@else{{old('price')}}@endif" maxlength="255">
                                        @if($errors->has('price'))
                                        <span style="color: red;">{{ $errors->first('price') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Discount<span class="text-danger">*</span></label>
                                        <input type="text" name="discount" id="discount" placeholder="Discount" class="form-control numeric" value="@if(old('discount')!=''){{old('discount')}}@elseif(isset($categories->discount)){{$categories->discount}}@else{{old('discount')}}@endif" maxlength="255">
                                        @if($errors->has('discount'))
                                        <span style="color: red;">{{ $errors->first('discount') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="offers">Offers </label>
                                    <select name="offers" id="offers" class="form-control">
                                        <option selected disabled>Select</option>
                                        <option value="diwali">Diwali</option>
                                        <option value="holi">holi</option>
                                    </select>
                                    @if($errors->has('offers'))
                                        <span style="color: red;">{{ $errors->first('offers') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ url('admin/products/subcategory') }}" class="btn btn-default">Cancel</a>
                            <button type="submit" id="submit" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#sub_cat").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                lettersonly: true,
                maxlength: 25,
            },
            category_name: {
                required: true,
                lettersonly: true,
                maxlength: 25,
            },
            description:{
                required: true
            }
            status : {
                required: true,
            },
            price : {
                required: true,
                digits: true,
                maxlength: 50,
            },
            discount : {
                required: true,
                digits: true,
                maxlength: 100,
            }
            offers : {
                required: true,
                maxlength: 100,
            }
            
        },
        messages: {
            "name":{
                required: "Please enter name.",
                lettersonly: "Please enter only character.",
                maxlength: "Please enter max 25 character.",
            },
            "category_name":{
                required: "Please enter category name.",
                lettersonly: "Please enter only character.",
                maxlength: "Please enter no more than 25 digit.",
            },
            "description":{
                required: "Please enter description.",
            },
            "status":{
                required: "Please select status.",
            },
            "price":{
                required: "Please select price.",
                digits: "Please enter number only.",
                maxlength: "Please enter max 50 character."
            },
            "discount":{
                required: "Please select discount.",
                maxlength: "Please enter max 100 character."
                digits: "Please enter number only.",
            },
            "offers": {
                required: "Please select offers.",
                maxlength: "Please enter max 100 character."
            }
        },
        submitHandler: function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });
    
    $('.numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
    $('.character').on('input', function (event) {
            this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
        });

</script>
@endsection
