@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

    @include('layouts.errors-and-messages')
    <!-- Default box -->
            <div class="box">
                <div class="box-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <td class="col-md-1">Name</td>
                            <td class="col-md-2">Category Name</td>
                            <td class="col-md-2">Description</td>
                            <td class="col-md-1">Price</td>
                            <td class="col-md-1">Discount</td>
                            <td class="col-md-1">Offer Price</td>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $subcategory->name }}</td>
                                <td>{{ $subcategory->category_name }}</td>
                                <td>{{ $subcategory->description }}</td>
                                <td>{{ $subcategory->price }}</td>
                                <td>{{ $subcategory->discount }}</td>
                                <td>{{ $subcategory->offers }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.products.subcategory') }}" class="btn btn-default btn-sm">Back</a>
                    </div>
                </div>
            </div>
            <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
