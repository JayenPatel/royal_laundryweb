@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
            <div class="box">

                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header"  >
                            <h4 class="page-title header-color">Manage Category</h4>
                        	<a href="{{ route('admin.product.create-category') }}" class="header-add-btn">Add Category</a>
                        </div>                        	
                    </div>
                    <div class="box-body box-body2">
	                    
                    </div>
                    
                  		<div class="box-body">
							<div>
								 <table class="table border-b1px" id="table-id">
									<thead>
										<th class="col-md-1">Id</th>
										<th class="col-md-1">Name</th>
										<th class="col-md-2">Product Name</th>
										<th class="col-md-2">Short Description</th>
										<th class="col-md-1">Status</th>
										<th class="col-md-3">Action</th>
								</thead>
								<tbody>
									@foreach($categories as $category)
									<tr>
										<td> {{ $category->id }} </td>
										<td> {{ $category->name }} </td>
										<td> {{ $category->product_name }} </td>
										<td> {{ $category->description }} </td>
										<td> @include('layouts.status', ['status' => $category['status']]) </td>
										<td>
	                                        <form action="{{ url('admin/products/category_destroy/'.$category->id) }}" method="get" class="form-horizontal">
	                                            <input type="hidden" name="_method" value="delete">
	                                            <div class="btn-group">
	                                            	<a href="{{route('admin.products.category_show',$category->id)}}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> </a>
                                                    <a href="{{route('admin.products.edit_category',$category->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
	                                            </div>
	                                        </form>
	                                    </td>
									</tr>
									@endforeach
									
								</tbody>
							</table>
							</div>
						</div>
				                <!-- /.box-body -->
				</div>
			</div>
				            <!-- /.box -->
				    </section>
				    <!-- /.content -->
				@endsection