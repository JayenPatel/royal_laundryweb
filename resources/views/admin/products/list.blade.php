@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
            <div class="box">
                <div class="box-body">
                    <div class="box-body">
                        <div class="box-header with-border bg-title border-header">
                            <h4 class="page-title header-color">Manage Products</h4>
                            <a href="{{ route('admin.products.create') }}" class="header-add-btn">Add Product</a>
                        </div>
                    </div>
                    <div class="box-body">
                        {{ csrf_field() }}
                        @include('admin.shared.products')
                    </div>
                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection

@section('js')

@endsection
