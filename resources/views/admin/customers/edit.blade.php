@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="box-header with-border bg-title border-header">
                    <h4 class="page-title header-color">Edit Customer</h4>
                </div>
            </div>
            <div class="box-body">
                <form action="{{ route('admin.customers.update', $customer->id) }}" method="post" class="form">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="name">Name <span class="text-danger">*</span></label>
                                    <input type="text" name="name" id="name" placeholder="Name" class="form-control character" value="{!! $customer->name ?: old('name')  !!}" maxlength="25">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="mobile">Mobile <span class="text-danger">*</span></label>
                                    <input type="text" name="mobile" id="mobile" placeholder="Mobile Number" class="form-control" value="{!! $customer->mobile ?: old('mobile')  !!}" maxlength="10">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-addon">@</span>
                                        <input type="text" name="email" id="email" placeholder="Email" class="form-control" value="{!! $customer->email ?: old('email')  !!}" maxlength="255">
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="text-danger" style="color: red">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" placeholder="xxxxx" class="form-control" maxlength="15">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-5 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="status">Status <span class="text-danger">*</span></label></label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="0" @if($customer->status == 0) selected="selected" @endif>Disable</option>
                                        <option value="1" @if($customer->status == 1) selected="selected" @endif>Enable</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.customers.index') }}" class="btn btn-default btn-sm">Cancel</a>
                            <button type="submit" class="btn btn-primary btn-sm">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection


@section('js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $("#edit_customer").validate({
        ignore: [],
        errorClass: 'error',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        validClass: "validation-valid-label",
        rules: {
            name: {
                required: true,
                // lettersonly: true,
                maxlength: 25,
            },
            mobile: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
            },
            email: {
                required: true,
                email: true,
            },
            status : {
                required: true,
            }
        },
        messages: {
            "name":{
                required: "Please enter name.",
                // lettersonly: "Please enter only character.",
                maxlength: "Please enter max 25 character.",
            },
            "mobile":{
                required: "Please enter mobile.",
                digits: "Please enter number only.",
                minlength: "Please enter min 10 digit.",
                maxlength: "Please enter max 10 digit.",
            },
            "email":{
                required: "Please enter email.",
                email: "Please enter a valid email address.",
            },
            "status":{
                required: "Please select status.",
            }
        },
        submitHandler: function(form) {
            $('button[type="submit"]').attr('disabled', true);
            form.submit();
        },
    });

    $('.numeric').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
    $('.character').on('input', function (event) {
            this.value = this.value.replace(/[^0-9\.\a-z\A-Z\@ _()$&]/g, '');
        });

</script>
@endsection

