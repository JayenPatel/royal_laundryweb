@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <h2>Customer</h2>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="col-md-3">ID</th>
                        <th class="col-md-3">Name</th>
                        <th class="col-md-3">Email</th>
                        <th class="col-md-3">Mobile Number</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ $customer->id }}</td>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->email }}</td>
                        <td>{{ $customer->mobile }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="box-body">
                <h2>Addresses</h2>
                <table class="table">
                    <thead>
                    <tr>

                        <th class="col-md-1">Alias</th>
                        <th class="col-md-1">Address 1</th>
                        <th class="col-md-1">Country</th>
                        <!-- <th class="col-md-1">State</th>
                        <th class="col-md-1">City</th>
                        <th class="col-md-1">Tehsil</th>
                        <th class="col-md-1">Village</th>
                        <th class="col-md-1">Zip Code</th> -->
                        <th class="col-md-1">Status</th>
                        <!-- <td class="col-md-2">Actions</td> -->
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="btn-group">
                    <a href="{{ route('admin.customers.index') }}" class="btn btn-default btn-sm">Back</a>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection
