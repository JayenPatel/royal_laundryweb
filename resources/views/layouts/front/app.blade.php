<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-9325492-23"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{ env('GOOGLE_ANALYTICS') }}');
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }}</title>
    <!-- <title>Laracom - Laravel FREE E-Commerce Software</title> -->
    <meta name="description" content="Modern open-source e-commerce framework for free">
    <meta name="tags" content="modern, opensource, open-source, e-commerce, framework, free, laravel, php, php7, symfony, shop, shopping, responsive, fast, software, blade, cart, test driven, adminlte, storefront">
    <meta name="author" content="Jeff Simons Decena">
    <link href="{{ asset('css/style.min.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')}}"></script>
    <script src="{{ asset('https://oss.maxcdn.com/respond/1.4.2/respond.min.js')}}"></script>
    <![endif]-->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicons/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicons/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicons/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicons/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicons/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicons/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicons/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicons/logoico.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/logoico.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicons/logoico.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/logoico.png')}}">
    <link rel="manifest" href="{{ asset('favicons/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    @yield('css')
    <meta property="og:url" content="{{ request()->url() }}"/>
    @yield('og')
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js') }}"></script>

    <link rel="stylesheet" href="style.css">
</head>
<body>
 <header class="header-area">

        <div class="top-header-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="top-header-content d-flex align-items-center justify-content-center">

                            <div class="top-header-meta">
                                <p>Welcome to <span>KhedutBolo</span>, we hope you will enjoy our products and have good experience
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="famie-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">

                    <nav class="classy-navbar justify-content-between" id="famieNav">

                        <a href="index.html" class="nav-brand"><img src="img/core-img/logo.png" alt=""></a>



                        <div class="classy-menu">

                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <div class="classynav">
                                <div class="top-header-meta text-right">
                                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="khedutbolo@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <span>Email: khedutbolo@gmail.com</span></a>
                                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="+91 123 456 7890"><i class="fa fa-phone" aria-hidden="true"></i>
                                        <span>Call Us: +91 123 456 7890</span></a>
                                </div>

                                <!-- <div id="searchIcon">
                                    <i class="icon_search" aria-hidden="true"></i>
                                </div> -->

                            </div>

                        </div>
                    </nav>


                    <div class="search-form">
                        <form action="#" method="get">
                            <input type="search" name="search" id="search" placeholder="Type keywords &amp; press enter...">
                            <button type="submit" class="d-none"></button>
                        </form>

                        <div class="closeIcon"><i class="fa fa-times" aria-hidden="true"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <div class="hero-area">
        <div class="welcome-slides owl-carousel">

            <!-- Single Welcome Slides -->
            <div class="single-welcome-slides bg-img bg-overlay jarallax" style="background-image: url(img/bg-img/3.jpg);">
            </div>

            <!-- Single Welcome Slides -->
            <div class="single-welcome-slides bg-img bg-overlay jarallax" style="background-image: url(img/bg-img/18.jpg);">
            </div>

        </div>
    </div>


    <section class="shop-area section-padding-0-100">
        <div class="container">
            <div class="row">
                <div class="col-12 mt-5">
                    <!-- Section Heading -->
                    <div class="section-heading text-center">
                        <h2>Our Agricultural Product</h2>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-12 mb-5">
                    <ul class="famie-tags justify-content-center">
                        <li><a href="#" class="active" data-filter="*">All product</a></li>
                        <li><a href="#" class="">FUNGICIDES</a></li>
                        <li><a href="#" class="">HERBICIDES</a></li>
                        <li><a href="#" class="">INSECTICIDES</a></li>
                        <li><a href="#" class="">PLANT GROWTH REGULATOR</a></li>
                        <!-- <li><a href="#" class="">COTTON SEEDS</a></li>
                        <li><a href="#" class="">SEEDS</a></li> -->
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="row">
                        <div class="col-12 col-sm-4 col-lg-3">
                            <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="100ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                <div class="product-thumbnail">
                                    <img src="img/bg-img/p1.jpg" alt="">
                                </div>
                                <div class="product-desc text-center pt-4">
                                    <h3 class="product-head">INDEX</h3>
                                    <a class="product-title">NAGARJUNA (500 GM)</a>
                                    <del>
                                        <span class="amount">₹1090</span>
                                    </del>
                                    <ins>
                                        <span class="amount">₹1000</span>
                                    </ins>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="500ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                <div class="product-thumbnail">
                                    <img src="img/bg-img/p2.jpg" alt="">
                                </div>
                                <div class="product-desc text-center pt-4">
                                    <h3 class="product-head">TURF</h3>
                                    <a class="product-title">SWAL (1 KG)</a>
                                    <del>
                                        <span class="amount">₹670</span>
                                    </del>
                                    <ins>
                                        <span class="amount">₹570</span>
                                    </ins>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="1000ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                <div class="product-thumbnail">
                                    <img src="img/bg-img/p3.jpg" alt="">
                                </div>
                                <div class="product-desc text-center pt-4">
                                    <h3 class="product-head">STARGEM 45</h3>
                                    <a class="product-title">SWAL (1 KG)</a>
                                    <del>
                                        <span class="amount">₹380</span>
                                    </del>
                                    <ins>
                                        <span class="amount">₹290</span>
                                    </ins>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="1500ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                <div class="product-thumbnail">
                                    <img src="img/bg-img/p4.jpg" alt="">
                                </div>
                                <div class="product-desc text-center pt-4">
                                    <h3 class="product-head">ROKO</h3>
                                    <a class="product-title">BIOSTADT (1 KG)</a>
                                    <del>
                                        <span class="amount">₹1460</span>
                                    </del>
                                    <ins>
                                        <span class="amount">₹1230</span>
                                    </ins>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="500ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                <div class="product-thumbnail">
                                    <img src="img/bg-img/p1.jpg" alt="">
                                </div>
                                <div class="product-desc text-center pt-4">
                                    <h3 class="product-head">INDEX</h3>
                                    <a class="product-title">NAGARJUNA (500 GM)</a>
                                    <del>
                                        <span class="amount">₹1090</span>
                                    </del>
                                    <ins>
                                        <span class="amount">₹1000</span>
                                    </ins>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="1000ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                <div class="product-thumbnail">
                                    <img src="img/bg-img/p2.jpg" alt="">
                                </div>
                                <div class="product-desc text-center pt-4">
                                    <h3 class="product-head">TURF</h3>
                                    <a class="product-title">SWAL (1 KG)</a>
                                    <del>
                                        <span class="amount">₹670</span>
                                    </del>
                                    <ins>
                                        <span class="amount">₹570</span>
                                    </ins>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="1500ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                <div class="product-thumbnail">
                                    <img src="img/bg-img/p3.jpg" alt="">
                                </div>
                                <div class="product-desc text-center pt-4">
                                    <h3 class="product-head">STARGEM 45</h3>
                                    <a class="product-title">SWAL (1 KG)</a>
                                    <del>
                                        <span class="amount">₹380</span>
                                    </del>
                                    <ins>
                                        <span class="amount">₹290</span>
                                    </ins>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="2000ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                <div class="product-thumbnail">
                                    <img src="img/bg-img/p4.jpg" alt="">
                                </div>
                                <div class="product-desc text-center pt-4">
                                    <h3 class="product-head">ROKO</h3>
                                    <a class="product-title">BIOSTADT (1 KG)</a>
                                    <del>
                                        <span class="amount">₹1460</span>
                                    </del>
                                    <ins>
                                        <span class="amount">₹1230</span>
                                    </ins>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="1000ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                <div class="product-thumbnail">
                                    <img src="img/bg-img/p1.jpg" alt="">
                                </div>
                                <div class="product-desc text-center pt-4">
                                    <h3 class="product-head">INDEX</h3>
                                    <a class="product-title">NAGARJUNA (500 GM)</a>
                                    <del>
                                        <span class="amount">₹1090</span>
                                    </del>
                                    <ins>
                                        <span class="amount">₹1000</span>
                                    </ins>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="1500ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                <div class="product-thumbnail">
                                    <img src="img/bg-img/p2.jpg" alt="">
                                </div>
                                <div class="product-desc text-center pt-4">
                                    <h3 class="product-head">TURF</h3>
                                    <a class="product-title">SWAL (1 KG)</a>
                                    <del>
                                        <span class="amount">₹670</span>
                                    </del>
                                    <ins>
                                        <span class="amount">₹570</span>
                                    </ins>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="2000ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                <div class="product-thumbnail">
                                    <img src="img/bg-img/p3.jpg" alt="">
                                </div>
                                <div class="product-desc text-center pt-4">
                                    <h3 class="product-head">STARGEM 45</h3>
                                    <a class="product-title">SWAL (1 KG)</a>
                                    <del>
                                        <span class="amount">₹380</span>
                                    </del>
                                    <ins>
                                        <span class="amount">₹290</span>
                                    </ins>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="2500ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                                <div class="product-thumbnail">
                                    <img src="img/bg-img/p4.jpg" alt="">
                                </div>
                                <div class="product-desc text-center pt-4">
                                    <h3 class="product-head">ROKO</h3>
                                    <a class="product-title">BIOSTADT (1 KG)</a>
                                    <del>
                                        <span class="amount">₹1460</span>
                                    </del>
                                    <ins>
                                        <span class="amount">₹1230</span>
                                    </ins>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="contact-info-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading -->
                    <div class="section-heading text-center">
                        <p>CONTACT INFO</p>
                        <h2><span>The Best Way</span> To Contact Us For The Help</h2>
                    </div>
                </div>
            </div>

            <div class="row">

                <!-- Single Information Area -->
                <div class="col-12 col-md-4">
                    <div class="single-information-area text-center mb-100 wow fadeInUp" data-wow-delay="1000ms" style="visibility: visible; animation-delay: 100ms; animation-name: fadeInUp;">
                        <div class="contact-icon">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <h5>Address</h5>
                        <h6>Rajkot, Gujarat</h6>
                    </div>
                </div>

                <!-- Single Information Area -->
                <div class="col-12 col-md-4">
                    <div class="single-information-area text-center mb-100 wow fadeInUp" data-wow-delay="1500ms" style="visibility: visible; animation-delay: 500ms; animation-name: fadeInUp;">
                        <div class="contact-icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <h5>Phone</h5>
                        <h6>+91 123 456 7890</h6>
                    </div>
                </div>

                <!-- Single Information Area -->
                <div class="col-12 col-md-4">
                    <div class="single-information-area text-center mb-100 wow fadeInUp" data-wow-delay="2000ms" style="visibility: visible; animation-delay: 1000ms; animation-name: fadeInUp;">
                        <div class="contact-icon">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <h5>Email</h5>
                        <h6>khedutbolo@gmail.com</h6>
                    </div>
                </div>

            </div>
            <div class="c-border"></div>
        </div>
    </section>

    <section class="contact-area section-padding-100-0">
        <div class="container">
            <div class="row justify-content-between">

                <!-- Contact Content -->
                <div class="col-12 col-lg-5">
                    <div class="contact-content mb-100">
                        <!-- Section Heading -->
                        <div class="section-heading">
                            <p class="text-white">Contact now</p>
                            <h2 class="text-white"><span>Get In Touch</span> With Us</h2>
                        </div>
                        <!-- Contact Form Area -->
                        <div class="contact-form-area">
                            <form action="#" method="post">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="name" placeholder="Your Name">
                                    </div>
                                    <div class="col-lg-6">
                                        <input type="email" class="form-control" name="email" placeholder="Your Email">
                                    </div>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="subject" placeholder="Your Subject">
                                    </div>
                                    <div class="col-12">
                                        <textarea name="message" class="form-control" cols="30" rows="10" placeholder="Your Message"></textarea>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn famie-btn">Send Message</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Contact Maps -->
                <div class="col-lg-6">
                    <div class="contact-maps mb-100">
                        <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28313.28917392649!2d-88.2740948914384!3d41.76219337461615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880efa1199df6109%3A0x8a1293b2ae8e0497!2sE+New+York+St%2C+Aurora%2C+IL%2C+USA!5e0!3m2!1sen!2sbd!4v1542893000723" allowfullscreen=""></iframe> -->
                        <iframe src="https://maps.google.com/maps?width=100%&amp;height=NaN&amp;hl=en&amp;coord=22.3051991,70.8028335&amp;q=Rajkot%2C%20city%2C%20India+(Khedutbolo)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer-area">
        <div class="copywrite-area">
            <div class="container">
                <div class="copywrite-text">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <p>
                                Copyright &copy;
                                <script>
                                    document.write(new Date().getFullYear());
                                </script> All rights reserved | by <a href="{{url('/')}}">Khedutbolo</a>
                            </p>
                        </div>
                        <div class="col-md-6 text-right">
                            <p>
                                Design by | <a href="https://www.shineinfosoft.in/" target="_blank">Shine Infosoft</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<!-- @yield('content') -->

<!-- @include('layouts.front.footer') -->

<script src="{{ asset('js/front.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="js/popper.min.js"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

    <script src="js/active.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('ul li a').click(function() {
                $('li a').removeClass("active");
                $(this).addClass("active");
            });
        });
    </script>
@yield('js')
</body>
</html>