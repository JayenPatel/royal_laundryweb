=============================================== -->


<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <!-- <div class="pull-left image">
                <img src="{{ asset('img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ $user->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div> -->
            <!-- <h3 style="color: #b8c7ce">Navigation</h3> -->
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li><a href="{{ route('admin.dashboard') }}"> <i class="fa fa-home"></i><span> Dashboard</span></a></li>
          <!--   @php
                $permission = App\Helper\Permission::permission('company');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'companies') active @endif">
                    <a href="#">
                        <i class="fa fa-building"></i> <span>Companies</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.companies.index') }}" class="data @if(Request::route()->getName() == 'admin.companies.index') active @endif"><i class="fa fa-circle-o"></i> List Companies</a></li>
                        @endif
                        @if($permission->add==1)
                            <li><a href="{{ route('admin.companies.create') }}" class="@if(Request::route()->getName() == 'admin.companies.create' || Request::route()->getName() == 'admin.companies.edit') active @endif"><i class="fa fa-plus"></i>Add Company</a></li>
                        @endif
                    </ul>
                </li> -->
            @endif
            @php
                $permission = App\Helper\Permission::permission('category');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'categories') active @endif">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Categories</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href=" {{ url('admin/products/category') }} "><i class="fa fa-circle-o"></i> List Category</a></li>
                        <li><a href="{{ url('admin/products/subcategory') }}"><i class="fa fa-circle-o"></i> List SubCategory</a></li>
                    </ul>
                </li>
            @endif
             @php
                $permission = App\Helper\Permission::permission('product');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'products') active @endif">
                    <a href="{{ url('admin/products/index') }}">
                        <i class="fa fa-envira"></i> <span>Products</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                </li>
            @endif

             @php
                $permission = App\Helper\Permission::permission('order');
            @endphp
            @if($permission->view==1)
                <li class="treeview @if(request()->segment(2) == 'orders') active @endif">
                    <a href="#">
                        <i class="fa fa-money"></i> <span>Orders</span>
                        <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                </li>
            @endif
            @php
                $permission = App\Helper\Permission::permission('order_status');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'order-statuses') active @endif">
                    <a href="#">
                        <i class="fa fa-anchor"></i> <span>Order Status</span>
                        <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.order-statuses.index') }}" class="data @if(Request::route()->getName() == 'admin.order-statuses.index') active @endif"><i class="fa fa-circle-o"></i> List Status</a></li>
                        @endif
                        <!-- @if($permission->add==1)
                            <li><a href="{{ route('admin.order-statuses.create') }}"><i class="fa fa-plus"></i> Add Status</a></li>
                        @endif -->
                    </ul>
                </li>
            @endif
            
            @php
                $permission = App\Helper\Permission::permission('customer');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'customers') active @endif">
                    <a href="{{ route('admin.customers.index') }}">
                        <i class="fa fa-user"></i> <span>Customers</span>
                    </a>
                    <!-- <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.customers.index') }}" class="data @if(Request::route()->getName() == 'admin.customers.index') active @endif"><i class="fa fa-circle-o"></i> List Customers</a></li>
                        @endif
                        @if($permission->add==1)
                            <li><a href="{{ route('admin.customers.create') }}" class="@if(Request::route()->getName() == 'admin.customers.create' || Request::route()->getName() == 'admin.customers.edit') active @endif"><i class="fa fa-plus"></i> Add Customer</a></li>
                        @endif
                    </ul> -->
                </li>
            @endif
            <li class="treeview @if(request()->segment(2) == 'driver') active @endif">
                <a href="{{ url('admin/driver/index') }}">
                    <i class="fa fa-user"></i> <span>Drivers</span>
                </a>
            </li>
             @php
                $permission = App\Helper\Permission::permission('customer');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'customers') active @endif">
                    <a href="#">
                        <i class="fa fa-user"></i> <span>Offers</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.customers.index') }}" class="data @if(Request::route()->getName() == 'admin.customers.index') active @endif"><i class="fa fa-circle-o"></i> List Customers</a></li>
                        @endif
                        @if($permission->add==1)
                            <li><a href="{{ route('admin.customers.create') }}" class="@if(Request::route()->getName() == 'admin.customers.create' || Request::route()->getName() == 'admin.customers.edit') active @endif"><i class="fa fa-plus"></i> Add Customer</a></li>
                        @endif
                    </ul>
                </li>
            @endif
             @php
                $permission = App\Helper\Permission::permission('customer');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'customers') active @endif">
                    <a href="#">
                        <i class="fa fa-user"></i> <span>Update Stock</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.customers.index') }}" class="data @if(Request::route()->getName() == 'admin.customers.index') active @endif"><i class="fa fa-circle-o"></i> List Customers</a></li>
                        @endif
                        @if($permission->add==1)
                            <li><a href="{{ route('admin.customers.create') }}" class="@if(Request::route()->getName() == 'admin.customers.create' || Request::route()->getName() == 'admin.customers.edit') active @endif"><i class="fa fa-plus"></i> Add Customer</a></li>
                        @endif
                    </ul>
                </li>
            @endif
             @php
                $permission = App\Helper\Permission::permission('customer');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'customers') active @endif">
                    <a href="#">
                        <i class="fa fa-map"></i> <span>Route master</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                   <!--  <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.customers.index') }}" class="data @if(Request::route()->getName() == 'admin.customers.index') active @endif"><i class="fa fa-user-circle"></i> List Customers</a></li>
                        @endif
                        @if($permission->add==1)
                            <li><a href="{{ route('admin.customers.create') }}" class="@if(Request::route()->getName() == 'admin.customers.create' || Request::route()->getName() == 'admin.customers.edit') active @endif"><i class="fa fa-plus"></i> Add Customer</a></li>
                        @endif
                    </ul> -->
                </li>
            @endif
             @php
                $permission = App\Helper\Permission::permission('customer');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'customers') active @endif">
                    <a href="#">
                        <i class="fa fa-taxi"></i> <span>Vehicle master</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                   <!--  <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.customers.index') }}" class="data @if(Request::route()->getName() == 'admin.customers.index') active @endif"><i class="fa fa-circle-o"></i> List Customers</a></li>
                        @endif
                        @if($permission->add==1)
                            <li><a href="{{ route('admin.customers.create') }}" class="@if(Request::route()->getName() == 'admin.customers.create' || Request::route()->getName() == 'admin.customers.edit') active @endif"><i class="fa fa-plus"></i> Add Customer</a></li>
                        @endif
                    </ul> -->
                </li>
            @endif
             @php
                $permission = App\Helper\Permission::permission('customer');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'customers') active @endif">
                    <a href="#">
                        <i class="fa fa-money"></i> <span>Price master</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <!-- <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.customers.index') }}" class="data @if(Request::route()->getName() == 'admin.customers.index') active @endif"><i class="fa fa-circle-o"></i> List Customers</a></li>
                        @endif
                        @if($permission->add==1)
                            <li><a href="{{ route('admin.customers.create') }}" class="@if(Request::route()->getName() == 'admin.customers.create' || Request::route()->getName() == 'admin.customers.edit') active @endif"><i class="fa fa-plus"></i> Add Customer</a></li>
                        @endif
                    </ul> -->
                </li>
            @endif
             @php
                $permission = App\Helper\Permission::permission('customer');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'customers') active @endif">
                    <a href="#">
                        <i class="fa fa-file-archive-o"></i> <span>Report master</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.customers.index') }}" class="data @if(Request::route()->getName() == 'admin.customers.index') active @endif"><i class="fa fa-circle-o"></i> List Customers</a></li>
                        @endif
                        @if($permission->add==1)
                            <li><a href="{{ route('admin.customers.create') }}" class="@if(Request::route()->getName() == 'admin.customers.create' || Request::route()->getName() == 'admin.customers.edit') active @endif"><i class="fa fa-plus"></i> Add Customer</a></li>
                        @endif
                    </ul>
                </li>
            @endif
             @php
                $permission = App\Helper\Permission::permission('customer');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'customers') active @endif">
                    <a href="#">
                        <i class="fa fa-user"></i> <span>Trip master</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.customers.index') }}" class="data @if(Request::route()->getName() == 'admin.customers.index') active @endif"><i class="fa fa-circle-o"></i> List Customers</a></li>
                        @endif
                        @if($permission->add==1)
                            <li><a href="{{ route('admin.customers.create') }}" class="@if(Request::route()->getName() == 'admin.customers.create' || Request::route()->getName() == 'admin.customers.edit') active @endif"><i class="fa fa-plus"></i> Add Customer</a></li>
                        @endif
                    </ul>
                </li>
            @endif
             @php
                $permission = App\Helper\Permission::permission('customer');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'customers') active @endif">
                    <a href="#">
                        <i class="fa fa-usd"></i> <span>Tax master</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a><!-- 
                    <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.customers.index') }}" class="data @if(Request::route()->getName() == 'admin.customers.index') active @endif"><i class="fa fa-circle-o"></i> List Customers</a></li>
                        @endif
                        @if($permission->add==1)
                            <li><a href="{{ route('admin.customers.create') }}" class="@if(Request::route()->getName() == 'admin.customers.create' || Request::route()->getName() == 'admin.customers.edit') active @endif"><i class="fa fa-plus"></i> Add Customer</a></li>
                        @endif
                    </ul>
                </li>
            @endif -->
           <!--  @php
                $permission = App\Helper\Permission::permission('crop');
            @endphp
            @if($permission->view==1 || $permission->add==1)
            <li class="treeview @if(request()->segment(2) == 'crops') active @endif">
                    <a href="#">
                        <i class="fa fa-gear"></i> <span>Solution</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.crops.index') }}" class="data @if(Request::route()->getName() == 'admin.crops.index') active @endif"><i class="fa fa-circle-o"></i> List Solution</a></li>
                        @endif -->
                        <!-- @if($permission->add==1)
                            <li><a href="{{ route('admin.crops.create') }}" class="data @if(Request::route()->getName() == 'admin.crops.create' || Request::route()->getName() == 'admin.crops.edit') active @endif"><i class="fa fa-plus"></i>Add Solution</a></li>
                        @endif -->
                        <!--  @if($permission->add==1)<li><a href="{{ route('admin.crops.create', $id=1) }}" class="@if(Request::route()->getName() == 'admin.crops.create') active @endif"><i class="fa fa-plus"></i> Add Sub-Solution</a></li>@endif
                    </ul>
                </li>
            @endif -->
            
           <!--  @php
                $permission = App\Helper\Permission::permission('technical');
            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'technical') active @endif">
                    <a href="#">
                        <i class="fa fa-star-o"></i> <span>Technical Categories</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.technical.index') }}" class="data @if(Request::route()->getName() == 'admin.technical.index') active @endif"><i class="fa fa-circle-o"></i> List Technical Categories</a></li>
                        @endif
                        @if($permission->add==1)
                            <li><a href="{{ route('admin.technical.create') }}" class="@if(Request::route()->getName() == 'admin.technical.create' || Request::route()->getName() == 'admin.technical.edit') active @endif"><i class="fa fa-plus"></i> Add Technical Categories</a></li>
                        @endif
                    </ul>
                </li>
            @endif -->
           
           
            <!-- <li class="treeview @if(request()->segment(2) == 'addresses') active @endif">
                <a href="#"><i class="fa fa-map-marker"></i> Addresses
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.addresses.index') }}"><i class="fa fa-circle-o"></i> List Addresses</a></li>
                    <li><a href="{{ route('admin.addresses.create') }}"><i class="fa fa-plus"></i> Add Address</a></li>
                </ul>
            </li> -->
           <!--  @php
                $permission = App\Helper\Permission::permission('offer');

            @endphp
            @if($permission->view==1 || $permission->add==1)
                <li class="treeview @if(request()->segment(2) == 'offers' || request()->segment(2) == 'addresses') active @endif">
                    <a href="#">
                        <i class="fa fa-gift"></i> <span>Offers</span>
                        <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if($permission->view==1)
                            <li><a href="{{ route('admin.offers.index') }}" class="data @if(Request::route()->getName() == 'admin.offers.index') active @endif"><i class="fa fa-circle-o"></i> List Offers</a></li>
                        @endif
                        @if($permission->add==1)
                            <li><a href="{{ route('admin.offers.create') }}" class="@if(Request::route()->getName() == 'admin.offers.create' || Request::route()->getName() == 'admin.offers.edit') active @endif"><i class="fa fa-plus"></i> Add Offer</a></li>
                        @endif
                    </ul>
                </li>
            @endif -->
            
            <!-- <li class="header">CONFIG</li> -->
            
           <!--  @if($user->hasRole('admin|superadmin'))
                @php
                    $permission = App\Helper\Permission::permission('role');
                @endphp
                @if($permission->view==1 || $permission->add==1)
                    <li class="treeview @if(request()->segment(2) == 'roles') active @endif">
                        <a href="#">
                            <i class="fa fa-group"></i> <span>Roles</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            @if($permission->view==1)
                                <li><a href="{{ route('admin.roles.index') }}" class="data @if(Request::route()->getName() == 'admin.roles.index') active @endif"><i class="fa fa-circle-o"></i> List Roles</a></li>
                            @endif
                            @if($permission->add==1)
                                <li><a href="{{ route('admin.roles.create') }}" class="@if(Request::route()->getName() == 'admin.roles.create' || Request::route()->getName() == 'admin.roles.edit') active @endif"><i class="fa fa-plus"></i> Add Roles</a></li>
                            @endif
                        </ul>
                    </li>
                @endif -->
           <!--  @endif
            @php
                $permission = App\Helper\Permission::permission('priority');
            @endphp
            @if($permission->view==1)
                <li class="treeview @if(request()->segment(2) == 'pro_priority') active @endif">
                    <a href="#">
                        <i class="fa fa-list"></i> <span>Update Priority</span>
                        <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                            <li><a href="{{ route('admin.pro_priority.index') }}" class="@if(Request::route()->getName() == 'admin.pro_priority.index') active @endif"><i class="fa fa-circle-o"></i> List Priority</a></li>
                    </ul>
                </li>
            @endif -->
      <!--       @php
                $permission = App\Helper\Permission::permission('update_stock');
            @endphp
            @if($permission->view==1)
                <li class="treeview @if(request()->segment(2) == 'updateStock') active @endif">
                    <a href="#">
                        <i class="fa fa-edit"></i> <span>Update Stock</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('admin.updateStock.index') }}" class="@if(Request::route()->getName() == 'admin.updateStock.index' || Request::route()->getName() == 'admin.updateStock.edit') active @endif"><i class="fa fa-circle-o"></i> List Update Stock</a></li>
                    </ul>
                </li>
            @endif
            @php
                $permission = App\Helper\Permission::permission('notification');
            @endphp
            @if($permission->view==1 || $permission->add==1)
            <li class="treeview @if(request()->segment(2) == 'notifications') active @endif">
                <a href="#">
                    <i class="fa fa-bell"></i> <span>Customer Notification</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if($permission->view==1)<li><a href="{{ route('admin.notifications.index') }}" class="data @if(Request::route()->getName() == 'admin.notifications.index') active @endif"><i class="fa fa-circle-o"></i> List Notification</a></li>@endif
                    @if($permission->add==1)<li><a href="{{ route('admin.notifications.create') }}" class="@if(Request::route()->getName() == 'admin.notifications.create' || Request::route()->getName() == 'admin.notifications.edit') active @endif"><i class="fa fa-plus"></i> Create Notification</a></li>@endif
                </ul>
            </li>
            @endif -->
           <!--  <li class="treeview @if(request()->segment(2) == 'notifications') active @endif">
                <a href="#">
                    <i class="fa fa-bell"></i> <span>Country Data</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.country_data.index') }}" class="data @if(Request::route()->getName() == 'admin.country_data.index') active @endif"><i class="fa fa-circle-o"></i> List State</a></li>
                    <li><a href="{{ route('admin.country_data.createCity') }}" class=data "@if(Request::route()->getName() == 'admin.country_data.createCity') active @endif"><i class="fa fa-plus"></i> Add District</a></li>
                    <li><a href="{{ route('admin.country_data.create') }}" class="data @if(Request::route()->getName() == 'admin.country_data.create') active @endif"><i class="fa fa-plus"></i> Add Tehsil</a></li>
                    <li><a href="{{ route('admin.country_data.createVillage') }}" class="@if(Request::route()->getName() == 'admin.country_data.createVillage') active @endif"><i class="fa fa-plus"></i> Add Village</a></li>
                    </ul>
            </li> -->
          
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- ===============================================