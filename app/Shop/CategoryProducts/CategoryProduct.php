<?php

namespace App\Shop\CategoryProducts;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
    public $table = "category_product";
}
