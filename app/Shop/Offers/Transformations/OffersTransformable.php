<?php

namespace App\Shop\Offers\Transformations;

use App\Shop\Offers\Offer;
use App\Shop\Employees\Employee;
use App\Shop\Employees\Repositories\EmployeeRepository;
use App\Shop\Categories\Category;
use App\Shop\Categories\Repositories\CategoryRepository;

trait OffersTransformable
{
    /**
     * Transform the address
     *
     * @param Offer $address
     *
     * @return Offer
     * @throws \App\Shop\Cities\Exceptions\CityNotFoundException
     * @throws \App\Shop\Countries\Exceptions\CountryNotFoundException
     * @throws \App\Shop\Customers\Exceptions\CustomerNotFoundException
     */
    public function transformOffer(Offer $offer)
    {
        $obj = new Offer();
        $obj->id = $offer->id;
        $obj->cover = $offer->cover;
        $obj->offer_type = $offer->offer_type;
        $obj->expired_date = $offer->expired_date;
        $obj->expired_time = $offer->expired_time;
        $obj->description = $offer->description;
        $obj->status = $offer->status;
        $obj->parent_id = $offer->parent_id;

        $employeeRepo = new EmployeeRepository(new Employee());
        $company = $employeeRepo->findEmployeeById($offer->company_id);
        $obj->company = $company->name;

        $categoryRepo = new CategoryRepository(new Category());
        $category = $categoryRepo->findCategoryById($offer->category_id);
        $obj->category = $category->name;

        return $obj;
    }
}
