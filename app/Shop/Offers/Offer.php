<?php

namespace App\Shop\Offers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\Shop\Categories\Category;

class Offer extends Model
{
    use SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'category_id',
        'cover',
        'offer_type',
        'expired_date',
        'expired_time',
        'status',
        'description',
        'parent_id'
    ];

    public function companies(){
        return $this->hasOne('App\Shop\Employees\Employee','id','company_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

}
