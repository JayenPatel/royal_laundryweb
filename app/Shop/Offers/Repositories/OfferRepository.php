<?php

namespace App\Shop\Offers\Repositories;

use Jsdecena\Baserepo\BaseRepository;
use App\Shop\Offers\Offer;
use App\Shop\Offers\Exceptions\OfferNotFoundException;
use App\Shop\Offers\Repositories\Interfaces\OfferRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Http\UploadedFile;

class OfferRepository extends BaseRepository implements OfferRepositoryInterface
{
    /**
     * EmployeeRepository constructor.
     *
     * @param Offer $offer
     */
    public function __construct(Offer $offer)
    {
        parent::__construct($offer);
        $this->model = $offer;
    }

    /**
     * List all the employees
     *
     * @param string $order
     * @param string $sort
     *
     * @return Collection
     */
    public function listOffers(string $order = 'id', string $sort = 'desc'): Collection
    {
        return $this->all(['*'], $order, $sort);
    }

    /**
     * Create the offer
     *
     * @param array $data
     *
     * @return Offer
     */
    public function createOffer(array $data): Offer
    {
        // dd($data);
        return $this->create($data);
    }

    /**
     * Find the offer by id
     *
     * @param int $id
     *
     * @return Offer
     */
    public function findOfferById(int $id): Offer
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new OfferNotFoundException;
        }
    }

    /**
     * Update offer
     *
     * @param array $params
     *
     * @return bool
     */
    public function updateOffer(array $params): bool
    {
      return $this->update($params);
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function saveCoverImage(UploadedFile $file) : string
    {
        return $file->store('offers', ['disk' => 'public']);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteOffer() : bool
    {
        return $this->delete();
    }

    /**
     * @param $file
     * @param null $disk
     * @return bool
     */
    public function deleteFile(array $file, $disk = null) : bool
    {
        return $this->update(['cover' => null], $file['offers']);
    }
}
