<?php

namespace App\Shop\Offers\Requests;

use App\Shop\Base\BaseFormRequest;
use Illuminate\Validation\Rule;

class UpdateOfferRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required'],
            'category_id' => ['required'],
            'offer_type' => ['required'],
            'expired_date' => ['required']
        ];
    }
}
