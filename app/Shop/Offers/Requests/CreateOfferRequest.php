<?php

namespace App\Shop\Offers\Requests;

use App\Shop\Base\BaseFormRequest;

class CreateOfferRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required'],
            'category_id' => ['required'],
            'offer_type' => ['required'],
            'expired_date' => ['required'],
            'expired_time' => ['required'],
            // 'description' => ['required'],
            'cover' => ['required', 'file', 'image:png,jpeg,jpg,gif']
        ];
    }
}
