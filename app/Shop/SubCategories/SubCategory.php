<?php

namespace App\Shop\SubCategories;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'subcategory';
    public $timestamp = false;
    protected $fillable = [
    	'name',
    	'category_name',
    	'description',
    	'price',
    	'discount',
    	'offers',
    	'status'
    ];
}
