<?php

namespace App\Shop\Carts;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $table = "cart";
    public $primaryKey = "id";
    public $timestamps = false;
    protected $fillable = [
        'product_id',
        'customer_id',
        'quantity',
        'product_size',
        'total_price',
        'attribute_id',
        'shipping_price'
    ];
}
