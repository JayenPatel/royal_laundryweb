<?php

namespace App\Shop\Categories;

use App\Shop\Products\Product;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $table = 'categories';
    protected $fillable = [
        'name',
        'description',
        'status',
        'product_name'
    ];
}
