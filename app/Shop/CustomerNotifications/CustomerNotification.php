<?php

namespace App\Shop\CustomerNotifications;

use Illuminate\Database\Eloquent\Model;

class CustomerNotification extends Model
{
    public $table = "customer_notifications";
    protected $fillable = [
        'company_id',
        'customer_type',
        'place_type',
        'value',
        'status',
        'msg'
    ];
}
