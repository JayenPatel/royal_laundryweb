<?php

namespace App\Shop\Crops\Requests;

use App\Shop\Base\BaseFormRequest;

class CreateCropRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'crop_category' => ['required'],
            'name' => ['required'],
            'image' => ['required'],
        ];
    }
}
