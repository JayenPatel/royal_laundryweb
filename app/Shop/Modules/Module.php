<?php

namespace App\Shop\Modules;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Module extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table = "module";
    protected $fillable = [
        'name'
    ];

}
