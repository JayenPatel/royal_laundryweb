<?php

namespace App\Shop\ModulePermissions;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Nicolaslopezj\Searchable\SearchableTrait;

class ModulePermission extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table = "module_permission";
    protected $fillable = [
        'module_id',
        'user_id',
        'view',
        'edit',
        'add',
        'delete'
    ];
}
