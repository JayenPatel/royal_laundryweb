<?php

namespace App\Shop\Products;

use Illuminate\Database\Eloquent\Model;

class Product_attributes extends Model
{
    public $table = "product_attribute";

    // protected $fillable = [
    //     'product_id',
    //     'key',
    //     'value',
    //     'price',
    //     'sale_price',
    //     'quantity',
    //     'available_quantity'
    // ]; 

    protected $visible = ['product_id','key','value','price','sale_price','quantity','available_quantity','id','user_quantity','shipping_price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bookmark()
    {
        return $this->belongsTo(Bookmark::class);
    }
}
