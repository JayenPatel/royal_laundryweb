<?php

namespace App\Shop\Drivers;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
   public $table = 'drivers';
   protected $fillable = [ 
   	'vehicle_type',
   	'name',
   	'email',
   	'contact',
   	'profile',
   	'status',
   	'rating',
   	'pc_certificate',
   	'vehicle_reg_no',
   	'licence_no',
   	'bank_acc_no',
   	'bank_name',
   	'acc_holder_name'
   ];
}
