<?php

namespace App\Helper;

use Illuminate\Http\Request;
use Mail;

class MailHelper 
{
	public static function productNotification($data="") 
	{
	    // dd($product);
	    try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.company.productNotification',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('Khedut Bolo product details');
			        $message->from('shineinfosoft22@gmail.com','Khedut bolo');
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}

	public static function invoiceNotification($data="") 
	{
		try {
			if($data=="") {
				return "false";
			} else{
				$sendMail = Mail::send('emails.customer.invoice',['data'=>$data] , function($message) use ($data) {
			        $message->to($data['email'], $data['name'])->subject('Khedut Bolo order invoice');
			        $message->from('shineinfosoft22@gmail.com','Khedut bolo');
			        $message->attach(public_path('invoices/'.$data['order']->id.'.pdf'));
			    });
		        if($sendMail){
			        return "true";
		        } else{
		        	return "false";
		        }
			}
			exit;
		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}

}