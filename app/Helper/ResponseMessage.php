<?php 
namespace App\Helper;

use Illuminate\Database\Eloquent\Helper;

class ResponseMessage
{
	public static function success($msg,$data = array())
	{
		echo json_encode(['status' => 200, 'message' => $msg, 'data' => $data]);
		exit;
	}

	public static function error($msg,$data = array())
	{
		echo json_encode(['status' => 0, 'message' => $msg , 'data' => $data]); 	
		exit;
	}

	public static function unauthorize($msg)
	{
		echo json_encode(['status' => 401, 'message' => $msg , 'data' => array()]);
		exit;
	}

	public static function successor($msg,$data = array(), $extra=array())
	{
		if(count($extra)!=0){
			echo json_encode(['status' => 200, 'message' => $msg , 'data' => $data , array_keys($extra)[0]=>array_values($extra)[0]]) ; 	
		} else{
			echo json_encode(['status' => 200, 'message' => $msg , 'data' => null]);
		}
		exit;
	}
}
?>