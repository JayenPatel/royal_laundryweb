<?php 
namespace App\Helper;

use Illuminate\Database\Eloquent\Helper;
use AWS;
use App\Helper\RandomHelper;
use App\Helper\Exceptions;
use Illuminate\Support\Facades\Cache;

class SMSHelper
{
	public static function sendOTP($country_code="",$phone="")
	{
		try {
			$exp_time = 0;
			if(Cache::has($country_code))
			{
				$temp = Cache::get($country_code);
				$otp = $temp;
			} else{
				$otp = RandomHelper::randomOTP();
				Cache::put($country_code,$otp,$exp_time);
			}
			if(Cache::has($country_code))
			{
				$temp = Cache::get($country_code);
				$otp = $temp;
			} else{
				$otp = RandomHelper::randomOTP();
				Cache::put($country_code,$otp,$exp_time);
			}	
			if($country_code=="" || $phone=="") {
				return "false";
			} else{
				$sms = AWS::createClient('sns');
				$message = " is the one time password (OTP) for khetudbolo. This is usable once time and PLEASE DO NOT SHARE WITH ANYONE.";
		        $sendSMS = $sms->publish([
		            'Message' => $otp.$message,
		            'PhoneNumber' => $country_code.$phone,    
		            'MessageAttributes' => [
		                'AWS.SNS.SMS.SMSType'  => [
		                    'DataType'    => 'String',
		                    'StringValue' => 'Transactional',
		                 ]
		             ],
		        ]);


		        if($sendSMS){
			        $data['otp'] = $otp;
			        $data['phone'] = $phone;
			        return $data;
		        } else{
		        	return "false";
		        }
			}
			exit;

		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}


	public static function customerNotification($country_code="",$phone="", $msg="")
	{
		try {	
			if($country_code=="" || $phone=="") {
				return "false";
			} else{
				$sms = AWS::createClient('sns');
				$message = $msg;
		        $sendSMS = $sms->publish([
		            'Message' => $message,
		            'PhoneNumber' => $country_code.$phone,  
		            'MessageAttributes' => [
		                'AWS.SNS.SMS.SMSType'  => [
		                    'DataType'    => 'String',
		                    'StringValue' => 'Transactional',
		                 ]
		             ],
		        ]);
		        // dd($country_code.$phone);
		        if($sendSMS){
			        $data['msg'] = $message;
			        $data['phone'] = $phone;
			        return $data;
		        } else{
		        	return "false";
		        }
			}
			exit;

		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}


	public static function orderStatus($country_code="",$phone="", $status="")
	{
		try {	
			if($country_code=="" || $phone=="") {
				return "false";
			} else{
                // dd($country_code.$phone);
				$sms = AWS::createClient('sns');
				$message = "Dear Customer, your order has been ".$status;
		        $sendSMS = $sms->publish([
		            'Message' => $message,
		            'PhoneNumber' => $country_code.$phone,  
		            'MessageAttributes' => [
		                'AWS.SNS.SMS.SMSType'  => [
		                    'DataType'    => 'String',
		                    'StringValue' => 'Transactional',
		                 ]
		             ],
		        ]);
		        // dd($country_code.$phone);
		        if($sendSMS){
			        $data['msg'] = $message;
			        $data['phone'] = $phone;
			        return $data;
		        } else{
		        	return "false";
		        }
			}
			exit;

		} catch (Exception $e) {
			Exceptions::exception($e);	
		}
	}
}