<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\ResponseMessage;
use App\Shop\States\State;
use App\Shop\Cities\City;
use Log;
use App\Shop\CountriesData\CountryData;
use DB;


class AddressController extends Controller
{
   public function state(Request $request) {
        try {
            $states=[];
            if($request->code!=""){
                if(CountryData::where('STCode', 'like', '%' .$request->code)->where('DTCode',000)->exists()){
                    $states = DB::select(DB::raw("select `STCode`,`DTName` from `countryData` WHERE `DTCode`= 000 ORDER BY FIELD(`STCode`,$request->code) DESC, `DTName`"));
                } else {
                    $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();
                }
            }
            else{
                $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();
            }
            ResponseMessage::success('Success', $states);
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function city(Request $request) {
        try {
            $id = trim(strip_tags($request->state_id));
            $cities=[];
            if($request->code!=""){
                if(CountryData::where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' .$id)->exists()){
                    $cities = DB::select(DB::raw("select `DTCode`,`DTName` from `countryData` WHERE `SDTCode`= 00000 AND `DTCode`!= 000 AND `STCode`= $id ORDER BY FIELD(`DTCode`,$request->code) DESC,`DTName`"));
                } else {
                    $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();
                }
            }
            else{
                $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $id)->orderBy('DTName', 'ASC')->get();
            }
            ResponseMessage::success('Success', $cities);
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function tehsil(Request $request) {
        try {
            $tehsils=[];
            $id = trim(strip_tags($request->city_id));
            if($request->code!=""){
                if(CountryData::where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->exists()){
                    $tehsils = DB::select(DB::raw("select `SDTCode`,`SDTName` from `countryData` WHERE `TVCode`= 000000 AND `SDTCode`!= 00000 AND `DTCode`= $id ORDER BY FIELD(`SDTCode`,$request->code) DESC,`SDTName`"));
                } else {
                    $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
                }
            }
            else{
                $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
            }
            ResponseMessage::success('Success', $tehsils);
        } catch (Exception $e) {
            log::error($e);
        }
    }
    public function village(Request $request) {
        try {
            $id = trim(strip_tags($request->tehsil_id));
            if($request->code!=""){
                if(CountryData::where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->exists()){
                    $villages = DB::select(DB::raw("select `TVCode`,`Name` from `countryData` WHERE `TVCode`!= 000000 AND `SDTCode`= $id ORDER BY FIELD(`TVCode`,$request->code) DESC,`Name`"));
                } else {
                    $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                }
            }
            else{
                $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
            }
            
            ResponseMessage::success('Success', $villages);
        } catch (Exception $e) {
            log::error($e);
        }
    }
}
