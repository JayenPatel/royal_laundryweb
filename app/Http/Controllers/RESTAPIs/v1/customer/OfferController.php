<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Offers\Offer;
use App\Helper\ResponseMessage;

class OfferController extends Controller
{
    public function offer(Request $request) {
    	try {
    		// $userid = trim(strip_tags($request->userid));
    		// $lan_id = trim(strip_tags($request->userid));
    		$current_date =  date('Y-m-d');
    		date_default_timezone_set('Asia/Kolkata');
    		$current_time = date( 'H:i:s');
			$offers = Offer::select('id', 'cover')
							->where('expired_date', '>=', $current_date)
							// ->where('expired_time', '>=', $current_time)
							->where('status', 1)
							->get();
			if($offers) {
				ResponseMessage::success("Success", $offers);
			}
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
}
