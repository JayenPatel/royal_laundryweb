<?php

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Khedutbolo Swagger API ",
 *      description="L5 Swagger OpenApi description",
 *      @OA\Contact(
 *          email="shineinfosoft22@gmail.com"
 *      ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 * @OA\Tag(
 *     name="project",
 *     description="Everything about your Projects",
 * )
 *  @OA\Server(
 *      url="https://localhost:8000/api",
 *      description="Khedutbolo Swagger Server"
 * )
 * @OA\SecurityScheme(
 *     type="apiKey",
 *     name="khediutbolo",
 *     in="header",
 *     securityScheme="12345678",
 * )
 */

namespace App\Http\Controllers\RestAPIs\v1\customer;
use App\Http\Controllers\Controllers;

 Class Controllers extends \App\Http\Controllers\Controller
 {

 }

 ?>