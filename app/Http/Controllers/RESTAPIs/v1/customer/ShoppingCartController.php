<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\OrderProducts\OrderProduct;
use App\Helper\ResponseMessage;
use App\Shop\Customers\Customer;
use App\Shop\Addresses\Address;
use App\Shop\Products\Product;
use App\Shop\Payments\Payment;
use App\Shop\States\State;
use App\Shop\Orders\Order;
use App\Shop\Carts\Cart;
use \stdClass;
use Validator;
use Log;
use App\Shop\Products\Product_attributes;
use App\Shop\Notifications\Notification;

class ShoppingCartController extends Controller
{
    public function payment(Request $request) {
		try {
			$rules = [
    			'device' => 'required',
    			'amount' => 'required',
    			'transaction_id' => 'required',
    			'payment_type' => 'required',
    			'userid' => 'required',
    			'status' => 'required',
    		];
    		$customeMessage = [
    			'device.required' => 'Please enter device.',
    			'amount.required' => 'Please enter amount.',
    			'payment_type.required' => 'Please enter payment type.',
    			'transaction_id.required' => 'Please enter transaction id.',
    			'userid.required' => 'Please enter user id.',
    			'status.required' => 'Please enter status.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$device = trim(strip_tags($request->device));
	        	$amount = trim(strip_tags($request->amount));
	        	$transaction_id = trim(strip_tags($request->transaction_id));
	        	$payment_type = trim(strip_tags($request->payment_type));
	        	$userid = trim(strip_tags($request->userid));
	        	$status = trim(strip_tags($request->status));

	        	$payment = new Payment();
	        	$payment->device = $device;
	        	$payment->amount = $amount;
	        	$payment->transaction_id = $transaction_id;
	        	$payment->payment_type = $payment_type;
	        	$payment->userid = $userid;
	        	$payment->status = $status;
	        	if($payment->save()){
	        		ResponseMessage::success('Success', $payment);
		        } else {
		        	ResponseMessage::error('Somethig was wrong please try again!!');
		        }
	        }
		} catch (Exception $e) {
			log::error($e);
		}
	}

	public function order(Request $request) {
		try {
			$rules = [
    			'alias' => 'required',
    			'mobile' => 'required|numeric',
    			'zip' => 'required|numeric',
    			'street' => 'required',
    			'city' => 'required',
    			'state'=>'required',
                'tid' => 'required',
                'vid' => 'required',
    			'payment' => 'required',
    			'customer_id' =>'required',
    		];
    		$customeMessage = [
    			'alias.required' => 'Please enter alias.',
    			'mobile.required' => 'Please enter mobile number.',
    			'mobile.numeric' => 'Please enter number only.',
    			'zip.required' => 'Please enter pin code.',
    			'zip.numeric' => 'Please enter number only.',
    			'street.required' => 'Please enter socity/street.',
    			'city.required' => 'Please enter city',
                'tid.required' => 'Please enter tehsil',
                'vid.required' => 'Please enter village',
    			'state.required' => 'Please enter state',
    			'full_address.required' => 'Please enter full_address.',
    			'payment.required' => 'Please select payment method.',
    			'customer_id.required' => 'Please enter customer id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$alias = trim(strip_tags($request->alias));
	        	$mobile = trim(strip_tags($request->mobile));
	        	$zip = trim(strip_tags($request->zip));
	        	$street = trim(strip_tags($request->street));
	        	$city = trim(strip_tags($request->city));
	        	$state = trim(strip_tags($request->state));
                $tid = trim(strip_tags($request->tid));
                $vid = trim(strip_tags($request->vid));
	        	$full_address = trim(strip_tags($request->full_address));
	        	$payment = trim(strip_tags($request->payment));
	        	$customer_id = trim(strip_tags($request->customer_id));
	        	if(isset($request->mail)){
	        		$email = trim(strip_tags($request->mail));
	        	}
	        	if(Cart::where('customer_id', $customer_id)->where('status',1)->exists()){
		        	$address = new Address();
		        	$address->alias = $alias;
		        	$address->address_1 = $street;
		        	$address->address_2 = $full_address;
		        	$address->zip = $zip;
		        	$address->city = $city;
		        	$address->state_code = $state;
		        	$address->tehsil_id = $tid;
		        	$address->village_id = $vid;
		        	$address->phone = $mobile;
		        	$address->country_id = 99;
		        	$address->customer_id = $customer_id;
		        	$address->save();

		        	$address_id = Address::where('customer_id', $customer_id)->orderBy('id', 'DESC')->first();
		        	$total_price=0;
		        	$shipping_price=0;
		        	$products = Cart::where('customer_id', $customer_id)
			        					->where('status',1)
			        					->get();
		        	foreach($products as $prod) {
			        	$product = Product_attributes::find($prod->attribute_id);
			        	if($prod->quantity > $product->available_quantity){
			        		$productname = Product::find($prod->product_id);
			        		ResponseMessage::error('Currently '.$productname->name.' product quantity not available.');
			        	}
			        	$total_price=$total_price+($product->sale_price*$prod->quantity);
			        }
			     //   dd($shipping_price);

		        	$reference = Order::max('id')+1;
		        	$order = new Order();
		        	$order->reference = $reference;
		        	$order->customer_id = $customer_id;
		        	if(isset($request->mail)){
		        		$order->email = $email;
		        	}
		        	$order->address_id = $address_id->id;
		        	$order->payment = $payment;
		        	$order->order_status_id = 3;
		        	$order->total = $total_price;
		        	$order->courier_id = 1;
		        	$order->total_products = $total_price;
		        	$order->mail_status=1;
		        	date_default_timezone_set('Asia/Kolkata');
                    $order->created_at = date("Y-m-d H:i:s");
		        	$order->save();


			        $order_id = Order::where('customer_id', $customer_id)->orderBy('id', 'DESC')->first();
			        
		        	foreach($products as $prod) {
			        	$product = Product::find($prod->product_id);
			        	$orderproduct = new OrderProduct();
			        	$orderproduct->order_id = $order_id->id;
			        	$orderproduct->product_id = $product->id;
			        	$orderproduct->quantity = $prod->quantity;
			        	$orderproduct->product_name = $product->name;
			        	$orderproduct->product_sku = $product->sku;
			        	$orderproduct->product_description = $product->description;
			        	$orderproduct->product_price = $product->price;
			        	$orderproduct->attribute_id = $prod->attribute_id;
			        	if($product->shipping_price){
			        	    $orderproduct->shipping_price = $product->shipping_price;
			        	    $shipping_price=$shipping_price+$product->shipping_price;
			        	}
			        	if($orderproduct->save()) {
			        		$attribute = Product_attributes::find($prod->attribute_id);
			        		$available_quantity = $attribute->available_quantity-$prod->quantity;
			        		$attribute->available_quantity =$available_quantity;
			        		$attribute->save();

			        		$notification = new Notification();
							$notification->customer_id=$customer_id;
							$notification->notify_detail="Product ordered Successfully";
							date_default_timezone_set('Asia/Kolkata');
							$notification->time=date("Y-m-d H:i:s");
							$notification->save();
							
			        		$carts = Cart::where('customer_id', $customer_id)
			        					->where('product_id',$product->id)
			        					->where('attribute_id',$prod->attribute_id)
			        					->where('status',1)
			        					->get();
			        		foreach($carts as $cart){
			        		    $cart->delete();
			        		}
			        	}

		        	}
		        	$order_id->total_shipping = $shipping_price;
		        	$order_id->save();
		        	ResponseMessage::success('Success', "You have placed Order successfully");
		        } else {
		        	ResponseMessage::error('Cart is empty!!!!');
		        }
	        }
		} catch (Exception $e) {
			log::error($e);
		}
	}

    public function currentOrder(Request $request) {
    	try {
    		$rules = [
    			'user_id' => 'required',
    		];
    		$customeMessage = [
    			'user_id.required' => 'Please enter user id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$data = array();
		    	$admin_id = trim(strip_tags($request->user_id));
		    	if(Order::where('customer_id', $admin_id)->exists()) {
			    	$orders = Order::where('customer_id', $admin_id)
			    					->where('order_status_id', 3)
			    					->orWhere('order_status_id', 2)
			    					->orderBy('orders.id', 'DESC')
			    					->get();
			    					// dd($order);
			    	if($orders) {
			    		foreach ($orders as $order) {
			    			$products = Product::join('order_product', 'order_product.product_id', 'products.id')
			    								->join('orders', 'orders.id', 'order_product.order_id')
			    								->join('product_attribute','product_attribute.id','order_product.attribute_id')
		    									->Select('orders.id as order_id','order_product.product_name', 'product_attribute.price', 'order_product.quantity', 'product_attribute.sale_price', 'orders.created_at', 'products.cover')
		    									->where('order_product.order_id', $order->id)
		    									->get();
		    				foreach($products as $product) {
		    					array_push($data, $product);
		    				}
			    		}
		    			ResponseMessage::success('Success', $data);
			    	} else {
			    		ResponseMessage::error('Order not found.');
			    	}
			    } else {
			    	ResponseMessage::error('Order not found.');
			    }
		    }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }

    public function completeOrder(Request $request) {
    	try {
    		$rules = [
    			'user_id' => 'required',
    		];
    		$customeMessage = [
    			'user_id.required' => 'Please enter user id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$data = array();
		    	$admin_id = trim(strip_tags($request->user_id));
		    	if(Order::where('customer_id', $admin_id)->exists()) {
			    	$orders = Order::where('customer_id', $admin_id)
			    					->where('order_status_id', 1)
			    					->orderBy('id', 'DESC')
			    					->get();
			    	if($orders) {
			    		foreach($orders as $order) {
			    			$products = Product::join('order_product', 'order_product.product_id', 'products.id')
			    				->join('orders', 'orders.id', 'order_product.order_id')
			    				->join('product_attribute','product_attribute.id','order_product.attribute_id')
			    				->Select('orders.id as order_id','order_product.product_name', 'product_attribute.price', 'order_product.quantity', 'product_attribute.sale_price', 'orders.created_at', 'products.cover')
			    				->where('order_product.order_id', $order->id)
			    				->get();
			    			foreach($products as $product) {
		    					array_push($data, $product);
		    				}
			    		}
			    		ResponseMessage::success('Success', $data);
			    	} else {
			    		ResponseMessage::error('Order not found.');
			    	}
			    } else {
			    	ResponseMessage::error('Order not found.');
			    }
		    }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }

    public function showCancelOrder(Request $request) {
    	try {
    		$rules = [
    			'user_id' => 'required',
    		];
    		$customeMessage = [
    			'user_id.required' => 'Please enter user id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$data = array();
		    	$admin_id = trim(strip_tags($request->user_id));
		    	if(Order::where('customer_id', $admin_id)->exists()) {
			    	$orders = Order::where('customer_id', $admin_id)
			    					->where('order_status_id', 4)
			    					->orderBy('id', 'DESC')
			    					->get();
			    	if($orders) {
			    		foreach($orders as $order) {
			    			$products = Product::join('order_product', 'order_product.product_id', 'products.id')
			    			->join('orders', 'orders.id', 'order_product.order_id')
			    			->join('product_attribute','product_attribute.id','order_product.attribute_id')
			    			->Select('orders.id as order_id','order_product.product_name', 'product_attribute.price', 'order_product.quantity', 'product_attribute.sale_price', 'orders.created_at', 'products.cover')
			    			->where('order_product.order_id', $order->id)
			    			->get();
			    			foreach($products as $product) {
		    					array_push($data, $product);
		    				}
			    		}
			    		ResponseMessage::success('Success', $data);
			    	} else {
			    		ResponseMessage::error('Order not found.');
			    	}
			    } else {
			    	ResponseMessage::error('Order not found.');
			    }
		    }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }

    public function cancelOrder(Request $request) {
    	try {
    		$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter order id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
		    	$id = trim(strip_tags($request->id));
		    	if(Order::where('id', $id)->exists()) {
			    	$order = Order::where('id', $id)	
			    					->where('order_status_id', '!=', 1)
			    					->where('order_status_id', '!=', 4)
			    					->first();
			    	if($order) {
			    		$order->order_status_id = 4;
			    		if($order->save()){
				    		$product = OrderProduct::where('order_id', $id)->get();
				    		foreach ($product as $prod) {
				    			$attribute = Product_attributes::find($prod->attribute_id);
				    			$quantity = $attribute->available_quantity + $prod->quantity;
				    			$attribute->available_quantity = $quantity;
				    			$attribute->save();
				    		}
			    			$response = Product::join('order_product', 'order_product.product_id', 'products.id')
			    				->join('orders', 'orders.id', 'order_product.order_id')
			    				->join('product_attribute','product_attribute.id','order_product.attribute_id')
			    				->Select('orders.id as order_id', 'order_product.product_name', 'product_attribute.price', 'order_product.quantity', 'product_attribute.sale_price', 'orders.created_at', 'products.cover')->where('order_product.order_id', $order->id)
			    				->get();
			    			ResponseMessage::success('Success', $response);
			        	} else{
			        		ResponseMessage::error("Something went wrong please try again.");	
			        	}
			    	} else {
			    		ResponseMessage::error('Order not found.');
			    	}
			    } else {
			    	ResponseMessage::error('Order not found.');
			    }
		    }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }


    public function deleteProductCart(Request $request) {
    	try {
    		$rules = [
    			'product_id' => 'required',
    			'customer_id' => 'required',
    			'attribute_id' => 'required',
    		];
    		$customeMessage = [
    			'product_id.required' => 'Please enter product id.',
    			'customer_id.required' => 'Please enter customer id.',
    			'attribute_id.required' => 'Please enter attribute id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	    		$product_id = trim(strip_tags($request->product_id));
	    		$customer_id = trim(strip_tags($request->customer_id));
	    		$attribute_id = trim(strip_tags($request->attribute_id));

	    		if(Cart::where('product_id', $product_id)->where('customer_id', $customer_id)->where('attribute_id', $attribute_id)->where('status', 1)->exists()) {
		    		$product = Cart::where('product_id', $product_id)
		    						->where('customer_id', $customer_id)
		    						->where('attribute_id', $attribute_id)
		    						->where('status', 1)
		    						->first();
		    		if($product->delete()){
		        		$notification = new Notification();
						$notification->customer_id=$customer_id;
						$notification->notify_detail="Product deleted to cart successfully";
						date_default_timezone_set('Asia/Kolkata');
						$notification->time=date("Y-m-d H:i:s");
						$notification->save();
		    			ResponseMessage::success('Success', "Product delete successfully.");
		    		} else {
		    			ResponseMessage::error("Something went wrong please try again.");
		    		}
		    	} else {
		    		ResponseMessage::error("Product does not found!!!.");
		    	}
	    	}
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }

    public function addCart(Request $request) {
    	try {
    		$rules = [
    			'customer_id' => 'required',
    			'product_id' => 'required',
    			'attribute_id' => 'required',
    			'quantity' => 'required|numeric',
    			'total_price' => 'required',
    		];
    		$customeMessage = [
    			'customer_id.required' => 'Please enter user id.',
    			'product_id.required' => 'Please enter product id.',
    			'attribute_id.required' => 'Please enter attribute id.',
    			'quantity.required' => 'Please enter quantity.',
    			'quantity.numeric' => 'Please enter number.',
    			'total_price.required' => 'Please enter total price.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
		    	$customer_id = trim(strip_tags($request->customer_id));
		    	$product_id = trim(strip_tags($request->product_id));
		    	$attribute_id = trim(strip_tags($request->attribute_id));
		    	$quantity = trim(strip_tags($request->quantity));
		    	$total_price = trim(strip_tags($request->total_price));
		    	if(Customer::where('id',$customer_id)->exists()) {
		    		if(Product::where('id',$product_id)->exists()) {
		    			if(Product_attributes::where('id',$attribute_id)->where('product_id',$product_id)->exists()){
		    				$Product_attributes = Product_attributes::where('id',$attribute_id)->where('product_id',$product_id)->first();
				    		if($quantity < $Product_attributes->available_quantity){
					    		if(Cart::where('customer_id',$customer_id)->where('product_id', $product_id)->where('attribute_id',$attribute_id)->exists()) {
					    			$cart = Cart::where('customer_id',$customer_id)->where('product_id', $product_id)->where('attribute_id',$attribute_id)->first();
					    		} else {
					    			$cart = new Cart();
					    		}
					    		$cart->customer_id = $customer_id;
					    		$cart->product_id = $product_id; 
					    		$cart->quantity = $quantity;
					    		$cart->total_price=$total_price;
					    		$cart->status = 1;
					    		$cart->attribute_id = $attribute_id; 
					    // 		dd($cart);
					    		if($cart->save()) {
					        		$notification = new Notification();
									$notification->customer_id=$customer_id;
									$notification->notify_detail="Product added to cart successfully";
									date_default_timezone_set('Asia/Kolkata');
									$notification->time=date("Y-m-d H:i:s");
									$notification->save();
					    			ResponseMessage::success('Success', 'Product added successfully in the cart');
					    		} else {
					    			ResponseMessage::error('Somethig was wrong please try again');
					    		}
					    	}else {
					    		ResponseMessage::error('Product quantity not available');
					    	}
				    	} else {
				    		ResponseMessage::error('Product attribute not found');
				    	}
			    	} else {
			    		ResponseMessage::error('Product not found');
			    	}
		    	} else {
		    		ResponseMessage::error('Customer not found');
		    	}
		    }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    
    public function showCart(Request $request) {
    	try {
    		$rules = [
    			'customer_id' => 'required',
    		];
    		$customeMessage = [
    			'customer_id.required' => 'Please enter user id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
		    	$customer_id = trim(strip_tags($request->customer_id));
		    	if(Customer::where('id',$customer_id)->exists()) {
		    		$carts = Cart::join('products', 'products.id', 'cart.product_id')
		    						->join('employees', 'employees.id', 'products.company_id')
		    						->Select('products.id', 'products.name', 'products.price', 'cart.quantity', 'cart.product_size', 'cart.attribute_id', 'products.sale_price', 'cart.total_price', 'products.cover', 'employees.name as dealer_name')
		    					   ->where('cart.status', 1)
		    					   ->where('cart.customer_id', $customer_id)
		    					   ->get();
		    		$total =0;
		    		foreach ($carts as $cart) {
		    			$attribute = Product_attributes::where('id',$cart->attribute_id)
		    											->where('product_id',$cart->id)
		    											->first();
		    			if($attribute){
    		    			$cart->price = $attribute->price;
    		    			$cart->sale_price = $attribute->sale_price;
    		    			$total = $total+($cart->sale_price*$cart->quantity);
		    			} else {
		    			    ResponseMessage::error('Product attribute not found.');
		    			}
		    		}
		    		$product['total']=$total;
		    		$product['carts']=$carts;
		    		ResponseMessage::success('Success', $product);
		    	}  else {
		    		ResponseMessage::error('Customer not found.');
		    	}
		    }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
    
    public function updateQuntity(Request $request) {
		try {
			$rules = [
    			'customer_id' => 'required',
    			'product_id' => 'required',
    			'quantity' => 'required|numeric',
    		];
    		$customeMessage = [
    			'customer_id.required' => 'Please enter user id.',
    			'product_id.required' => 'Please enter product id.',
    			'quantity.required' => 'Please enter quantity.',
    			'quantity.numeric' => 'Please enter only number.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$customer_id = trim(strip_tags($request->customer_id));
	        	$product_id = trim(strip_tags($request->product_id));
	        	$quantity = trim(strip_tags($request->quantity));
	        	if(Cart::where('customer_id', $customer_id)->where('product_id', $product_id)->exists()) {
	        		$product = Cart::where('customer_id', $customer_id)
	        					->where('product_id', $product_id)
	        					->where('status', 1)
	        					->first();
		        	$product->quantity = $quantity;
		        	if($product->save()) {
		        		ResponseMessage::success('Success', 'Product quantity update successfully.');
		        	} else {
		        		ResponseMessage::error('Somethig was wrong please try again!!');
		        	}
		        } else {
		        	ResponseMessage::error('Customer not found!!');
		        }
	        }
		} catch (Exception $e) {
			log::error($e);
		}
	}

	public function cartProductCount(Request $request){
		// dd($request);
		try {
			$rules = [
    			'customer_id' => 'required',
    		];
    		$customeMessage = [
    			'customer_id.required' => 'Please enter user id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$customer_id = trim(strip_tags($request->customer_id));
	        	if(Cart::where('customer_id', $customer_id)->exists()) {
	        		$count = Cart::where('customer_id', $customer_id)->count();
	        		ResponseMessage::success('Success', $count);
	        	} else {
	        		ResponseMessage::error('Customer not found!!');
	        	}
	        }
		} catch (Exception $e) {
			log::error($e);
		}
	}
}
