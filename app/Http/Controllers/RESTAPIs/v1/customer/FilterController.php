<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\Employees\Employee;
use Log;
use Validator;
use App\Helper\ResponseMessage;
use \stdClass;
use App\Shop\Products\Product_attributes;

class FilterController extends Controller
{
    public function filter(Request $request) {
    	try {
    		$data = array();
            $technical_prod=[];
        	$technicals = $request->technicals;
        	$companies = $request->companies;
            $loopdata;
    		if($companies && $technicals) {
                if(count($technicals) > count($companies)) {
                    $loopdata = count($technicals);
                } else if(count($technicals) < count($companies)) {
                    $loopdata = count($companies);
                }else {
                    $loopdata = count($companies);
                }
                for ($i=1; $i <= $loopdata; $i++) { 
    				$query =  Product::join('employees', 'employees.id', 'products.company_id')->join('categories', 'categories.id', 'products.subcategory_id')->Select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'products.technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name as category_name')->where('products.status',1)->where('employees.status',1);
                    if(isset($technicals[$i])){
						$query->where('products.technical_name', $technicals[$i]);
                    }
                    if(isset($companies[$i])){
        				$query->where('employees.id', $companies[$i]);
                    }
                    $technical_prod  = $query->get();

        			foreach ($technical_prod as $com) {
                        $attr = Product_attributes::where('product_id',$com->id)->first();
                        if($attr){
                            $com->price = $attr->price;
                            $com->sale_price = $attr->sale_price;
                        }
        			}
    			}
                for($j=0; $j<count($technicals);$j++) {
                    for($i=0; $i<count($technical_prod);$i++) {
                        if($technicals[$j]==$technical_prod[$i]->technical_name){
                            array_push($data, $technical_prod[$i]);
                        }
                    }  
                }
        	} else {
        		if($companies) {
        			foreach($companies as $company){
        				$id = $company;
        				$company_prod = Employee::join('products', 'products.company_id', 'employees.id')
	        							->join('categories', 'categories.id', 'products.subcategory_id')
	        							->Select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'products.technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name as category_name')
	        							->where('employees.id', $id)
                                        ->where('products.status',1)
	        							->get();
	        			foreach ($company_prod as $com) {
                            $attr = Product_attributes::where('product_id',$com->id)->first();
                            if($attr){
                                $com->price = $attr->price;
                                $com->sale_price = $attr->sale_price;
                            }
	        				array_push($data, $com);
	        			}
        			}
        		} else {
        			foreach($technicals as $technical){
        				$technical = $technical;
        				$technical_prod =  Product::join('employees', 'employees.id', 'products.company_id')
	        							->join('categories', 'categories.id', 'products.subcategory_id')
	        							->Select('products.id', 'products.name', 'employees.name as dealer_name', 'employees.mobile', 'products.technical_name', 'products.price', 'products.sale_price', 'products.additional_info', 'products.cover', 'categories.name as category_name')
	        							->where('products.technical_name', $technical)
                                        ->where('products.status',1)
	        							->get();
	        			foreach ($technical_prod as $com) {
                            $attr = Product_attributes::where('product_id',$com->id)->first();
                            if($attr){
                                $com->price = $attr->price;
                                $com->sale_price = $attr->sale_price;
                            }
	        				array_push($data, $com);
	        			}
        			}
        		}
        	}
        	ResponseMessage::success('Success', $data);
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
}
