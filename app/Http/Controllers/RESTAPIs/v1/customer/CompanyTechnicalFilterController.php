<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;
use Validator;
use App\Helper\ResponseMessage;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\Employees\Employee;
use \stdClass;

class CompanyTechnicalFilterController extends Controller
{
    public function companyTechnicalFilter(Request $request) {
    	try {
    		$data = array();
    		$crop_id = $request->crop_id;

			// $companies = Employee::select('id', 'name')->get();
			$companies = Product::select('company_id', 'employees.name')
								->leftjoin('employees', 'employees.id','products.company_id')
								->where('crop_category', $crop_id)
								->where('employees.status',1)
								->distinct()
								->get();
			$company = new stdClass();
			$company->type = 'companies';
			$company->data = $companies;
			array_push($data, $company);

			// $techicals = Product::select('technical_name')->distinct()->get();
			$techicals = Product::select('technical_name')
								->where('crop_category', $crop_id)
								->distinct()
								->get();
			$techical = new stdClass();
			$techical->type = 'techicals';
			$techical->data = $techicals;
			array_push($data, $techical);

			ResponseMessage::success('Success', $data);
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
}
