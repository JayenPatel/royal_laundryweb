<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\RestAPIs\v1\customer\Controllers;
use Log;
use Validator;
use App\Helper\ResponseMessage;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\OrderProducts\OrderProduct;
use App\Shop\Carts\Cart;
use App\Shop\Products\Product_attributes;
use App\Shop\Employees\Employee;
use App\Shop\TechnicalCategories\Technical;

class ProductController extends Controller
{


    Public function productDetail(Request $request){
        try {
            $rules = [
                'id' => 'required',
                'customer_id' => 'required',
            ];
            $customeMessage = [
                'id.required' => 'Please enter product id.',
                'customer_id.required' => 'Please enter customer id.',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $id = trim(strip_tags($request->id));
                $customer_id = trim(strip_tags($request->customer_id));
                if(Product::where('id', $id)->exists()){
                    $product = Product::join('employees', 'employees.id', 'products.company_id')
                                        ->Select('products.id', 'products.name', 'employees.name as company_name', 'employees.mobile as mobile', 'products.status', 'products.weight', 'products.shipping_price', 'products.cover', 'products.additional_info')
                                        ->where('products.id', $id)
                                        ->where('products.status',1)
                                        ->first();
                    $infos = Product_attributes::select('key','value','quantity', 'price','sale_price', 'available_quantity','id')->where('product_id',$id)->get();
                    $product->unit = $infos;
                    $user_quantity=0;
                    foreach ($infos as $info) {
                        $i=0;
                        if(Cart::where('customer_id', $customer_id)->where('product_id', $id)->exists()){
                            $carts = Cart::where('customer_id', $customer_id)
                                        ->where('product_id', $id)
                                        ->get();
                            foreach ($carts as $cart) {
                                if($info->id == $cart->attribute_id){
                                    $user_quantity = $cart->quantity;
                                } else {
                                    $user_quantity = 0;
                                }
                            }
                        } else {
                            $user_quantity = 0;
                        }
                        $info->user_quantity=$user_quantity;
                        $i++;
                    }
                    if($product->additional_info) {
                        $decode = json_decode($product->additional_info);
                        $product->additional_info = $decode;
                        foreach($product->additional_info as $info){
                            if(!$info->key){
                                $product->additional_info = [];
                            }
                        }
                    }else {
                        $product->additional_info = [];
                    }
                    ResponseMessage::success('Success', $product);
                } else {
                    ResponseMessage::error('Product not found.');
                }
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function productAttribute(Request $request){
        try {
            $rules = [
                'product_id' => 'required',
                'attribute_id' => 'required',
                'customer_id' => 'required',
            ];
            $customeMessage = [
                'product_id.required' => 'Please enter product id.',
                'attribute_id.required' => 'Please enter attribute id.',
                'customer_id.required' => 'Please enter customer id.',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $product_id = trim(strip_tags($request->product_id));
                $attribute_id = trim(strip_tags($request->attribute_id));
                $customer_id = trim(strip_tags($request->customer_id));
                if(Product::where('id', $product_id)->exists()){
                    if (Product_attributes::where('product_id',$product_id)->where('id',$attribute_id)->exists()){
                        $attribute = Product_attributes::where('product_id',$product_id)->where('id',$attribute_id)->first();
                        if(Cart::where('customer_id',$customer_id)->where('product_id', $product_id)->where('attribute_id',$attribute_id)->exists()) 
                        {
                            $cart = Cart::where('customer_id',$customer_id)->where('product_id', $product_id)->where('attribute_id',$attribute_id)->first();
                            $attribute->user_quantity = $cart->quantity;
                        } else {
                            $attribute->user_quantity = 0;
                        }
                        // dd($attribute);
                        ResponseMessage::success('Success', $attribute);
                    } else {
                        ResponseMessage::error('Product attribute not found.');
                    }
                } else {
                    ResponseMessage::error('Product not found.');
                }    
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function similarProducts(Request $request){
        try {
            $rules = [
                'id' => 'required',
            ];
            $customeMessage = [
                'id.required' => 'Please enter product id.',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $products = array();
                $i=1;
                $id = trim(strip_tags($request->id));
                if(Product::where('id', $id)->exists()){
                    $product = Product::where('id', $id)->first();
                    if(Product::where('subcategory_id', $product->subcategory_id)->exists()){
                        $similarProducts = Product::join('employees', 'employees.id', 'products.company_id')
                                                ->Select('products.id','products.name', 'products.price', 'products.company_id', 'employees.name as product_dealer', 'products.cover', 'products.sale_price')
                                                ->where('products.subcategory_id', $product->subcategory_id)
                                                ->where('products.id', '!=', $product->id)
                                                ->where('products.status', 1)
                                                ->get();
                        foreach ($similarProducts as $similarProduct) {
                            $attr = Product_attributes::where('product_id',$similarProduct->id)->first();
                            if($attr){
                                $similarProduct->price = $attr->price;
                                $similarProduct->sale_price = $attr->sale_price;
                            }
                            array_push($products, $similarProduct);
                        }
                        ResponseMessage::success('Success', $products);
                    } else {
                        ResponseMessage::success('Similar product not found.');
                    }
                } else {
                    ResponseMessage::error('Product not found.');
                }
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function companyProducts(Request $request){
        try {
            $rules = [
                'company_id' => 'required',
            ];
            $customeMessage = [
                'company_id.required' => 'Please enter company id.',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                $errors = $validator->errors();
                ResponseMessage::error($errors->first());
            } else {
                $company_id = trim(strip_tags($request->company_id));
                $technicals = Category::where('parent_id',null)->get();
                $data=[];
                if(Employee::where('id', $company_id)->exists()){
                    if(Product::where('company_id', $company_id)->exists()){
                        $products = Product::join('employees', 'employees.id', 'products.company_id')
                                            ->leftjoin('categories', 'categories.id', 'products.category_id')
                                            ->Select('products.id', 'products.name', 'products.price', 'products.sale_price', 'employees.name as company_name', 'categories.name as category_name', 'products.cover')
                                            ->where('employees.id', $company_id)
                                            ->where('employees.status', 1)
                                            ->get();
                        foreach ($products as $prod) {
                            $attr = Product_attributes::where('product_id',$prod->id)->first();
                            if($attr){
                                $prod->price = $attr->price;
                                $prod->sale_price = $attr->sale_price;
                            }
                        }
                        for($j=0; $j<count($technicals);$j++) {
                            for($i=0; $i<count($products);$i++) {
                                if($technicals[$j]->name==$products[$i]->category_name){
                                    array_push($data, $products[$i]);
                                }
                            }  
                        }
                        ResponseMessage::success('Success', $data);
                    } else {
                        ResponseMessage::error('Product not found.');
                    }
                } else {
                    ResponseMessage::error('Company not found.');
                }
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function traddingProducts(Request $request){
        try {
            if(OrderProduct::exists()){
                $product_detail = array();
                $product_ids = OrderProduct::select('product_id')->distinct()->get();
                foreach ($product_ids as $product_id) {
                    $count = OrderProduct::where('product_id', $product_id->product_id)->count();
                    $quantities = OrderProduct::select('quantity')->where('product_id', $product_id->product_id)->get();
                    $sale_quantity=0;
                    foreach ($quantities as $quantity) {
                        $sale_quantity = $sale_quantity+$quantity->quantity;
                    }
                    $data['sale_quantity'] = $sale_quantity;
                    $data['id ']= $product_id->product_id;
                    $data['order_product'] = $count;
                    $data['product'] = Product::join('employees', 'employees.id', 'products.company_id')
                                    ->leftjoin('categories', 'categories.id', 'products.category_id')
                                    ->Select('products.name', 'products.price', 'products.sale_price', 'employees.name as company_name', 'categories.name as category_name', 'products.cover')
                                    ->where('products.id', $product_id->product_id)->get();
                    array_push($product_detail, $data);
                 }
                rsort($product_detail);
                ResponseMessage::success('Success', $product_detail);
            } else {
                ResponseMessage::error('Product not found.');
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }

}
