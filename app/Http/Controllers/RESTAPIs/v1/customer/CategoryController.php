<?php

namespace App\Http\Controllers\RESTAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;
use Validator;
use App\Helper\ResponseMessage;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\Employees\Employee;
use App\Shop\Priorities\Priority;
use \stdClass;
use App\Shop\Priorities\TrendingProduct;
use App\Shop\Products\Product_attributes;
use App\Shop\TechnicalCategories\Technical;

class CategoryController extends Controller
{
	public function productCategories(Request $request) {
		try {
			$data = array();
			$comp = array();
			$cate = array();
			$sub_cat = array();
			if(Category::where('parent_id', null)->exists()) {
				$categories = Category::Select('id', 'name', 'cover')->where('parent_id', null)->get();
				foreach ($categories as $category) {
					$cate_add = new stdClass();
					$cate_add->id=$category->id;
					$cate_add->name=$category->name;
					$cate_add->cover=$category->cover;
					$cate_add->price=null;
					$cate_add->sale_price=null;
					$cate_add->company_name=null;
					array_push($cate, $cate_add);
				}
				$companies = Employee::Select('id', 'name', 'logo as cover')->get();
				foreach ($companies as $company) {
					$company_add = new stdClass();
					$company_add->id=$company->id;
					$company_add->name=$company->name;
					$company_add->cover=$company->cover;
					$company_add->price=null;
					$company_add->sale_price=null;
					$company_add->company_name=null;
					array_push($comp, $company_add);
				}

				$category = new stdClass();
				$category->type = 'Product Categories / Manage Products';
				$category->data = $cate;
				$priority = Priority::where('name', $category->type)->first();
				$category->priority = $priority->priority;
				array_push($data, $category);

				$company = new stdClass();
				$company->type = 'Company List';
				$company->data = $comp;
				$priority = Priority::where('name', $company->type)->first();
				$company->priority = $priority->priority;
				array_push($data, $company);

				$product = new stdClass();
				$product->type = 'Trending Products';
				$priority = Priority::where('name', $product->type)->first();
				$subcategories = TrendingProduct::select('priority','product_id')->get();
				foreach ($subcategories as $subcategory) {
					$sub_category_data = Product::join('employees', 'employees.id', 'products.company_id')
									->join('product_attribute','product_attribute.product_id','products.id')
									->Select('products.id', 'products.name', 'products.cover', 'product_attribute.price', 'product_attribute.sale_price', 'employees.name as company_name')
									->where('products.status',1)
									->where('products.id', $subcategory->product_id)
									->first();
					if($sub_category_data){
						array_push($sub_cat, $sub_category_data);
					}
				}
				$product->data = $sub_cat;
				// $product->data =[];
				$product->priority = $priority->priority;
				array_push($data, $product);
				ResponseMessage::success('Success', $data);
			} else {
				ResponseMessage::error('Category not found.');
			}
		} catch (Exception $e) {
			log::error($e);
		}
	}

	public function selectedCategory(Request $request) {
		try {
			$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter sub-category id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
				if(Category::where('id', $id)->exists()) {
	        		$category = array();
	                $about_product = array();
	                $i=1;
	                $technicals = Technical::get();
	                if(Product::where('subcategory_id', $id)->exists()){
	                    $solutions = Product::join('categories', 'categories.id', 'products.subcategory_id')
	                                        ->join('employees', 'employees.id', 'products.company_id')
	                                        ->Select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name')
	                                        ->where('products.subcategory_id', $id)
	                                        ->where('products.status',1)
	                                        ->get();
                        $technical = array();
                        foreach($solutions as $solution) {
                        	$attr = Product_attributes::where('product_id',$solution->id)->first();
                            if($attr){
                                $solution->price = $attr->price;
                                $solution->sale_price = $attr->sale_price;
                            }
                            if($solution->additional_info) {
                                $decode = json_decode($solution->additional_info);
                                $solution->additional_info = $decode;
                            }else {
                                $solution->additional_info = [];
                            }
                            // array_push($technical, $solution);
                        }
		                for($j=0; $j<count($technicals);$j++) {
		                    for($i=0; $i<count($solutions);$i++) {
		                        if($technicals[$j]->name==$solutions[$i]->technical_name){
		                            array_push($technical, $solutions[$i]);
		                        }
		                    }  
		                }
	                    $data = $technical;
		        		ResponseMessage::success('Success', $data);
		        	}else {
						ResponseMessage::error('Product not found!!');
					}
				} else {
					ResponseMessage::error('Category id not found!!');
				}
	        }
		} catch (Exception $e) {
			log::error($e);
		}
	}

	public function get_subCategory(Request $request) {
		try {
			$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter product id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
				if(Category::where('id', $id)->exists()) {
		        	$sub_categories = Category::select('id','name','cover')
		        								->where('parent_id', $id)
		        								->get();
		        	$extra=[];
		        	if(count($sub_categories)!=0){
		        		$extra['subcategory_status']=0;
		        		$data=$sub_categories;
		        	} else {
		        		$extra['subcategory_status']=1;
		        		$category = array();
		                $about_product = array();
		                $i=1;
                    	$technicals = Technical::get();
		                if(Product::where('subcategory_id', $id)->exists()){
		                    $solutions = Product::join('categories', 'categories.id', 'products.subcategory_id')
		                                        ->join('employees', 'employees.id', 'products.company_id')
		                                        ->Select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name')
		                                        ->where('products.subcategory_id', $id)
		                                        ->where('products.status',1)
		                                        ->get();
		                                        // dd($solutions);
	                        $technical = array();
	                        foreach($solutions as $solution) {
	                        	$attr = Product_attributes::where('product_id',$solution->id)->first();
	                            if($attr){
	                                $solution->price = $attr->price;
	                                $solution->sale_price = $attr->sale_price;
	                            }
	                            if($solution->additional_info) {
	                                $decode = json_decode($solution->additional_info);
	                                $solution->additional_info = $decode;
	                            }else {
	                                $solution->additional_info = [];
	                            }
	                            // array_push($technical, $solution);
	                        }
			                for($j=0; $j<count($technicals);$j++) {
			                    for($i=0; $i<count($solutions);$i++) {
			                        if($technicals[$j]->name==$solutions[$i]->technical_name){
			                            array_push($technical, $solutions[$i]);
			                        }
			                    }  
			                }
		                    $data = $technical;
			        	}
		        	}
		        	ResponseMessage::successor('Success', $data, $extra);
				} else {
					ResponseMessage::error('Category id not found!!');
				}
			}
		} catch (Exception $e) {
			log::error($e);
		}
	}

	public function get_subCategory_product(Request $request) {
		try {
			$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please enter category id.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
				if(Category::where('id', $id)->exists()) {
	        		$category = array();
	                $about_product = array();
	                $i=1;
                    $technicals = Technical::get();
                    dd($technicals);
	                if(Product::where('subcategory_id', $id)->exists()){
	                    $solutions = Product::join('categories', 'categories.id', 'products.subcategory_id')
	                                        ->join('employees', 'employees.id', 'products.company_id')
	                                        ->Select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name')
	                                        ->where('products.subcategory_id', $id)
	                                        ->where('products.status',1)
	                                        ->get();
                        $technical = array();
                        foreach($solutions as $solution) {
                        	$attr = Product_attributes::where('product_id',$solution->id)->first();
                            if($attr){
                                $solution->price = $attr->price;
                                $solution->sale_price = $attr->sale_price;
                            }
                            if($solution->additional_info) {
                                $decode = json_decode($solution->additional_info);
                                $solution->additional_info = $decode;
                            }else {
                                $solution->additional_info = [];
                            }
                        }
		                for($j=0; $j<count($technicals);$j++) {
		                    for($i=0; $i<count($solutions);$i++) {
		                        if($technicals[$j]==$solutions[$i]->technical_name){
		                            array_push($technical, $solutions[$i]);
		                        }
		                    }  
		                }
	                    $data = $technical;
		        	}
		        	ResponseMessage::success('Success', $data);
				} else {
					ResponseMessage::error('Category id not found!!');
				}
			}
		} catch (Exception $e) {
			log::error($e);
		}
	}
}