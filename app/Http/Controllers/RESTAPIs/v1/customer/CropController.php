<?php

namespace App\Http\Controllers\RestAPIs\v1\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\RestAPIs\v1\customer\Controllers;
use Log;
use Validator;
use App\Shop\Crops\Crop;
use App\Shop\Products\Product;
use App\Shop\CategoryProducts\CategoryProduct;
use App\Helper\ResponseMessage;
use App\Shop\Products\Product_attributes;
use App\Shop\TechnicalCategories\Technical;

class CropController extends Controller
{
    public function selectCrop(Request $request) {
    	try {
        	if(Crop::where('crop_category', null)->exists()){
        		$crops = Crop::select('id', 'name', 'image')->where('crop_category', null)->get();
        		ResponseMessage::success("Success", $crops);
        	} else {
        		ResponseMessage::error("Crop not found.");
        	}
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }

    public function selectCropSolution(Request $request) {
    	try {
    		$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please Select crop.',
    		];
    		$validator = Validator::make($request->all(),$rules,$customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	    		$id = trim(strip_tags($request->id));
	    		if(Crop::where('crop_category', $id)->exists()) {
	    			$cropSolutions = Crop::Select('id', 'name', 'image')->where('crop_category', $id)->get();
	    			ResponseMessage::success('Success', $cropSolutions);
	    		} else {
	    			ResponseMessage::error('Crop does not exist.');
	    		}
	    	}
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }

    public function selectSolution(Request $request) {
    	try {
    		$rules = [
    			'id' => 'required',
    		];
    		$customeMessage = [
    			'id.required' => 'Please Select crop.',
    		];
    		$validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
	            $errors = $validator->errors();
				ResponseMessage::error($errors->first());
	        } else {
	        	$id = trim(strip_tags($request->id));
                $category = array();
                $about_product = array();
                $i=1;
	        	if(Product::where('crop_id', $id)->exists()){
                    $technicals = Technical::get();
                    $solutions = Product::join('categories', 'categories.id', 'products.subcategory_id')
                                    ->join('employees', 'employees.id', 'products.company_id')
                                    ->Select('products.id', 'products.name', 'products.technical_name', 'products.price', 'products.sale_price', 'products.cover', 'products.additional_info', 'employees.name as dealer_name', 'employees.mobile', 'categories.name as category_name')
                                    ->where('products.crop_id', $id)
                                    ->where('products.status',1)
                                    ->get();
                    $technical = array();
                    foreach($solutions as $solution) {
                        $attr = Product_attributes::where('product_id',$solution->id)->first();
                        if($attr){
                            $solution->price = $attr->price;
                            $solution->sale_price = $attr->sale_price;
                        }
                        if($solution->additional_info) {
                            $decode = json_decode($solution->additional_info);
                            $solution->additional_info = $decode;
                        }else {
                            $solution->additional_info = [];
                        }
                        // array_push($technical, $solution);
                    }
                    for($j=0; $j<count($technicals);$j++) {
                        for($i=0; $i<count($solutions);$i++) {
                            if($technicals[$j]->name==$solutions[$i]->technical_name){
                                array_push($technical, $solutions[$i]);
                            }
                        }  
                    }
                    $data = $technical;
	        		ResponseMessage::success('Success', $data);
	        	} else {
	        		ResponseMessage::error('Solution does not exist.');
	        	}
	        }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    }
}
