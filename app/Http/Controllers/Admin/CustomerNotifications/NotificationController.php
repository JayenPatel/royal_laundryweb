<?php

namespace App\Http\Controllers\Admin\CustomerNotifications;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Employees\Employee;
use App\Shop\Addresses\Address;
use App\Shop\Customers\Customer;
use App\Shop\States\State;
use App\Shop\Cities\City;
use App\Shop\CustomerNotifications\CustomerNotification;
use Log;
use Validator;
use App\Shop\CountriesData\CountryData;

class NotificationController extends Controller
{
    public function index() {
        $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->orderBy('DTName', 'ASC')->get();
        // dd($cities);
    	$notifications = CustomerNotification::get();
    	return view('admin.customer-notifications.list', ['notifications' => $notifications]);
    }

    public function create() {
    	$companies = Employee::get();
    	return view('admin.customer-notifications.create', ['companies' => $companies]);
    }

    public function edit($id)
    {
        // $permission = Permission::permission('technical');
        // if($permission->edit==1) {
    		$notification = CustomerNotification::find($id);
    		$companies = Employee::get();
            $data=[];
            $value=[];
            $state=[];
            $city=[];
            if($notification->customer_type=='single_customer') {
                $customers = Employee::join('products', 'products.company_id', 'employees.id')
                                ->join('order_product', 'order_product.product_id', 'products.id')
                                ->join('orders', 'orders.id', 'order_product.order_id')
                                ->join('customers', 'customers.id', 'orders.customer_id')
                                ->select('customers.id', 'customers.name')
                                ->where('employees.id', $notification->company_id)
                                ->distinct()
                                ->get();
                foreach( $customers as $customer )
                {
                    $data[$customer->id] = $customer->name;
                }
            }
            if($notification->customer_type=='state_customer' || $notification->customer_type=='city_customer' || $notification->customer_type=='tehsil_customer' || $notification->customer_type=='village_customer') {
                $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->orderBy('DTName', 'ASC')->get();
                foreach( $states as $state )
                {
                    $data[$state->STCode] = $state->DTName;
                }
                if($notification->customer_type!='state_customer'){
                    $value['state'] = $data;
                }
            }
            if($notification->customer_type=='city_customer' || $notification->customer_type=='tehsil_customer' || $notification->customer_type=='village_customer') {
                $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->where('STCode', 'like', '%' . $notification->state_id)->orderBy('DTName', 'ASC')->get();
                $data=[];
                foreach( $cities as $city )
                {
                    $data[$city->DTCode] = $city->DTName;
                }
                if($notification->customer_type!='city_customer'){
                    $value['city'] = $data;
                }
            }
            if($notification->customer_type=='tehsil_customer' || $notification->customer_type=='village_customer') {
                $data=[];
                $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$notification->city_id)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
                foreach( $tehsils as $tehsil )
                {
                    $data[$tehsil->SDTCode] = $tehsil->SDTName;
                }
                if($notification->customer_type!='tehsil_customer'){
                    $value['tehsil'] = $data;
                }
            }
            if($notification->customer_type=='village_customer') {
                $data=[];
                $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$id)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
                foreach( $villages as $village )
                {
                    $data[$village->TVCode] = $village->Name;
                }
            }
    		return view('admin.customer-notifications.create', [
    			'companies' => $companies,
    			'notification' => $notification,
                'data' => $data, 
                'values' => $value
    		]);
        // } else {
        //     return view('layouts.errors.403');
        // }
    }

    public function store(Request $request) {
        $rules = [
            'company_id' => 'required',
            'customer_type' => 'required',
            'status' => 'required',
            'msg'  => 'required',
        ];
        $customeMessage = [
            'company_id.required' => 'Please  select company',
            'customer_type.required' => 'Please enter customer type.',
            'status.required' => 'Please select status',
            'msg.required' => 'Please enter message',
        ];
        $validator = Validator::make($request->all(),$rules, $customeMessage);

        if( $validator->fails() ) {
             return back()->withInput()->withErrors($validator->errors());
        } else {
        	if($request->id){
        		$notification = CustomerNotification::find($request->id);
        	} else {
        		$notification = new CustomerNotification();
                $notification->value = $request->value;
        	}
        	
            if(isset($request->value) || ($request->customer_type=='all_customer')){
                $notification->value = $request->value;
            }
        	$notification->company_id = $request->company_id;
        	$notification->customer_type = $request->customer_type;
        	$notification->status = $request->status;
            $notification->msg = $request->msg;
            if($request->states){
                $notification->state_id = $request->states;
            } else {
                $notification->state_id = null;
            }
            if($request->cities){
                $notification->city_id = $request->cities;
            } else {
                $notification->city_id = null;
            }
            if($request->tehsil){
                $notification->tehsil_id = $request->tehsil;
            } else {
                $notification->tehsil_id = null;
            }
            // dd($notification);
    	    if($notification->save()) {
        		if(!$request->id) {
    	    		return redirect()->route('admin.notifications.index')->with('message', 'Notification create successfully.');
    	    	} else {
    	    		return redirect()->route('admin.notifications.index')->with('message', 'Notification update successfully.');
    	    	}
    	    }
        }
    }


    public function destroy($id) {
    	if(CustomerNotification::where('id', $id)->exists()) {
	    	$notification = CustomerNotification::find($id);
	    	if($notification->delete()) {
	    		return redirect()->route('admin.notifications.index')->with('message', 'Notification delete successfully.');
	    	}
	    }
    }

    public function getValue(Request $request) {
        $data = [];
        if($request->value=='single_customer') {
            $customers = Employee::join('products', 'products.company_id', 'employees.id')
                            ->join('order_product', 'order_product.product_id', 'products.id')
                            ->join('orders', 'orders.id', 'order_product.order_id')
                            ->join('customers', 'customers.id', 'orders.customer_id')
                            ->select('customers.id', 'customers.name')
                            ->where('employees.id', $request->company_id)
                            ->distinct()
                            ->get();
            foreach( $customers as $customer )
            {
                $data[$customer->id] = $customer->name;
            }
        }
        if($request->value=='state_customer') {
            $states = CountryData::select('STCode', 'DTName')->where('DTCode',000)->get();
            foreach( $states as $state )
            {
                $data[$state->STCode] = $state->DTName;
            }
        }
        if($request->value=='city_customer') {
            $cities = CountryData::select('DTCode', 'DTName')->where('SDTCode', 00000)->where('DTCode', '!=',000)->orderBy('DTName', 'ASC')->where('STCode', 'like', '%' . $request->state)->get();
            foreach( $cities as $city )
            {
                $data[$city->DTCode] = $city->DTName;
            }
        }
        if($request->value=='tehsil_customer') {
            $tehsils = countrydata::select('SDTCode', 'SDTName')->where('DTCode', 'like', '%' .$request->city)->where('SDTCode', '!=',00000)->where('TVCode',000000)->orderBy('SDTName', 'ASC')->get();
            foreach( $tehsils as $tehsil )
            {
                $data[$tehsil->SDTCode] = $tehsil->SDTName;
            }
        }
        if($request->value=='village_customer') {
            $villages = countrydata::select('TVCode', 'Name')->where('SDTCode', 'like', '%' .$request->tehsil)->where('TVCode', '!=',000000)->orderBy('Name', 'ASC')->get();
            foreach( $villages as $village )
            {
                $data[$village->TVCode] = $village->Name;
            }
        }
        return $data;
    }
}
