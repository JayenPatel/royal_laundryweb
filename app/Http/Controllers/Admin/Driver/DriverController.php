<?php

namespace App\Http\Controllers\Admin\Driver;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Drivers\Driver;
use App\Helper\Permission;
use Validator;

class DriverController extends Controller
{
	public function show()
	{
	try 
		{
		$data['drivers'] = Driver::all();
        return view('admin.drivers.list')->with($data);
        }
		catch (Exception $e)
		{
			log::error($e);
		}
		
    }
      
    public function create(Request $request)
    {
        try
        {
            $data['drivers'] = Driver::all();
            return view('admin.drivers.create')->with($data);
        }
        catch (Exception $e) 
    	{
    		Log::error($e);
    	}
    }
    public function store(Request $request){
        try{
            $rules = [
                'vehicletype' => 'required',
                'name' => 'required',
                'email' => 'required',
                'contact' => 'required',
                'status' => 'required',
                'vehicle_reg_no' => 'required',
                'licence_no' => 'required',
            ];
            $customMsg = [
                'vehicletype.required' => 'Name is required',
                'name.required' => 'Description is required',
                'email.required' => 'Status is required',
                'contact.required' => 'Image is required',
                'status.required' => 'Image is required',
                'vehicle_reg_no.required' => 'Image is required',
                'licence_no.required' => 'Image is required',
            ];

            $validator = Validator::make($request->all(), $rules ,$customMsg);
            if ($validator->fails()) {
                return back()->withErrors($validator->errors())->withInput();
            }else{
                if ($request->id) {
                    $id=$request->id;
                    $drivers = Driver::find($id);
                }else{
                    $drivers = new Driver();
                }
                
                $drivers->vehicle_type = request('vehicletype');
                $drivers->name = request('name');
                $drivers->email = request('email');
                $drivers->contact = request('contact');
                $drivers->status = request('status');
                $drivers->vehicle_reg_no = request('vehicle_reg_no');
                $drivers->licence_no = request('licence_no');
                
                if($drivers->save()){
                    if ($request->has('profile')) {

                        $file = $request->profile;

                        $extension = $file->getClientOriginalExtension();                
                        $time = rand(00,99);
                        $filename = $time.".".$extension;
                        $file->move(public_path('/drivers'),$filename);
                        $drivers = Driver::find($drivers->id);
                        $drivers->profile = $filename;
                        $drivers->save(); 
                    }
                }

                if($request->id){
                    $request->session()->flash('message', 'Driver update successfully');    
                }else{
                    $request->session()->flash('message', 'Driver create successfully!');
                }
                return redirect('admin/driver/index');    
            }    
        }catch (Exception $e){
            Log::error($e); 
        }
    }

    public function edit($id){
        try 
        {
            $data['drivers'] = Driver::find($id);
            return view('admin.drivers.create')->with($data);
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function destroy($id){
        try {
            $drivers = Driver::find($id);
            $drivers->delete();
            return redirect('admin/driver/index')->with('delete', 'Delete Succesfully...');
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function usershow($id){
        try{
            $data['drivers'] = Driver::find($id);
            return view('admin.drivers.usershow')->with($data);
        }catch (Exception $e){
            Log::error($e);
        }
    }
}
