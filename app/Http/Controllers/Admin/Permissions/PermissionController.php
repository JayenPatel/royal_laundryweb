<?php

namespace App\Http\Controllers\Admin\Permissions;

use App\Http\Controllers\Controller;
use App\Shop\Permissions\Repositories\PermissionRepository;
use Illuminate\Http\Request;
use Log;
use Validator;
use App\Helper\ResponseMessage;
use App\Shop\Permissions\Permission;

class PermissionController extends Controller
{
    /**
     * @var PermissionRepository
     */
    private $permRepo;

    /**
     * PermissionController constructor.
     *
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permRepo = $permissionRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $list = $this->permRepo->listPermissions();

        $permissions = $this->permRepo->paginateArrayResults($list->all());

        return view('admin.permissions.list', compact('permissions'));
    }

    public function store(Request $request){
        try{
            $rules = [
                'name' => 'required|unique:permissions',
                'display_name' => 'required',
            ];
            $customeMessage = [
                'name.required' => 'Please enter name.',
                'name.unique' => 'Please enter unique name.',
                'display_name.required' => 'Please enter display name',
            ];
            $validator = Validator::make($request->all(),$rules, $customeMessage);

            if( $validator->fails() ) {
                 return back()->withInput()->withErrors($validator->errors());
            } else {
                $this->permRepo->createPermission($request->except('_token', '_method'));
                    $request->session()->flash('message', 'Permission Create successfully');
                    return redirect()->route('admin.permissions.index');
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }

    public function destroy($id)
    {
        $permission = $this->permRepo->findPermissionById($id);
        $permission->delete();
        return back()->with('message', 'Permission delete successfully.');
    }

    public function edit($id)
    {
        $permissions = $this->permRepo->findPermissionById($id);
        return view('admin.permissions.edit',['permissions' => $permissions]);
    }

    public function create()
    {
        return view('admin.permissions.create');
    }

    public function update(Request $request, $id){
        // dd($id);
        $rules=array();
        $customeMessage=array();
        if($request!=""){
            $old_data = Permission::where('id',$id)->first();
            // dd($old_data);
            $old_id = $id;
            $old_name = $old_data->name;
            if( $old_id != $id ){
               $rules['id'] = 'required';
                $customeMessage['id.required']  = 'Please enter id.';
            } 
            if( $old_name != $request->name ){
                $rules['name'] = 'unique:permissions';   
                $customeMessage['name.unique']  = 'Please enter unique name.';
            }
        } else{
             $rules = [
                'name' => 'required|unique:permissions',
                'display_name' => 'required',
            ];
            $customeMessage = [
                'name.required' => 'Please enter name.',
                'name.unique' => 'Please enter unique name.',
                'display_name.required' => 'Please enter display name',
            ];
        }
            $validator = Validator::make($request->all(),$rules, $customeMessage);
            if( $validator->fails() ){
                return back()->withInput()->withErrors($validator->errors());
            } else{
                $permissions = $this->permRepo->findPermissionById($id);
                $update = new PermissionRepository($permissions);
                $update->updatePermission($request->all());
                $request->session()->flash('message', 'Permission update successfully.');
                return redirect()->route('admin.permissions.index', $id);
            }

    }

}
