<?php

namespace App\Http\Controllers\Admin\Offers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Offers\Repositories\OfferRepository;
use App\Shop\Offers\Repositories\Interfaces\OfferRepositoryInterface;
use App\Shop\Offers\Offer;
use App\Shop\Categories\Category;
use App\Shop\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use CreateOffersTable;
use App\Shop\Offers\Requests\CreateOfferRequest;
use App\Shop\Offers\Requests\UpdateOfferRequest;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Offers\Transformations\OffersTransformable;
use App\Helper\Permission;
use Auth;

class OffersController extends Controller
{
    private $offerRepo;
    use OffersTransformable;

    public function __construct(CategoryRepositoryInterface $categoryRepository,OfferRepository $offerRepo,EmployeeRepositoryInterface $employeeRepository)
    {
        $this->offerRepo = $offerRepo;
        $this->companiesRepo = $employeeRepository;
        $this->categoryRepo = $categoryRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permission = Permission::permission('offer');
        if($permission->view==1) {
            $list = $this->offerRepo->listOffers('created_at', 'desc');

            $offers = $list->map(function (Offer $offer) {
                return $this->transformOffer($offer);
            })->all();
            return view('admin.offers.list', [
                'offers' => $this->offerRepo->paginateArrayResults($offers),
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $offers = Offer::where('parent_id',null)->get();
        $permission = Permission::permission('offer');
        if($permission->add==1) {
            return view('admin.offers.create', [
                'offer' => new Offer,
                'offers'=> $offers,
                'companies' => $this->companiesRepo->listEmployees('name', 'asc')
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

   /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProductRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOfferRequest $request)
    {
        $permission = Permission::permission('offer');
        if($permission->add==1) {
        
            $data = $request->except('_token', '_method');
            $des = $data['description'];
            $des = str_replace('<p>', "", $des);
            $des = str_replace('</p>', "", $des);
            $data['description']=$des;
            $url = env('APP_URL');
            

            $offer = $this->offerRepo->createOffer($data);
            if ($request->has('cover')) {
                $file    = $request->cover;
                $time    = md5(time());
                $profile = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                $path    = "images/offers/";
                $file->move(public_path($path), $time.'.'.$profile);
                $profile_update          = Offer::find($offer->id);
                $profile_update->cover = $url."/".$path.$time.'.'.$profile;
                $profile_update->save();
            }
            return redirect()->route('admin.offers.index')->with('message', 'Offer create successfully');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::permission('offer');
        $offer = $this->offerRepo->findOfferById($id);
        $offers = Offer::where('parent_id',$id)->get();
        return view('admin.offers.show', [
            'offer' => $offer,
            'offers'=> $offers,
            'permission' => $permission,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offers = Offer::where('parent_id',null)->get();
        $permission = Permission::permission('offer');
        if($permission->edit==1) {
            $offer = $this->offerRepo->findOfferById($id);
            $categories = Category::where('company_id', $offer->company_id)->where('parent_id',null)->get();
            return view('admin.offers.edit', [
                'offer' => $offer,
                'offers' => $offers,
                'companies' => $this->companiesRepo->listEmployees('name', 'asc'),
                'categories' => $categories,
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOfferRequest $request, $id)
    {
        $permission = Permission::permission('offer');
        if($permission->edit==1) {
            $offer = $this->offerRepo->findOfferById($id);
            $update = new OfferRepository($offer);
            $data = $request->except('_token', '_method');
            $des = $data['description'];
            $des = str_replace('<p>', "", $des);
            $des = str_replace('</p>', "", $des);
            $data['description']=$des;
            $url = env('APP_URL');
            $update->updateOffer($data);
            if ($request->has('cover')) {
                $file    = $request->cover;
                $time    = md5(time());
                $profile = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName();
                $path    = "images/offers/";
                $file->move(public_path($path), $time.'.'.$profile);
                $profile_update          = Offer::find($offer->id);
                $profile_update->cover = $url."/".$path.$time.'.'.$profile;
                $profile_update->save();
            }
            $request->session()->flash('message', 'Offer update successfully');
            return redirect()->route('admin.offers.index');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::permission('offer');
        if($permission->delete==1) {
            $offer = $this->offerRepo->findOfferById($id);
            $offer->delete();
            request()->session()->flash('message', 'Offer delete successfully');
            return redirect()->route('admin.offers.index');
        } else {
            return view('layouts.errors.403');
        }
    }

    /* @param Request $request
    * @return \Illuminate\Http\RedirectResponse
    */
   public function removeImage(Request $request)
   {
       $this->offerRepo->deleteFile($request->only('offers'));
       request()->session()->flash('message', 'Image delete successful');
       return redirect()->route('admin.offers.edit', $request->input('offers'));
   }
}
