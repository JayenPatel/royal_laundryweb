<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Shop\Addresses\Repositories\Interfaces\AddressRepositoryInterface;
use App\Shop\Addresses\Transformations\AddressTransformable;
use App\Shop\Couriers\Courier;
use App\Shop\Couriers\Repositories\CourierRepository;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Customer;
use App\Shop\Countries\Country;
use App\Shop\Cities\City;
use App\Shop\Addresses\Address;
use App\Shop\States\State;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Shop\Orders\Order;
use App\Shop\OrderProducts\OrderProduct;
use App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface;
use App\Shop\Orders\Repositories\OrderRepository;
use App\Shop\OrderStatuses\OrderStatus;
use App\Shop\OrderStatuses\Repositories\Interfaces\OrderStatusRepositoryInterface;
use App\Shop\OrderStatuses\Repositories\OrderStatusRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Helper\Permission;
use Auth;
use PDF;
use File;
use App\Helper\NotificationHelper;
use App\Helper\ResponseMessage;
use App\Shop\Products\Product_attributes;
use App\Shop\CountriesData\CountryData;

class OrderController extends Controller
{
    use AddressTransformable;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepo;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * @var OrderStatusRepositoryInterface
     */
    private $orderStatusRepo;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        CourierRepositoryInterface $courierRepository,
        AddressRepositoryInterface $addressRepository,
        CustomerRepositoryInterface $customerRepository,
        OrderStatusRepositoryInterface $orderStatusRepository
    ) {
        $this->orderRepo = $orderRepository;
        $this->courierRepo = $courierRepository;
        $this->addressRepo = $addressRepository;
        $this->customerRepo = $customerRepository;
        $this->orderStatusRepo = $orderStatusRepository;

        $this->middleware(['permission:update-order, guard:employee'], ['only' => ['edit', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permission = Permission::permission('order');
        if($permission->view==1) {
            $query = Order::join('customers', 'customers.id', '=', 'orders.customer_id')
            ->join('order_statuses','order_statuses.id', '=','orders.order_status_id' )
            ->select('customers.name as customer_name', 'order_statuses.name as status', 'order_statuses.color as color', 'orders.*')
            ->whereNull('customers.deleted_at');

            if($request->sort!=""){
                if($request->sort == 'customer') {
                    $query->orderby('customers.email','DESC');
                }
                if($request->sort == 'price_low_to_high') {
                    $query->orderby('total','ASC');
                }
                if($request->sort == 'price_high_to_low') {
                    $query->orderby('total','DESC');
                }
                if($request->sort == 'date') {
                    $query->orderBy('orders.created_at', 'DESC');
                }
            } else {
                $query->orderBy('orders.created_at', 'DESC');
            }
            if($request->product_no!="") {
                $product_no = $request->product_no;
            } else {
                $product_no = 10;
            }
            $orders = $query->paginate($product_no);
            $fullscreen_theme = $query->orderBy('Id','DESC')->paginate($product_no)->appends($request->except('page'));

            // $orders = $list->paginate(10);
            // $fullscreen_theme = $list->paginate(10)->appends($request->except('page'));


            // if (request()->has('q')) {
            //     $list = $this->orderRepo->searchOrder(request()->input('q') ?? '');
            // }

            // $orders = $this->orderRepo->paginateArrayResults($this->transFormOrder($list), 10);
            return view('admin.orders.list', ['orders' => $orders, 'fullscreen_theme' => $fullscreen_theme]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $orderId
     * @return \Illuminate\Http\Response
     */
    public function show($orderId)
    {
        $order = $this->orderRepo->findOrderById($orderId);
        $order->courier = $this->courierRepo->findCourierById($order->courier_id);
        $order->address = $this->addressRepo->findAddressById($order->address_id);
        // dd($order->address);
        $orderRepo = new OrderRepository($order);

        // $items = $orderRepo->listOrderedProducts();
        $items = OrderProduct::where('order_id', $orderId)->get();
        foreach($items as $item){
            $attr = Product_attributes::find($item->attribute_id);
            if($attr){
                $item->product_price = $attr->sale_price;
            }
        }
        // $state = State::find($order->address->state_code);
        $state = CountryData::select('DTName')->where('STCode', 'like', '%' .$order->address->state_code)->where('DTCode',000)->first();
        $city = CountryData::select('DTName')->where('DTCode', 'like', '%' .$order->address->city)->first();
        $tehsil = CountryData::select('SDTName')->where('SDTCode','like', '%' .$order->address->tehsil_id)->first();
        $village = CountryData::select('Name')->where('TVCode','like', '%' .$order->address->village_id)->first();
       // dd($tehsil);
        // $order->address->state_code = $state->state; 
        $order->address->state_code = $state->DTName; 
        $order->address->city = $city->DTName; 
        $order->address->tehsil_id = $tehsil->SDTName; 
        $order->address->village_id = $village->Name; 
        $permission = Permission::permission('order');
        return view('admin.orders.show', [
            'order' => $order,
            'items' => $items,
            'customer' => $this->customerRepo->findCustomerById($order->customer_id),
            'currentStatus' => $this->orderStatusRepo->findOrderStatusById($order->order_status_id),
            'payment' => $order->payment,
            'user' => Auth::guard('employee')->user(),
            'permission' => $permission
        ]);
    }

    /**
     * @param $orderId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($orderId)
    {
        $permission = Permission::permission('order');
        if($permission->edit==1) {
            $order = $this->orderRepo->findOrderById($orderId);
            $order->courier = $this->courierRepo->findCourierById($order->courier_id);
            $order->address = $this->addressRepo->findAddressById($order->address_id);
            // $state = State::find($order->address->state_code);
            // $order->address->state_code = $state->state;
            $country = Country::find($order->address->country_id);
            $order->address->country_id = $country->name;
            // $state = State::find($order->address->state_code);
            $state = CountryData::select('DTName')->where('STCode', 'like', '%' .$order->address->state_code)->where('DTCode',000)->first();
            $city = CountryData::select('DTName')->where('DTCode', 'like', '%' .$order->address->city)->first();
            $tehsil = CountryData::select('SDTName')->where('SDTCode','like', '%' .$order->address->tehsil_id)->first();
            $village = CountryData::select('Name')->where('TVCode','like', '%' .$order->address->village_id)->first();
            $order->address->state_code = $state->DTName; 
            $order->address->city = $city->DTName; 
            $order->address->tehsil_id = $tehsil->SDTName; 
            $order->address->village_id = $village->Name; 
            $orderRepo = new OrderRepository($order);

            $items = OrderProduct::where('order_id', $orderId)->get();
            foreach($items as $item){
                $attr = Product_attributes::find($item->attribute_id);
                if($attr){
                    $item->product_price = $attr->sale_price;
                }
            }

            return view('admin.orders.edit', [
                'statuses' => $this->orderStatusRepo->listOrderStatuses(),
                'order' => $order,
                'items' => $items,
                'customer' => $this->customerRepo->findCustomerById($order->customer_id),
                'currentStatus' => $this->orderStatusRepo->findOrderStatusById($order->order_status_id),
                'payment' => $order->payment,
                'user' => auth()->guard('employee')->user()
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * @param Request $request
     * @param $orderId
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $orderId)
    {
        $permission = Permission::permission('order');
        if($permission->view==1) {
            $order = $this->orderRepo->findOrderById($orderId);
            $orderRepo = new OrderRepository($order);

            if ($request->has('total_paid') && $request->input('total_paid') != null) {
                $orderData = $request->except('_method', '_token');
            } else {
                $orderData = $request->except('_method', '_token', 'total_paid');
            }
            
            if($orderRepo->updateOrder($orderData)){
                $order_status = OrderStatus::find($request->order_status_id);
                $status = $order_status->name;

                $server_api_key = 'AAAAr2mz3qw:APA91bHreE9QcwXtIMa221zDANf-Nt1lpFHwNB7u1T3E2qVqFVPYbGt_NRBQT-khwCiZsHQcgaRND4uNNhRVDswAhH9SRorI19s0e-uLpsywUL1YCU0_E7Rm1HFkJ54iWwwcNiOmn9mS';
                $title = 'Order Status';
                $userid = $order->customer_id;
                $message = 'Your order has been '.$status;
                $token = Customer::select('token')->where('id', $userid)->first();
                $token = $token->token;
                $result = NotificationHelper::notification($message, $server_api_key, $token, $title);
                $result = json_decode($result);
                if(isset($result->results[0]->message_id)){
                    $orderRepo->updateOrder($orderData);
                    $notification = new Notification();
                    $notification->customer_id=$order->customer_id;
                    $notification->notify_detail="Your order has been ".$status;
                    date_default_timezone_set('Asia/Kolkata');
                    $notification->time=date("Y-m-d H:i:s");
                    $notification->save();
                    return redirect()->route('admin.orders.show', $orderId)->with('message', 'Order update successfully');
                }else{
                    return redirect()->route('admin.orders.show', $orderId)->with('message', 'Order update successfully but customer notification does not send.');
                }
            }

            // $orderRepo->updateOrder($orderData);

            // return redirect()->route('admin.orders.show', $orderId)->with('message', 'Order update successfully');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Generate order invoice
     *
     * @param int $id
     * @return mixed
     */
    public function generateInvoice(int $id)
    {
        $order = $this->orderRepo->findOrderById($id);
        $address = Address::where('id', $order->address_id)->first();
        $state = CountryData::select('DTName')->where('STCode', 'like', '%' .$order->address->state_code)->where('DTCode',000)->first();
        $city = CountryData::select('DTName')->where('DTCode', 'like', '%' .$order->address->city)->first();
        $tehsil = CountryData::select('SDTName')->where('SDTCode','like', '%' .$order->address->tehsil_id)->first();
        $village = CountryData::select('Name')->where('TVCode','like', '%' .$order->address->village_id)->first();
        $order->address->state_code = $state->DTName; 
        $order->address->city = $city->DTName; 
        $order->address->tehsil_id = $tehsil->SDTName; 
        $order->address->village_id = $village->Name; 
        $country = Country::find($order->address->country_id);
        $order->address->country_id = $country->name;
        // dd($order);
        $products = OrderProduct::where('order_id', $order->id)->get();
        foreach($products as $product){
            $attr = Product_attributes::find($product->attribute_id);
            if($attr){
                $product->product_price = $attr->sale_price;
            }
        }
        $data = [
            'order' => $order,
            'products' => $products,
            'customer' => $order->customer,
            'courier' => $order->courier,
            'address' => $order->address,
            'status' => $order->orderStatus,
            'payment' => $order->paymentMethod
        ];

        $pdf = PDF::loadView('invoices.orders', $data);
        $order->invoice_path='invoices/'.time().'.pdf';
        $pdf->save($order->invoice_path);
        $order->update();
        return $pdf->stream();
    }

    /**
     * @param Collection $list
     * @return array
     */
    private function transFormOrder(Collection $list)
    {
        $courierRepo = new CourierRepository(new Courier());
        $customerRepo = new CustomerRepository(new Customer());
        $orderStatusRepo = new OrderStatusRepository(new OrderStatus());

        return $list->transform(function (Order $order) use ($courierRepo, $customerRepo, $orderStatusRepo) {
            $order->courier = $courierRepo->findCourierById($order->courier_id);
            $order->customer = $customerRepo->findCustomerById($order->customer_id);
            $order->status = $orderStatusRepo->findOrderStatusById($order->order_status_id);
            return $order;
        })->all();
    }
}
