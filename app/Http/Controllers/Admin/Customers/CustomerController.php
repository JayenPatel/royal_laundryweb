<?php

namespace App\Http\Controllers\Admin\Customers;

use App\Shop\Customers\Customer;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Shop\Customers\Requests\CreateCustomerRequest;
use App\Shop\Customers\Requests\UpdateCustomerRequest;
use App\Shop\Customers\Transformations\CustomerTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\CustomersEmail;
use App\Helper\Permission;
use App\Shop\Addresses\Address;
use App\Shop\CountriesData\CountryData;

class CustomerController extends Controller
{
    use CustomerTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepo = $customerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permission = Permission::permission('customer');
        if($permission->view==1) {
            $list = $this->customerRepo->listCustomers('created_at', 'desc');

            if (request()->has('q')) {
                $list = $this->customerRepo->searchCustomer(request()->input('q'));
            }

            $customers = $list->map(function (Customer $customer) {
                return $this->transformCustomer($customer);
            })->all();


            return view('admin.customers.list', [
                'customers' => $this->customerRepo->paginateArrayResults($customers),
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::permission('customer');
        if($permission->add==1) {
            return view('admin.customers.create');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $permission = Permission::permission('customer');
        if($permission->add==1) {
            $this->customerRepo->createCustomer($request->except('_token', '_method'));
            $request->session()->flash('message', 'Customer create successfully!');
            return redirect()->route('admin.customers.index');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $customer = $this->customerRepo->findCustomerById($id);
        $addresses = Address::where('customer_id', $customer->id)->orderBy('id', 'DESC')->first();

        return view('admin.customers.show', [
            'customer' => $customer,
            'addresses' => $addresses
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::permission('customer');
        if($permission->edit==1) {
            return view('admin.customers.edit', ['customer' => $this->customerRepo->findCustomerById($id)]);
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomerRequest $request, $id)
    {
        $permission = Permission::permission('customer');
        if($permission->edit==1) {
            $customer = $this->customerRepo->findCustomerById($id);
           
            if ($request->input('status') != $customer->status) {
                $data = $customer;
                $data['status'] = $request->input('status');
                Mail::to($request->input('email'))->send(new CustomersEmail($data));
            }

            $update = new CustomerRepository($customer);
            $data = $request->except('_method', '_token', 'password');

            if ($request->has('password')) {
                $data['password'] = bcrypt($request->input('password'));
            }

            $update->updateCustomer($data);

            $request->session()->flash('message', 'Customer update successfully!');
            return redirect()->route('admin.customers.index');
        } else {
            return view('layouts.errors.403');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $permission = Permission::permission('customer');
        if($permission->delete==1) {
            $customer = $this->customerRepo->findCustomerById($id);

            $customerRepo = new CustomerRepository($customer);
            $customerRepo->deleteCustomer();

            return redirect()->route('admin.customers.index')->with('message', 'Customer delete successfully');
        } else {
            return view('layouts.errors.403');
        }
    }
}
