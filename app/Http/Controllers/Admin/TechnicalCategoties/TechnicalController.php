<?php

namespace App\Http\Controllers\Admin\TechnicalCategoties;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\TechnicalCategories\Technical;
use Log;
use App\Helper\Permission;
use Validator;
use App\Helper\ResponseMessage;
use App\Shop\Products\Product;

class TechnicalController extends Controller
{
    public function index()
    {
        $permission = Permission::permission('technical');
        if($permission->view==1) {
            $technicals = Technical::orderBy('created_at', 'ASC')->get();
            return view('admin.technical-categories.list', [
                'technicals' => $technicals,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    }

    public function create()
    {
        $permission = Permission::permission('technical');
        if($permission->add==1) {
		  return view('admin.technical-categories.create');
        } else {
            return view('layouts.errors.403');
        }
    }

    public function edit($id)
    {
        $permission = Permission::permission('technical');
        if($permission->edit==1) {
    		$technical = Technical::where('id', $id)->first();
    		return view('admin.technical-categories.create', ['technical' => $technical]);
        } else {
            return view('layouts.errors.403');
        }
    }

    public function store(Request $request)
    {
    	try {
            $permission = Permission::permission('technical');
            if($permission->add==1 || $permission->edit==1) {
                $rules =[];
                $customeMessage = [
                    'name.required' => 'Please enter customer id.',
                    'name.unique' => 'Technical name is already exists.',
                ];
                if (isset($request->id) && !empty($request->id)) {
                    // if (Technical::find($request->id)->name != $request->name) {
                    if (strcasecmp(Technical::find($request->id)->name, $request->name)) {
                        $rules['name'] = 'required|unique:technicals,name';
                    }
                } else{
                    $rules['name'] = 'required|unique:technicals,name';
                }
                $validator = Validator::make($request->all(),$rules,$customeMessage);

                if( $validator->fails() ){
                    return back()->withInput()->withErrors($validator->errors());
                } else{
            		$id = $request->id;
            		$name = $request->name;
            		if($id!="") {
            			$technical = Technical::find($id);
                        $product = Product::where('technical_name', $technical->name)->get();
                        if($product){
                            foreach ($product as $prod) {
                                $prod->technical_name = $name;
                                $prod->save();
                            }
                        }
            		} else {
            			$technical = new Technical();
            		}
            		$technical->name = $name;
            		if($technical->save()) {
            			if($id!="") {
            				return redirect()->route('admin.technical.index')->with('message', 'Technical category update successfully.');
            			} else {
        	    			return redirect()->route('admin.technical.index')->with('message', 'Technical category create successfully.');
        	    		}
            		} else {
            			return redirect()->route('admin.technical.create')->with('Something went wrong.');
            		}
                }
            } else {
                return view('layouts.errors.403');
            }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    	
    }


    public function destroy($id)
    {
        try {
            $permission = Permission::permission('technical');
            if($permission->delete==1) {
            	if(Technical::where('id', $id)->exists()){
            		$technical = Technical::where('id', $id)->first();
    	        	$product = Technical::join('products', 'products.technical_name', 'technicals.name')
    	        					->select('products.*')
    	        					->where('technicals.id', $id)
    	        					->first();
    	        	if($product) {
    	        		return redirect()->route('admin.technical.index')->with('message', 'Technical category have product first delete product.');
    	        	} else {
    	        		$technical->delete();
    	        		return redirect()->route('admin.technical.index')->with('message', 'Technical category delete successfully.');
    	        	} 			
                }
            } else {
                return view('layouts.errors.403');
            }
        } catch (Exception $e) {
            log::error($e);
        }
    }
}
