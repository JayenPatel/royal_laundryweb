<?php

namespace App\Http\Controllers\Admin\Products;

use App\Shop\Attributes\Repositories\AttributeRepositoryInterface;
use App\Shop\AttributeValues\Repositories\AttributeValueRepositoryInterface;
use App\Shop\Brands\Repositories\BrandRepositoryInterface;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\ProductAttributes\ProductAttribute;
use App\Shop\Products\Exceptions\ProductInvalidArgumentException;
use App\Shop\Products\Exceptions\ProductNotFoundException;
use App\Shop\Products\Product;
use App\Shop\Crops\Crop;
use App\Shop\TechnicalCategories\Technical;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Products\Repositories\ProductRepository;
use App\Shop\Products\Requests\CreateProductRequest;
use App\Shop\Products\Requests\UpdateProductRequest;
use App\Http\Controllers\Controller;
use App\Shop\Products\Transformations\ProductTransformable;
use App\Shop\Tools\UploadableTrait;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Shop\Employees\Repositories\Interfaces\EmployeeRepositoryInterface;
use App\Shop\Employees\Repositories\EmployeeRepository;
use App\Helper\Permission;
use App\Shop\AdditionalInfos\AdditionalInfo;
use App\Shop\Products\Product_attributes;
use App\Shop\Employees\Employee;
use App\Shop\SubCategories\SubCategory;
use App\Shop\Categories\Category;

class ProductController extends Controller
{ 
    public function index(Request $request)
    {
        try {
            $data['products'] = Product::get();
            return view('admin.products.list')->with($data);
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

 
    public function create()
    {
        try {
            $data['product'] = Product::get();
            return view('admin.products.create')->with($data);   
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function store(Request $request)
    {
        try {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'status' => 'required',
                'cover' => 'required',
            ];
            $customMsg = [
                'name.required' => 'Name is required',
                'description.required' => 'Description is required',
                'status.required' => 'Status is required',
                'cover.required' => 'Image is required',
            ];

            $validator = Validator::make($request->all(), $rules ,$customMsg);
            if ($validator->fails()) {
                return back()->withErrors($validator->errors())->withInput();
            }else{
                if ($request->id) {
                    $id=$request->id;
                    $products = Product::find($id);
                }else{
                    $products = new Product;
                }
                
                $products->name = request('name');
                $products->description = request('description');
                $products->status = request('status');

                if($products->save()){
                    if ($request->has('cover')) {

                        $file = $request->cover;
                        $extension = $file->getClientOriginalExtension();                
                        $time = rand(00,99);
                        $filename = $time.".".$extension;
                        $file->move(public_path('/product'),$filename);
                        $products = Product::find($products->id);
                        $products->cover = $filename;
                        
                        $products->save(); 
                    }
                }

                if($request->id){
                    $request->session()->flash('message', 'Product update successfully!');
                }else{
                    $request->session()->flash('message', 'Product create successfully!');
                }
                return redirect()->route('admin.products.index');
            }   
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function show($id)
    {
        return view('admin.products.list');
    }

    
    public function edit($id)
    {
        try {
            $data['product'] = Product::find($id);
            return view('admin.products.create')->with($data);
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function productshow($id)
    {
        try {
            $data['product'] = Product::find($id);
            return view('admin.products.show')->with($data);
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

   
    public function update(Request $request, $id)
    {
        return redirect()->route('admin.products.index')
            ->with('message', 'Product updated successfully');
    }

    public function destroy($id)
    {
        try {
            $products = Product::find($id);
            $products->delete();

            return redirect('admin/products/index')->with('message', 'Product deleted successfully');
        } catch (Exception $e) {
            Log::errors($e);
        }

    }

    public function category(Request $request)
    {
        try {
            $cat['categories'] = Category::get();
            return view('admin.products.category')->with($cat);   
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function create_category(Request $request){
        try {
            $data['category'] = Category::get();
            $data['products'] = Product::get();
            return view('admin.products.create-category')->with($data);
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function store_category(Request $request){
        try {
            $rules = [
                'name' => 'required',
                'product_name' => 'required',
                'description' => 'required',
                'status' => 'required',
            ];
            $customMsg = [
                'name.required' => 'Name is required',
                'product_name.required' => 'Product name is required',
                'description.required' => 'Description is required',
                'status.required' => 'Status is required'
            ];

            $validator = Validator::make($request->all(), $rules ,$customMsg);
            if ($validator->fails()) {
                return back()->withErrors($validator->errors())->withInput();
            }else{
                if ($request->id) {
                    $id=$request->id;
                    $categories = Category::find($id);
                }else{
                    $categories = new Category;
                }

                $categories->name = request('name');
                $categories->product_name = request('product_name');
                $categories->description = request('description');
                $categories->status = request('status');

                $categories->save();
            }
            if ($request->id) {
                $request->session()->flash('message', 'Category update successfully');
            }else{
                $request->session()->flash('message', 'Category added successfully');
            }
            return redirect()->route('admin.product.category'); 
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function edit_category($id)
    {
        try {
            $data['products'] = Product::get();
            $data['category'] = Category::find($id);
            return view('admin.products.create-category')->with($data);
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function category_show($id){
        try{
            $data['category'] = Category::find($id);
            return view('admin.products.category-show')->with($data);
        }catch (Exception $e){
            Log::error($e);
        }
    }

    public function category_destroy($id){
        try {
            $categories = Category::find($id);
            $categories->delete();

            return redirect('admin/products/category')->with('message', 'Category deleted successfully');
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function subcategory(Request $request){
        try {
            $data['subcategory'] = SubCategory::all();
            return view('admin.products.subcategory')->with($data);
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function create_subcategory(Request $request){
        try {
            $data['category'] = Category::all();
            $data['subcategory'] = SubCategory::all();
            return view('admin.products.create-subcategory')->with($data);
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function store_subcategory(Request $request){
        try {
            $rules = [
                'name' => 'required',
                'category_name' => 'required',
                'description' => 'required',
                'price' => 'required',
                'discount' => 'required',
                'offers' => 'required',
                'status' => 'required'
            ];
            $customMsg = [
                'name.required' => 'Name is required',
                'category_name.required' => 'Category name is required',
                'description.required' => 'Description is required',
                'price.required' => 'Price is required',
                'discount.required' => 'Discount is required',
                'offers.required' => 'Offer is required',
                'status.required' => 'Status is required'
            ];

            $validator = Validator::make($request->all(), $rules ,$customMsg);
            if ($validator->fails()) {
                return back()->withErrors($validator->errors())->withInput();
            }else{
                
                if ($request->id) {
                    $id = $request->id;
                    $subcategories = SubCategory::find($id);                    
                }else{
                    $subcategories = new SubCategory();
                }

                $subcategories->name = request('name');
                $subcategories->category_name = request('category_name');
                $subcategories->description = request('description');
                $subcategories->price = request('price');
                $subcategories->discount = request('discount');
                $subcategories->offers = request('offers');
                $subcategories->status = request('status');

                $subcategories->save();
            }
            if($request->id){
                $request->session()->flash('message','Subcategory update successfully');
            }else{
                $request->session()->flash('message','Subcategory added successfully');
            }
            return redirect('admin/products/subcategory'); 
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function edit_subcategory($id)
    {
        try {
            $data['category'] = Category::get();
            $data['categories'] = SubCategory::find($id);

            return view('admin.products.create-subcategory')->with($data);
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function subcategory_destroy($id){
        try {
            $subcategories = SubCategory::find($id);
            $subcategories->delete();
            return redirect()->route('admin.products.subcategory')->with('message', 'Subcategory delete successfully');
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

    public function subcategory_show($id){
        try {
            $data['subcategory'] = SubCategory::find($id);
            return view('admin.products.subcategory-show')->with($data);
        } catch (Exception $e) {
            Log::errors($e);
        }
    }

}
