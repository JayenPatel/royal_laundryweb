<?php

namespace App\Http\Controllers\Admin\Priorities;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Priorities\Priority;
use Log;
use App\Helper\Permission;
use App\Shop\Categories\Category;
use App\Shop\Products\Product;
use App\Shop\Priorities\TrendingProduct;

class PriorityController extends Controller
{
    public function index() {
        $permission = Permission::permission('priority');
        if($permission->view==1) {
        	$priorities = Priority::orderBy('priority', 'ASC')->get();
        	return view('admin.priorities.list', [
                'priorities' => $priorities,
                'permission' => $permission
            ]);
        } else {
            return view('layouts.errors.403');
        }
    } 

    public function edit($id) {
    	try {
            $permission = Permission::permission('priority');
            if($permission->edit==1) {
        		if(Priority::where('id', $id)->exists()) {
    	    		$priority = Priority::find($id);
                    $categories = Category::where('parent_id',null)->get();
                    $subcategory = Category::where('parent_id',$priority->category_id)->get();
                    // $products = Product::where('subcategory_id', $priority->subcategory_id)->get();
                    $products = TrendingProduct::join('products','products.id','trending_category.product_id')->select('products.id','products.name', 'trending_category.priority')->get();
    		    	return view('admin.priorities.edit', [
    		            'priority' => $priority,
                        'categories' => $categories,
                        'subcategories' => $subcategory,
                        'products' => $products
    		        ]);
    		    } else {
    		    	return redirect()->route('admin.pro_priority.index')->with('message', 'Priority not found!!');
    		    }
            } else {
                return view('layouts.errors.403');
            }
	    } catch (Exception $e) {
			log::error($e);
    	}
    }

    public function update(Request $request, $id) {
    	try {
            $permission = Permission::permission('priority');
            if($permission->edit==1) {
        		if(Priority::where('id', $id)->exists()) {
    	    		$priority = Priority::find($id);
    	    		$priority->category_id = $request->category_id;
                    $priority->subcategory_id = $request->subcategory_id;
    	    		$priority->save();
    	    		return redirect()->route('admin.pro_priority.index')->with('message', 'Priority update successfully.');
    	    	} else {
    	    		return redirect()->route('admin.pro_priority.index')->with('message', 'Priority not found!!');
    	    	}
            } else {
            return view('layouts.errors.403');
        }
    	} catch (Exception $e) {
    		log::error($e);
    	}
    	
    }

    public function cat_sort(Request $request)
    {
        try{
            $priorities = $request->id;
            if(count($priorities)!=0){
                foreach ($priorities as $key => $value) {
                    if(!empty($value)){
                        $priority = Priority::find($value);
                        $priority->priority = $key;
                        $priority->save();
                    }
                }
            }
            return "true";
        } catch (\Exception $e) {
            Exceptions::exception($e);
            return "false";
        }
    }

    public function subcat_sort(Request $request)
    {
        try{
            $sub_priorities = $request->id;
                // return($sub_priorities) ;
            if(count($sub_priorities)!=0){
                $trendings = TrendingProduct::get();
                if(count($trendings)!=0){
                    foreach($trendings as $trending){
                        $data = TrendingProduct::find($trending->id);
                        // return $data;
                        if($data->delete()){

                        }
                    }
                }
                foreach ($sub_priorities as $key => $value) {
                    if(!empty($value)){
                        $data = new TrendingProduct();
                        $data->priority = $key;
                        $data->product_id = $value;
                        if($data->save()){
                        }
                    }
                }
            }
            return "true";
        } catch (\Exception $e) {
            Exceptions::exception($e);
            return "false";
        }
    }

    public function getProduct(Request $request)
    {
        $products = Product::select('id', 'name')->where('subcategory_id', $request->id)->where('status', 1)->get();
        $output = [];
        foreach( $products as $product )
        {
            $output[$product->id] = $product->name;
        }
        return $output;
    }
}
